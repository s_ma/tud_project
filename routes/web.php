<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

/*
 * Subscribers Routes
 */
Route::resource('subscribers', 'SubscriberController');

/*
 * Aliases Routes
 */
Route::resource('aliases', 'AliasController');

/*
 * Reviews Routes
 */
Route::resource('reviews', 'ReviewController');

/*
 * Hoteliers Routes
 */
Route::resource('hoteliers', 'HotelierController');

/*
 * Hotels Routes
 */
Route::resource('hotels', 'HotelController');

/*
 * Photos Routes
 */
Route::resource('photos', 'PhotoController');

/*
 * Favourites Routes
 */
Route::resource('favourites', 'FavouriteController');

/*
 * SimilarHotels Routes
 */
Route::resource('similar_hotels', 'SimilarHotelController');

/*
 * HotelReviewRatings Routes
 */
Route::resource('hotel_review_ratings', 'HotelReviewRatingController');

/*
 * AnswerHotelQuestions Routes
 */
Route::resource('answer_hotel_questions', 'AnswerHotelQuestionController');

/*
 * SummaryStayTypes Routes
 */
Route::resource('summary_stay_types', 'SummaryStayTypeController');

/*
 * HotelServices Routes
 */
Route::resource('hotel_services', 'HotelServiceController');

/*
 * ReviewUsefulnesses Routes
 */
Route::resource('review_usefulnesses', 'ReviewUsefulnessController');

/*
 * HotelCategories Routes
 */
Route::resource('hotel_categories', 'HotelCategoryController');

/*
 * PriceRanges Routes
 */
Route::resource('price_ranges', 'PriceRangeController');

/*
 * Countries Routes
 */
Route::resource('countries', 'CountryController');

/*
 * VisitDates Routes
 */
Route::resource('visit_dates', 'VisitDateController');

/*
 * HotelQuestions Routes
 */
Route::resource('hotel_questions', 'HotelQuestionController');

/*
 * Services Routes
 */
Route::resource('services', 'ServiceController');

/*
 * HotelReviewTypes Routes
 */
Route::resource('hotel_review_types', 'HotelReviewTypeController');

/*
 * VisitTypes Routes
 */
Route::resource('visit_types', 'VisitTypeController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/**
 * Multi-Authentication Routes
 */
Route::get('/login/subscriber', 'Auth\LoginController@showSubscriberLoginForm')->name('login_subscriber');
Route::get('/login/hotelier', 'Auth\LoginController@showHotelierLoginForm')->name('login_hotelier');
Route::get('/register/subscriber', 'Auth\RegisterController@showSubscriberRegisterForm')->name('register_subscriber');
Route::get('/register/hotelier', 'Auth\RegisterController@showHotelierRegisterForm')->name('register_hotelier');

Route::post('/login/subscriber', 'Auth\LoginController@subscriberLogin');
Route::post('/login/hotelier', 'Auth\LoginController@hotelierLogin');
Route::post('/register/subscriber', 'Auth\RegisterController@createSubscriber');
Route::post('/register/hotelier', 'Auth\RegisterController@createHotelier');

Route::view('/home', 'home')->middleware('auth');
Route::view('/dashboard', 'dashboard');