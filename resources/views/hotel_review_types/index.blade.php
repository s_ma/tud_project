@extends('layouts.app')

@section('title', __('hotel_review_type.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\HotelReviewType)
            <a href="{{ route('hotel_review_types.create') }}" class="btn btn-success">{{ __('hotel_review_type.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('hotel_review_type.list') }} <small>{{ __('app.total') }} : {{ $hotelReviewTypes->total() }} {{ __('hotel_review_type.hotel_review_type') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('hotel_review_type.search') }}</label>
                        <input placeholder="{{ __('hotel_review_type.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('hotel_review_type.search') }}" class="btn btn-secondary">
                    <a href="{{ route('hotel_review_types.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('hotel_review_type.name') }}</th>
                        <th>{{ __('hotel_review_type.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hotelReviewTypes as $key => $hotelReviewType)
                    <tr>
                        <td class="text-center">{{ $hotelReviewTypes->firstItem() + $key }}</td>
                        <td>{!! $hotelReviewType->name_link !!}</td>
                        <td>{{ $hotelReviewType->description }}</td>
                        <td class="text-center">
                            @can('view', $hotelReviewType)
                                <a href="{{ route('hotel_review_types.show', $hotelReviewType) }}" id="show-hotel_review_type-{{ $hotelReviewType->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $hotelReviewTypes->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
