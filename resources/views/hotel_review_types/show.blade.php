@extends('layouts.app')

@section('title', __('hotel_review_type.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotel_review_type.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('hotel_review_type.name') }}</td><td>{{ $hotelReviewType->name }}</td></tr>
                        <tr><td>{{ __('hotel_review_type.description') }}</td><td>{{ $hotelReviewType->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $hotelReviewType)
                    <a href="{{ route('hotel_review_types.edit', $hotelReviewType) }}" id="edit-hotel_review_type-{{ $hotelReviewType->id }}" class="btn btn-warning">{{ __('hotel_review_type.edit') }}</a>
                @endcan
                <a href="{{ route('hotel_review_types.index') }}" class="btn btn-link">{{ __('hotel_review_type.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
