@extends('layouts.app')

@section('title', __('review.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('review.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('review.name') }}</td><td>{{ $review->name }}</td></tr>
                        <tr><td>{{ __('review.description') }}</td><td>{{ $review->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $review)
                    <a href="{{ route('reviews.edit', $review) }}" id="edit-review-{{ $review->id }}" class="btn btn-warning">{{ __('review.edit') }}</a>
                @endcan
                <a href="{{ route('reviews.index') }}" class="btn btn-link">{{ __('review.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
