@extends('layouts.app')

@section('title', __('review.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Review)
            <a href="{{ route('reviews.create') }}" class="btn btn-success">{{ __('review.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('review.list') }} <small>{{ __('app.total') }} : {{ $reviews->total() }} {{ __('review.review') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('review.search') }}</label>
                        <input placeholder="{{ __('review.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('review.search') }}" class="btn btn-secondary">
                    <a href="{{ route('reviews.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('review.name') }}</th>
                        <th>{{ __('review.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($reviews as $key => $review)
                    <tr>
                        <td class="text-center">{{ $reviews->firstItem() + $key }}</td>
                        <td>{!! $review->name_link !!}</td>
                        <td>{{ $review->description }}</td>
                        <td class="text-center">
                            @can('view', $review)
                                <a href="{{ route('reviews.show', $review) }}" id="show-review-{{ $review->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $reviews->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
