@extends('layouts.app')

@section('title', __('review.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $review)
        @can('delete', $review)
            <div class="card">
                <div class="card-header">{{ __('review.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('review.name') }}</label>
                    <p>{{ $review->name }}</p>
                    <label class="form-label text-primary">{{ __('review.description') }}</label>
                    <p>{{ $review->description }}</p>
                    {!! $errors->first('review_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('review.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('reviews.destroy', $review) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="review_id" type="hidden" value="{{ $review->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('reviews.edit', $review) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('review.edit') }}</div>
            <form method="POST" action="{{ route('reviews.update', $review) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('review.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $review->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('review.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $review->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('review.update') }}" class="btn btn-success">
                    <a href="{{ route('reviews.show', $review) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $review)
                        <a href="{{ route('reviews.edit', [$review, 'action' => 'delete']) }}" id="del-review-{{ $review->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
