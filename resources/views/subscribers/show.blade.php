@extends('layouts.app')

@section('title', __('subscriber.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('subscriber.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('subscriber.pseudo') }}</td><td><b>{!! !empty($subscriber->pseudo) ? $subscriber->pseudo : '-' !!}</b></td></tr>
                        <tr><td>{{ __('subscriber.firstname') }}</td><td>{!! !empty($subscriber->firstname) ? $subscriber->firstname : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.lastname') }}</td><td>{!! !empty($subscriber->lastname) ? $subscriber->lastname : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.email') }}</td><td>{!! !empty($subscriber->email) ? $subscriber->email : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.address_1') }}</td><td>{!! !empty($subscriber->address_1) ? $subscriber->address_1 : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.address_2') }}</td><td>{!! !empty($subscriber->address_2) ? $subscriber->address_2 : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.postcode') }}</td><td>{!! !empty($subscriber->postcode) ? $subscriber->postcode : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.city') }}</td><td>{!! !empty($subscriber->city) ? $subscriber->city : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.state') }}</td><td>{!! !empty($subscriber->state) ? $subscriber->state : '-' !!}</td></tr>
                        <tr><td>{{ __('country.name') }}</td><td>{!! !empty($subscriber->country->name) ? $subscriber->country->name : '-' !!}</td></tr>

                        <tr><td>{{ __('subscriber.dialling_code') }}</td><td>+{!! !empty($subscriber->dialling_code) ? $subscriber->dialling_code : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.phone') }}</td><td>{!! !empty($subscriber->phone) ? $subscriber->phone : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.airport') }}</td><td>{!! !empty($subscriber->airport) ? $subscriber->airport : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.latitude') }}</td><td>{!! !empty($subscriber->latitude) ? $subscriber->latitude : '-' !!}</td></tr>
                        <tr><td>{{ __('subscriber.longitude') }}</td><td>{!! !empty($subscriber->longitude) ? $subscriber->longitude : '-' !!}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{-- @can('update', $subscriber) --}}
                    <a href="{{ route('subscribers.edit', $subscriber) }}" id="edit-subscriber-{{ $subscriber->id }}" class="btn btn-warning">{{ __('subscriber.edit') }}</a>
                {{-- @endcan --}}
                
                {{-- <a href="{{ route('subscribers.index') }}" class="btn btn-link">{{ __('subscriber.back_to_index') }}</a> --}}
            </div>
        </div>
    </div>
</div>
@endsection
