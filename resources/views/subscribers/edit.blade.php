@extends('layouts.app')

@section('title', __('subscriber.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $subscriber)
        @can('delete', $subscriber)
            <div class="card">
                <div class="card-header">{{ __('subscriber.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('subscriber.name') }}</label>
                    <p>{{ $subscriber->name }}</p>
                    <label class="form-label text-primary">{{ __('subscriber.description') }}</label>
                    <p>{{ $subscriber->description }}</p>
                    {!! $errors->first('subscriber_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('subscriber.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('subscribers.destroy', $subscriber) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="subscriber_id" type="hidden" value="{{ $subscriber->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('subscribers.edit', $subscriber) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('subscriber.edit') }}</div>
            <form method="POST" action="{{ route('subscribers.update', $subscriber) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="pseudo" class="form-label">{{ __('subscriber.pseudo') }} <span class="form-required">*</span></label>
                        <input id="pseudo" type="text" maxlength="30" class="form-control{{ $errors->has('pseudo') ? ' is-invalid' : '' }}" name="pseudo" value="{{ old('pseudo', $subscriber->pseudo) }}" required>
                        {!! $errors->first('pseudo', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="firstname" class="form-label">{{ __('subscriber.firstname') }} <span class="form-required">*</span></label>
                        <input id="firstname" type="text" maxlength="80" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname', $subscriber->firstname) }}" required>
                        {!! $errors->first('firstname', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="form-label">{{ __('subscriber.lastname') }} <span class="form-required">*</span></label>
                        <input id="lastname" type="text" maxlength="80" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname', $subscriber->lastname) }}" required>
                        {!! $errors->first('lastname', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-label">{{ __('subscriber.email') }} <span class="form-required">*</span></label>
                        <input id="email" readonly="readonly" type="text" maxlength="80" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $subscriber->email) }}">
                        {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="address_1" class="form-label">{{ __('subscriber.address_1') }} <span class="form-required">*</span></label>
                        <input id="address_1" type="text" maxlength="100" class="form-control{{ $errors->has('address_1') ? ' is-invalid' : '' }}" name="address_1" value="{{ old('address_1', $subscriber->address_1) }}" required>
                        {!! $errors->first('address_1', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="address_2" class="form-label">{{ __('subscriber.address_2') }} <span class="form-required">*</span></label>
                        <input id="address_2" type="text" maxlength="100" class="form-control{{ $errors->has('address_2') ? ' is-invalid' : '' }}" name="address_2" value="{{ old('address_2', $subscriber->address_2) }}">
                        {!! $errors->first('address_2', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="postcode" class="form-label">{{ __('subscriber.postcode') }} <span class="form-required">*</span></label>
                        <input id="postcode" type="text" maxlength="10" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" value="{{ old('postcode', $subscriber->postcode) }}" required>
                        {!! $errors->first('postcode', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="city" class="form-label">{{ __('subscriber.city') }} <span class="form-required">*</span></label>
                        <input id="city" type="text" maxlength="50" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city', $subscriber->city) }}" required>
                        {!! $errors->first('city', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="state" class="form-label">{{ __('subscriber.state') }} <span class="form-required">*</span></label>
                        <input id="state" type="text" maxlength="50" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" value="{{ old('state', $subscriber->state) }}">
                        {!! $errors->first('state', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="country_id" class="form-label">{{ __('country.country') }} <span class="form-required">*</span></label>
                        <select name="country_id" id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" value="{{ old('country_id') }}" required>
                            <option value="">{{ __('country.choose') }}</option>
                            @foreach ($countries as $country)
                                <option value={{ $country->id }} {{ old('country_id', $subscriber->country_id) == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('country_id', '<div class="invalid-feedback error" role="alert">:message</div>') !!}
                    </div>
                    <div class="form-group">
                        <label for="dialling_code" class="form-label">{{ __('subscriber.dialling_code') }} <span class="form-required">*</span></label>
                        <input id="dialling_code" type="number" min="0" class="form-control{{ $errors->has('dialling_code') ? ' is-invalid' : '' }}" name="dialling_code" value="{{ old('dialling_code', $subscriber->dialling_code) }}" required>
                        {!! $errors->first('dialling_code', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="phone" class="form-label">{{ __('subscriber.phone') }} <span class="form-required">*</span></label>
                        <input id="phone" type="text" maxlength="20" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $subscriber->phone) }}" required>
                        {!! $errors->first('phone', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="airport" class="form-label">{{ __('subscriber.airport') }} <span class="form-required">*</span></label>
                        <input id="airport" type="text" maxlength="50" class="form-control{{ $errors->has('airport') ? ' is-invalid' : '' }}" name="airport" value="{{ old('airport', $subscriber->airport) }}">
                        {!! $errors->first('airport', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="latitude" class="form-label">{{ __('subscriber.latitude') }} <span class="form-required">*</span></label>
                        <input id="latitude" type="number" min="0" step="0.000001" class="form-control{{ $errors->has('latitude') ? ' is-invalid' : '' }}" name="latitude" value="{{ old('latitude', $subscriber->latitude) }}" required>
                        {!! $errors->first('latitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="longitude" class="form-label">{{ __('subscriber.longitude') }} <span class="form-required">*</span></label>
                        <input id="longitude" type="number" min="0" step="0.000001" class="form-control{{ $errors->has('longitude') ? ' is-invalid' : '' }}" name="longitude" value="{{ old('longitude', $subscriber->longitude) }}" required>
                        {!! $errors->first('longitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('subscriber.update') }}" class="btn btn-success">
                    <a href="{{ route('subscribers.show', $subscriber) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $subscriber)
                        <a href="{{ route('subscribers.edit', [$subscriber, 'action' => 'delete']) }}" id="del-subscriber-{{ $subscriber->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
