@extends('layouts.app')

@section('title', __('subscriber.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Subscriber)
            <a href="{{ route('subscribers.create') }}" class="btn btn-success">{{ __('subscriber.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('subscriber.list') }} <small>{{ __('app.total') }} : {{ $subscribers->total() }} {{ __('subscriber.subscriber') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('subscriber.search') }}</label>
                        <input placeholder="{{ __('subscriber.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('subscriber.search') }}" class="btn btn-secondary">
                    <a href="{{ route('subscribers.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('subscriber.name') }}</th>
                        <th>{{ __('subscriber.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($subscribers as $key => $subscriber)
                    <tr>
                        <td class="text-center">{{ $subscribers->firstItem() + $key }}</td>
                        <td>{!! $subscriber->name_link !!}</td>
                        <td>{{ $subscriber->description }}</td>
                        <td class="text-center">
                            @can('view', $subscriber)
                                <a href="{{ route('subscribers.show', $subscriber) }}" id="show-subscriber-{{ $subscriber->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $subscribers->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
