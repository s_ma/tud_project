@extends('layouts.app')

@section('title', __('hotel_question.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\HotelQuestion)
            <a href="{{ route('hotel_questions.create') }}" class="btn btn-success">{{ __('hotel_question.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('hotel_question.list') }} <small>{{ __('app.total') }} : {{ $hotelQuestions->total() }} {{ __('hotel_question.hotel_question') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('hotel_question.search') }}</label>
                        <input placeholder="{{ __('hotel_question.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('hotel_question.search') }}" class="btn btn-secondary">
                    <a href="{{ route('hotel_questions.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('hotel_question.name') }}</th>
                        <th>{{ __('hotel_question.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hotelQuestions as $key => $hotelQuestion)
                    <tr>
                        <td class="text-center">{{ $hotelQuestions->firstItem() + $key }}</td>
                        <td>{!! $hotelQuestion->name_link !!}</td>
                        <td>{{ $hotelQuestion->description }}</td>
                        <td class="text-center">
                            @can('view', $hotelQuestion)
                                <a href="{{ route('hotel_questions.show', $hotelQuestion) }}" id="show-hotel_question-{{ $hotelQuestion->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $hotelQuestions->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
