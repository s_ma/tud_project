@extends('layouts.app')

@section('title', __('hotel_question.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotel_question.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('hotel_question.name') }}</td><td>{{ $hotelQuestion->name }}</td></tr>
                        <tr><td>{{ __('hotel_question.description') }}</td><td>{{ $hotelQuestion->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $hotelQuestion)
                    <a href="{{ route('hotel_questions.edit', $hotelQuestion) }}" id="edit-hotel_question-{{ $hotelQuestion->id }}" class="btn btn-warning">{{ __('hotel_question.edit') }}</a>
                @endcan
                <a href="{{ route('hotel_questions.index') }}" class="btn btn-link">{{ __('hotel_question.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
