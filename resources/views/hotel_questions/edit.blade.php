@extends('layouts.app')

@section('title', __('hotel_question.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $hotelQuestion)
        @can('delete', $hotelQuestion)
            <div class="card">
                <div class="card-header">{{ __('hotel_question.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('hotel_question.name') }}</label>
                    <p>{{ $hotelQuestion->name }}</p>
                    <label class="form-label text-primary">{{ __('hotel_question.description') }}</label>
                    <p>{{ $hotelQuestion->description }}</p>
                    {!! $errors->first('hotel_question_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('hotel_question.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('hotel_questions.destroy', $hotelQuestion) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="hotel_question_id" type="hidden" value="{{ $hotelQuestion->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('hotel_questions.edit', $hotelQuestion) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('hotel_question.edit') }}</div>
            <form method="POST" action="{{ route('hotel_questions.update', $hotelQuestion) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('hotel_question.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $hotelQuestion->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('hotel_question.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $hotelQuestion->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('hotel_question.update') }}" class="btn btn-success">
                    <a href="{{ route('hotel_questions.show', $hotelQuestion) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $hotelQuestion)
                        <a href="{{ route('hotel_questions.edit', [$hotelQuestion, 'action' => 'delete']) }}" id="del-hotel_question-{{ $hotelQuestion->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
