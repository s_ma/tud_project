@extends('layouts.app')

@section('title', __('hotel_category.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\HotelCategory)
            <a href="{{ route('hotel_categories.create') }}" class="btn btn-success">{{ __('hotel_category.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('hotel_category.list') }} <small>{{ __('app.total') }} : {{ $hotelCategories->total() }} {{ __('hotel_category.hotel_category') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('hotel_category.search') }}</label>
                        <input placeholder="{{ __('hotel_category.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('hotel_category.search') }}" class="btn btn-secondary">
                    <a href="{{ route('hotel_categories.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('hotel_category.name') }}</th>
                        <th>{{ __('hotel_category.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hotelCategories as $key => $hotelCategory)
                    <tr>
                        <td class="text-center">{{ $hotelCategories->firstItem() + $key }}</td>
                        <td>{!! $hotelCategory->name_link !!}</td>
                        <td>{{ $hotelCategory->description }}</td>
                        <td class="text-center">
                            @can('view', $hotelCategory)
                                <a href="{{ route('hotel_categories.show', $hotelCategory) }}" id="show-hotel_category-{{ $hotelCategory->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $hotelCategories->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
