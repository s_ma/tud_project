@extends('layouts.app')

@section('title', __('hotel_category.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotel_category.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('hotel_category.name') }}</td><td>{{ $hotelCategory->name }}</td></tr>
                        <tr><td>{{ __('hotel_category.description') }}</td><td>{{ $hotelCategory->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $hotelCategory)
                    <a href="{{ route('hotel_categories.edit', $hotelCategory) }}" id="edit-hotel_category-{{ $hotelCategory->id }}" class="btn btn-warning">{{ __('hotel_category.edit') }}</a>
                @endcan
                <a href="{{ route('hotel_categories.index') }}" class="btn btn-link">{{ __('hotel_category.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
