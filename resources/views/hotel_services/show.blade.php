@extends('layouts.app')

@section('title', __('hotel_service.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotel_service.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('hotel_service.name') }}</td><td>{{ $hotelService->name }}</td></tr>
                        <tr><td>{{ __('hotel_service.description') }}</td><td>{{ $hotelService->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $hotelService)
                    <a href="{{ route('hotel_services.edit', $hotelService) }}" id="edit-hotel_service-{{ $hotelService->id }}" class="btn btn-warning">{{ __('hotel_service.edit') }}</a>
                @endcan
                <a href="{{ route('hotel_services.index') }}" class="btn btn-link">{{ __('hotel_service.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
