@extends('layouts.app')

@section('title', __('hotel_service.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\HotelService)
            <a href="{{ route('hotel_services.create') }}" class="btn btn-success">{{ __('hotel_service.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('hotel_service.list') }} <small>{{ __('app.total') }} : {{ $hotelServices->total() }} {{ __('hotel_service.hotel_service') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('hotel_service.search') }}</label>
                        <input placeholder="{{ __('hotel_service.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('hotel_service.search') }}" class="btn btn-secondary">
                    <a href="{{ route('hotel_services.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('hotel_service.name') }}</th>
                        <th>{{ __('hotel_service.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hotelServices as $key => $hotelService)
                    <tr>
                        <td class="text-center">{{ $hotelServices->firstItem() + $key }}</td>
                        <td>{!! $hotelService->name_link !!}</td>
                        <td>{{ $hotelService->description }}</td>
                        <td class="text-center">
                            @can('view', $hotelService)
                                <a href="{{ route('hotel_services.show', $hotelService) }}" id="show-hotel_service-{{ $hotelService->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $hotelServices->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
