@extends('layouts.app')

@section('title', __('price_range.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $priceRange)
        @can('delete', $priceRange)
            <div class="card">
                <div class="card-header">{{ __('price_range.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('price_range.name') }}</label>
                    <p>{{ $priceRange->name }}</p>
                    <label class="form-label text-primary">{{ __('price_range.description') }}</label>
                    <p>{{ $priceRange->description }}</p>
                    {!! $errors->first('price_range_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('price_range.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('price_ranges.destroy', $priceRange) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="price_range_id" type="hidden" value="{{ $priceRange->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('price_ranges.edit', $priceRange) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('price_range.edit') }}</div>
            <form method="POST" action="{{ route('price_ranges.update', $priceRange) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('price_range.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $priceRange->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('price_range.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $priceRange->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('price_range.update') }}" class="btn btn-success">
                    <a href="{{ route('price_ranges.show', $priceRange) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $priceRange)
                        <a href="{{ route('price_ranges.edit', [$priceRange, 'action' => 'delete']) }}" id="del-price_range-{{ $priceRange->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
