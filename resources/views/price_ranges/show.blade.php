@extends('layouts.app')

@section('title', __('price_range.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('price_range.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('price_range.name') }}</td><td>{{ $priceRange->name }}</td></tr>
                        <tr><td>{{ __('price_range.description') }}</td><td>{{ $priceRange->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $priceRange)
                    <a href="{{ route('price_ranges.edit', $priceRange) }}" id="edit-price_range-{{ $priceRange->id }}" class="btn btn-warning">{{ __('price_range.edit') }}</a>
                @endcan
                <a href="{{ route('price_ranges.index') }}" class="btn btn-link">{{ __('price_range.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
