@extends('layouts.app')

@section('title', __('alias.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('alias.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('alias.name') }}</td><td>{{ $alias->name }}</td></tr>
                        <tr><td>{{ __('alias.description') }}</td><td>{{ $alias->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $alias)
                    <a href="{{ route('aliases.edit', $alias) }}" id="edit-alias-{{ $alias->id }}" class="btn btn-warning">{{ __('alias.edit') }}</a>
                @endcan
                <a href="{{ route('aliases.index') }}" class="btn btn-link">{{ __('alias.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
