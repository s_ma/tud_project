@extends('layouts.app')

@section('title', __('alias.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $alias)
        @can('delete', $alias)
            <div class="card">
                <div class="card-header">{{ __('alias.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('alias.name') }}</label>
                    <p>{{ $alias->name }}</p>
                    <label class="form-label text-primary">{{ __('alias.description') }}</label>
                    <p>{{ $alias->description }}</p>
                    {!! $errors->first('alias_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('alias.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('aliases.destroy', $alias) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="alias_id" type="hidden" value="{{ $alias->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('aliases.edit', $alias) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('alias.edit') }}</div>
            <form method="POST" action="{{ route('aliases.update', $alias) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('alias.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $alias->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('alias.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $alias->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('alias.update') }}" class="btn btn-success">
                    <a href="{{ route('aliases.show', $alias) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $alias)
                        <a href="{{ route('aliases.edit', [$alias, 'action' => 'delete']) }}" id="del-alias-{{ $alias->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
