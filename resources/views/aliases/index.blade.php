@extends('layouts.app')

@section('title', __('alias.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Alias)
            <a href="{{ route('aliases.create') }}" class="btn btn-success">{{ __('alias.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('alias.list') }} <small>{{ __('app.total') }} : {{ $aliases->total() }} {{ __('alias.alias') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('alias.search') }}</label>
                        <input placeholder="{{ __('alias.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('alias.search') }}" class="btn btn-secondary">
                    <a href="{{ route('aliases.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('alias.name') }}</th>
                        <th>{{ __('alias.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($aliases as $key => $alias)
                    <tr>
                        <td class="text-center">{{ $aliases->firstItem() + $key }}</td>
                        <td>{!! $alias->name_link !!}</td>
                        <td>{{ $alias->description }}</td>
                        <td class="text-center">
                            @can('view', $alias)
                                <a href="{{ route('aliases.show', $alias) }}" id="show-alias-{{ $alias->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $aliases->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
