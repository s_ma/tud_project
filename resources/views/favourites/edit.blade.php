@extends('layouts.app')

@section('title', __('favourite.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $favourite)
        @can('delete', $favourite)
            <div class="card">
                <div class="card-header">{{ __('favourite.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('favourite.name') }}</label>
                    <p>{{ $favourite->name }}</p>
                    <label class="form-label text-primary">{{ __('favourite.description') }}</label>
                    <p>{{ $favourite->description }}</p>
                    {!! $errors->first('favourite_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('favourite.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('favourites.destroy', $favourite) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="favourite_id" type="hidden" value="{{ $favourite->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('favourites.edit', $favourite) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('favourite.edit') }}</div>
            <form method="POST" action="{{ route('favourites.update', $favourite) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('favourite.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $favourite->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('favourite.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $favourite->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('favourite.update') }}" class="btn btn-success">
                    <a href="{{ route('favourites.show', $favourite) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $favourite)
                        <a href="{{ route('favourites.edit', [$favourite, 'action' => 'delete']) }}" id="del-favourite-{{ $favourite->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
