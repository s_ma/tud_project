@extends('layouts.app')

@section('title', __('favourite.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Favourite)
            <a href="{{ route('favourites.create') }}" class="btn btn-success">{{ __('favourite.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('favourite.list') }} <small>{{ __('app.total') }} : {{ $favourites->total() }} {{ __('favourite.favourite') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('favourite.search') }}</label>
                        <input placeholder="{{ __('favourite.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('favourite.search') }}" class="btn btn-secondary">
                    <a href="{{ route('favourites.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('favourite.name') }}</th>
                        <th>{{ __('favourite.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($favourites as $key => $favourite)
                    <tr>
                        <td class="text-center">{{ $favourites->firstItem() + $key }}</td>
                        <td>{!! $favourite->name_link !!}</td>
                        <td>{{ $favourite->description }}</td>
                        <td class="text-center">
                            @can('view', $favourite)
                                <a href="{{ route('favourites.show', $favourite) }}" id="show-favourite-{{ $favourite->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $favourites->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
