@extends('layouts.app')

@section('title', __('favourite.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('favourite.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('favourite.name') }}</td><td>{{ $favourite->name }}</td></tr>
                        <tr><td>{{ __('favourite.description') }}</td><td>{{ $favourite->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $favourite)
                    <a href="{{ route('favourites.edit', $favourite) }}" id="edit-favourite-{{ $favourite->id }}" class="btn btn-warning">{{ __('favourite.edit') }}</a>
                @endcan
                <a href="{{ route('favourites.index') }}" class="btn btn-link">{{ __('favourite.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
