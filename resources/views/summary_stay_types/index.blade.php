@extends('layouts.app')

@section('title', __('summary_stay_type.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\SummaryStayType)
            <a href="{{ route('summary_stay_types.create') }}" class="btn btn-success">{{ __('summary_stay_type.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('summary_stay_type.list') }} <small>{{ __('app.total') }} : {{ $summaryStayTypes->total() }} {{ __('summary_stay_type.summary_stay_type') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('summary_stay_type.search') }}</label>
                        <input placeholder="{{ __('summary_stay_type.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('summary_stay_type.search') }}" class="btn btn-secondary">
                    <a href="{{ route('summary_stay_types.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('summary_stay_type.name') }}</th>
                        <th>{{ __('summary_stay_type.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($summaryStayTypes as $key => $summaryStayType)
                    <tr>
                        <td class="text-center">{{ $summaryStayTypes->firstItem() + $key }}</td>
                        <td>{!! $summaryStayType->name_link !!}</td>
                        <td>{{ $summaryStayType->description }}</td>
                        <td class="text-center">
                            @can('view', $summaryStayType)
                                <a href="{{ route('summary_stay_types.show', $summaryStayType) }}" id="show-summary_stay_type-{{ $summaryStayType->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $summaryStayTypes->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
