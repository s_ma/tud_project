@extends('layouts.app')

@section('title', __('summary_stay_type.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('summary_stay_type.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('summary_stay_type.name') }}</td><td>{{ $summaryStayType->name }}</td></tr>
                        <tr><td>{{ __('summary_stay_type.description') }}</td><td>{{ $summaryStayType->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $summaryStayType)
                    <a href="{{ route('summary_stay_types.edit', $summaryStayType) }}" id="edit-summary_stay_type-{{ $summaryStayType->id }}" class="btn btn-warning">{{ __('summary_stay_type.edit') }}</a>
                @endcan
                <a href="{{ route('summary_stay_types.index') }}" class="btn btn-link">{{ __('summary_stay_type.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
