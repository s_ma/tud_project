@extends('layouts.app')

@section('title', __('summary_stay_type.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $summaryStayType)
        @can('delete', $summaryStayType)
            <div class="card">
                <div class="card-header">{{ __('summary_stay_type.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('summary_stay_type.name') }}</label>
                    <p>{{ $summaryStayType->name }}</p>
                    <label class="form-label text-primary">{{ __('summary_stay_type.description') }}</label>
                    <p>{{ $summaryStayType->description }}</p>
                    {!! $errors->first('summary_stay_type_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('summary_stay_type.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('summary_stay_types.destroy', $summaryStayType) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="summary_stay_type_id" type="hidden" value="{{ $summaryStayType->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('summary_stay_types.edit', $summaryStayType) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('summary_stay_type.edit') }}</div>
            <form method="POST" action="{{ route('summary_stay_types.update', $summaryStayType) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('summary_stay_type.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $summaryStayType->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('summary_stay_type.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $summaryStayType->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('summary_stay_type.update') }}" class="btn btn-success">
                    <a href="{{ route('summary_stay_types.show', $summaryStayType) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $summaryStayType)
                        <a href="{{ route('summary_stay_types.edit', [$summaryStayType, 'action' => 'delete']) }}" id="del-summary_stay_type-{{ $summaryStayType->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
