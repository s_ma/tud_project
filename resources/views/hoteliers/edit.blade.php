@extends('layouts.app')

@section('title', __('hotelier.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $hotelier)
        @can('delete', $hotelier)
            <div class="card">
                <div class="card-header">{{ __('hotelier.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('hotelier.name') }}</label>
                    <p>{{ $hotelier->name }}</p>
                    <label class="form-label text-primary">{{ __('hotelier.description') }}</label>
                    <p>{{ $hotelier->description }}</p>
                    {!! $errors->first('hotelier_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('hotelier.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('hoteliers.destroy', $hotelier) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="hotelier_id" type="hidden" value="{{ $hotelier->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('hoteliers.edit', $hotelier) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('hotelier.edit') }}</div>
            <form method="POST" action="{{ route('hoteliers.update', $hotelier) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="firstname" class="form-label">{{ __('hotelier.firstname') }} <span class="form-required">*</span></label>
                        <input id="firstname" type="text" maxlength="80" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname', $hotelier->firstname) }}" required>
                        {!! $errors->first('firstname', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="lastname" class="form-label">{{ __('hotelier.lastname') }} <span class="form-required">*</span></label>
                        <input id="lastname" type="text" maxlength="80" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname', $hotelier->lastname) }}" required>
                        {!! $errors->first('lastname', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-label">{{ __('hotelier.email') }} <span class="form-required">*</span></label>
                        <input id="email" readonly="readonly" type="text" maxlength="80" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $hotelier->email) }}">
                        {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="address_1" class="form-label">{{ __('hotelier.address_1') }} <span class="form-required">*</span></label>
                        <input id="address_1" type="text" maxlength="100" class="form-control{{ $errors->has('address_1') ? ' is-invalid' : '' }}" name="address_1" value="{{ old('address_1', $hotelier->address_1) }}" required>
                        {!! $errors->first('address_1', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="address_2" class="form-label">{{ __('hotelier.address_2') }} <span class="form-required">*</span></label>
                        <input id="address_2" type="text" maxlength="100" class="form-control{{ $errors->has('address_2') ? ' is-invalid' : '' }}" name="address_2" value="{{ old('address_2', $hotelier->address_2) }}">
                        {!! $errors->first('address_2', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="postcode" class="form-label">{{ __('hotelier.postcode') }} <span class="form-required">*</span></label>
                        <input id="postcode" type="text" maxlength="10" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" value="{{ old('postcode', $hotelier->postcode) }}" required>
                        {!! $errors->first('postcode', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="city" class="form-label">{{ __('hotelier.city') }} <span class="form-required">*</span></label>
                        <input id="city" type="text" maxlength="50" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city', $hotelier->city) }}" required>
                        {!! $errors->first('city', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="state" class="form-label">{{ __('hotelier.state') }} <span class="form-required">*</span></label>
                        <input id="state" type="text" maxlength="50" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" value="{{ old('state', $hotelier->state) }}">
                        {!! $errors->first('state', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="country_id" class="form-label">{{ __('country.country') }} <span class="form-required">*</span></label>
                        <select name="country_id" id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" value="{{ old('country_id') }}" required>
                            <option value="">{{ __('country.choose') }}</option>
                            @foreach ($countries as $country)
                                <option value={{ $country->id }} {{ old('country_id', $hotelier->country_id) == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('country_id', '<div class="invalid-feedback error" role="alert">:message</div>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('hotelier.update') }}" class="btn btn-success">
                    <a href="{{ route('hoteliers.show', $hotelier) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $hotelier)
                        <a href="{{ route('hoteliers.edit', [$hotelier, 'action' => 'delete']) }}" id="del-hotelier-{{ $hotelier->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
