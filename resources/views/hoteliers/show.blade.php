@extends('layouts.app')

@section('title', __('hotelier.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotelier.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('hotelier.firstname') }}</td><td>{!! !empty($hotelier->firstname) ? $hotelier->firstname : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.lastname') }}</td><td>{!! !empty($hotelier->lastname) ? $hotelier->lastname : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.email') }}</td><td>{!! !empty($hotelier->email) ? $hotelier->email : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.address_1') }}</td><td>{!! !empty($hotelier->address_1) ? $hotelier->address_1 : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.address_2') }}</td><td>{!! !empty($hotelier->address_2) ? $hotelier->address_2 : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.postcode') }}</td><td>{!! !empty($hotelier->postcode) ? $hotelier->postcode : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.city') }}</td><td>{!! !empty($hotelier->city) ? $hotelier->city : '-' !!}</td></tr>
                        <tr><td>{{ __('hotelier.state') }}</td><td>{!! !empty($hotelier->state) ? $hotelier->state : '-' !!}</td></tr>
                        <tr><td>{{ __('country.name') }}</td><td>{!! !empty($hotelier->country->name) ? $hotelier->country->name : '-' !!}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{-- @can('update', $hotelier) --}}
                    <a href="{{ route('hoteliers.edit', $hotelier) }}" id="edit-hotelier-{{ $hotelier->id }}" class="btn btn-warning">{{ __('hotelier.edit') }}</a>
                {{-- @endcan --}}

                <a href="{{ route('hoteliers.index') }}" class="btn btn-link">{{ __('hotelier.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
