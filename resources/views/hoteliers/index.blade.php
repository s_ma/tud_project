@extends('layouts.app')

@section('title', __('hotelier.list'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-11">
        <div class="mb-3">
            <div class="float-right">
                {{-- @can('create', new App\Hotelier) --}}
                    <a href="{{ route('hoteliers.create') }}" class="btn btn-success">{{ __('hotelier.create') }}</a>
                {{-- @endcan --}}
            </div>
            <h1 class="page-title">{{ __('hotelier.list') }} <small>{{ __('app.total') }} : {{ $hoteliers->total() }} {{ __('hotelier.hotelier') }}</small></h1>
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-11">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('hotelier.search') }}</label>
                        <input placeholder="{{ __('hotelier.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('hotelier.search') }}" class="btn btn-secondary">
                    <a href="{{ route('hoteliers.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('hotelier.name') }}</th>
                        <th>{{ __('hotelier.email') }}</th>
                        <th>{{ __('hotelier.address_1') }}</th>
                        <th>{{ __('hotelier.city') }}</th>
                        <th>{{ __('country.name') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hoteliers as $key => $hotelier)
                    <tr>
                        <td class="text-center">{{ $hoteliers->firstItem() + $key }}</td>
                        <td>{!! $hotelier->firstname !!} {!! $hotelier->lastname !!}</td>
                        <td>{{ $hotelier->email }}</td>
                        <td>{{ $hotelier->address_1 }}</td>
                        <td>{{ $hotelier->city }}</td>
                        <td>{{ $hotelier->country->name }}</td>
                        <td class="text-center">
                            {{-- @can('view', $hotelier) --}}
                                <a href="{{ route('hoteliers.show', $hotelier) }}" id="show-hotelier-{{ $hotelier->id }}">{{ __('app.show') }}</a>
                            {{-- @endcan --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $hoteliers->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
