@extends('layouts.app')

@section('title', __('photo.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('photo.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('photo.name') }}</td><td>{{ $photo->name }}</td></tr>
                        <tr><td>{{ __('photo.description') }}</td><td>{{ $photo->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $photo)
                    <a href="{{ route('photos.edit', $photo) }}" id="edit-photo-{{ $photo->id }}" class="btn btn-warning">{{ __('photo.edit') }}</a>
                @endcan
                <a href="{{ route('photos.index') }}" class="btn btn-link">{{ __('photo.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
