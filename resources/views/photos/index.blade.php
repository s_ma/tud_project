@extends('layouts.app')

@section('title', __('photo.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Photo)
            <a href="{{ route('photos.create') }}" class="btn btn-success">{{ __('photo.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('photo.list') }} <small>{{ __('app.total') }} : {{ $photos->total() }} {{ __('photo.photo') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('photo.search') }}</label>
                        <input placeholder="{{ __('photo.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('photo.search') }}" class="btn btn-secondary">
                    <a href="{{ route('photos.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('photo.name') }}</th>
                        <th>{{ __('photo.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($photos as $key => $photo)
                    <tr>
                        <td class="text-center">{{ $photos->firstItem() + $key }}</td>
                        <td>{!! $photo->name_link !!}</td>
                        <td>{{ $photo->description }}</td>
                        <td class="text-center">
                            @can('view', $photo)
                                <a href="{{ route('photos.show', $photo) }}" id="show-photo-{{ $photo->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $photos->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
