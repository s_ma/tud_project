@extends('layouts.app')

@section('title', __('photo.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $photo)
        @can('delete', $photo)
            <div class="card">
                <div class="card-header">{{ __('photo.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('photo.name') }}</label>
                    <p>{{ $photo->name }}</p>
                    <label class="form-label text-primary">{{ __('photo.description') }}</label>
                    <p>{{ $photo->description }}</p>
                    {!! $errors->first('photo_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('photo.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('photos.destroy', $photo) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="photo_id" type="hidden" value="{{ $photo->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('photos.edit', $photo) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('photo.edit') }}</div>
            <form method="POST" action="{{ route('photos.update', $photo) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('photo.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $photo->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('photo.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $photo->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('photo.update') }}" class="btn btn-success">
                    <a href="{{ route('photos.show', $photo) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $photo)
                        <a href="{{ route('photos.edit', [$photo, 'action' => 'delete']) }}" id="del-photo-{{ $photo->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
