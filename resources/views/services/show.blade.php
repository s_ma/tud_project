@extends('layouts.app')

@section('title', __('service.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('service.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('service.name') }}</td><td>{{ $service->name }}</td></tr>
                        <tr><td>{{ __('service.description') }}</td><td>{{ $service->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $service)
                    <a href="{{ route('services.edit', $service) }}" id="edit-service-{{ $service->id }}" class="btn btn-warning">{{ __('service.edit') }}</a>
                @endcan
                <a href="{{ route('services.index') }}" class="btn btn-link">{{ __('service.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
