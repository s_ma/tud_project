@extends('layouts.app')

@section('title', __('service.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $service)
        @can('delete', $service)
            <div class="card">
                <div class="card-header">{{ __('service.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('service.name') }}</label>
                    <p>{{ $service->name }}</p>
                    <label class="form-label text-primary">{{ __('service.description') }}</label>
                    <p>{{ $service->description }}</p>
                    {!! $errors->first('service_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('service.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('services.destroy', $service) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="service_id" type="hidden" value="{{ $service->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('services.edit', $service) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('service.edit') }}</div>
            <form method="POST" action="{{ route('services.update', $service) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('service.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $service->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('service.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $service->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('service.update') }}" class="btn btn-success">
                    <a href="{{ route('services.show', $service) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $service)
                        <a href="{{ route('services.edit', [$service, 'action' => 'delete']) }}" id="del-service-{{ $service->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
