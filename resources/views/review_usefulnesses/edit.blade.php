@extends('layouts.app')

@section('title', __('review_usefulness.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $reviewUsefulness)
        @can('delete', $reviewUsefulness)
            <div class="card">
                <div class="card-header">{{ __('review_usefulness.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('review_usefulness.name') }}</label>
                    <p>{{ $reviewUsefulness->name }}</p>
                    <label class="form-label text-primary">{{ __('review_usefulness.description') }}</label>
                    <p>{{ $reviewUsefulness->description }}</p>
                    {!! $errors->first('review_usefulness_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('review_usefulness.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('review_usefulnesses.destroy', $reviewUsefulness) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="review_usefulness_id" type="hidden" value="{{ $reviewUsefulness->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('review_usefulnesses.edit', $reviewUsefulness) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('review_usefulness.edit') }}</div>
            <form method="POST" action="{{ route('review_usefulnesses.update', $reviewUsefulness) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('review_usefulness.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $reviewUsefulness->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('review_usefulness.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $reviewUsefulness->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('review_usefulness.update') }}" class="btn btn-success">
                    <a href="{{ route('review_usefulnesses.show', $reviewUsefulness) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $reviewUsefulness)
                        <a href="{{ route('review_usefulnesses.edit', [$reviewUsefulness, 'action' => 'delete']) }}" id="del-review_usefulness-{{ $reviewUsefulness->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
