@extends('layouts.app')

@section('title', __('review_usefulness.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\ReviewUsefulness)
            <a href="{{ route('review_usefulnesses.create') }}" class="btn btn-success">{{ __('review_usefulness.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('review_usefulness.list') }} <small>{{ __('app.total') }} : {{ $reviewUsefulnesses->total() }} {{ __('review_usefulness.review_usefulness') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('review_usefulness.search') }}</label>
                        <input placeholder="{{ __('review_usefulness.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('review_usefulness.search') }}" class="btn btn-secondary">
                    <a href="{{ route('review_usefulnesses.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('review_usefulness.name') }}</th>
                        <th>{{ __('review_usefulness.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($reviewUsefulnesses as $key => $reviewUsefulness)
                    <tr>
                        <td class="text-center">{{ $reviewUsefulnesses->firstItem() + $key }}</td>
                        <td>{!! $reviewUsefulness->name_link !!}</td>
                        <td>{{ $reviewUsefulness->description }}</td>
                        <td class="text-center">
                            @can('view', $reviewUsefulness)
                                <a href="{{ route('review_usefulnesses.show', $reviewUsefulness) }}" id="show-review_usefulness-{{ $reviewUsefulness->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $reviewUsefulnesses->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
