@extends('layouts.app')

@section('title', __('review_usefulness.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('review_usefulness.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('review_usefulness.name') }}</td><td>{{ $reviewUsefulness->name }}</td></tr>
                        <tr><td>{{ __('review_usefulness.description') }}</td><td>{{ $reviewUsefulness->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $reviewUsefulness)
                    <a href="{{ route('review_usefulnesses.edit', $reviewUsefulness) }}" id="edit-review_usefulness-{{ $reviewUsefulness->id }}" class="btn btn-warning">{{ __('review_usefulness.edit') }}</a>
                @endcan
                <a href="{{ route('review_usefulnesses.index') }}" class="btn btn-link">{{ __('review_usefulness.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
