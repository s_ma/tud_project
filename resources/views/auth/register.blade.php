@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ isset($url) ? ucwords($url) : ""}} {{ __('Register') }}</div>

                <div class="card-body">
                    @isset($url)
                        <form method="POST" action='{{ url("register/$url") }}' aria-label="{{ __('Register') }}">       
                            @csrf             
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.email') }}</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" maxlength="80" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                    {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.password') }}</label>
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    {!! $errors->first('password', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>                       
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.confirm_password') }}</label>
                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.lastname') }}</label>
                                <div class="col-md-6">
                                    <input id="lastname" type="text" maxlength="80" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>
                                    {!! $errors->first('lastname', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.firstname') }}</label>
                                <div class="col-md-6">
                                    <input id="firstname" type="text" maxlength="80" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>
                                    {!! $errors->first('firstname', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address_1" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.address_1') }}</label>
                                <div class="col-md-6">
                                    <input id="address_1" type="text" maxlength="100" class="form-control @error('address_1') is-invalid @enderror" name="address_1" value="{{ old('address_1') }}" required autocomplete="address_1" autofocus>
                                    {!! $errors->first('address_1', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address_2" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.address_2') }}</label>
                                <div class="col-md-6">
                                    <input id="address_2" type="text" maxlength="100" class="form-control @error('address_2') is-invalid @enderror" name="address_2" value="{{ old('address_2') }}" autocomplete="address_2" autofocus>
                                    {!! $errors->first('address_2', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="postcode" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.postcode') }}</label>
                                <div class="col-md-6">
                                    <input id="postcode" type="text" maxlength="10" class="form-control @error('postcode') is-invalid @enderror" name="postcode" value="{{ old('postcode') }}" required autocomplete="postcode" autofocus>
                                    {!! $errors->first('postcode', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="city" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.city') }}</label>
                                <div class="col-md-6">
                                    <input id="city" type="text" maxlength="50" class="form-control @error('city') is-invalid @enderror" name="city" value="{{ old('city') }}" required autocomplete="city" autofocus>
                                    {!! $errors->first('city', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="state" class="col-md-4 col-form-label text-md-right">{{ __('hotelier.state') }}</label>
                                <div class="col-md-6">
                                    <input id="state" type="text" maxlength="50" class="form-control @error('state') is-invalid @enderror" name="state" value="{{ old('state') }}" autocomplete="state" autofocus>
                                    {!! $errors->first('state', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="country_id" class="col-md-4 col-form-label text-md-right">{{ __('country.country') }} <span class="form-required">*</span></label>
                                <div class="col-md-6">
                                    <select name="country_id" id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" value="{{ old('country_id') }}" required>
                                        <option value="">{{ __('country.choose') }}</option>
                                        @foreach ($countries as $country)
                                            <option value={{ $country->id }} {{ old('country_id') == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('country_id', '<div class="invalid-feedback error" role="alert">:message</div>') !!}
                                </div>
                            </div>

                            {{-- Additional fields if Subscriber account --}}
                            @if($url == "subscriber")
                                <div class="form-group row">
                                    <label for="latitude" class="col-md-4 col-form-label text-md-right">{{ __('subscriber.latitude') }}</label>
                                    <div class="col-md-6">
                                        <input id="latitude" type="number" min="0" step="0.000001" class="form-control @error('state') is-invalid @enderror" name="latitude" value="{{ old('latitude') }}" required autocomplete="latitude" autofocus>
                                        {!! $errors->first('latitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="longitude" class="col-md-4 col-form-label text-md-right">{{ __('subscriber.longitude') }}</label>
                                    <div class="col-md-6">
                                        <input id="longitude" type="number" min="0" step="0.000001" class="form-control @error('longitude') is-invalid @enderror" name="longitude" value="{{ old('longitude') }}" required autocomplete="longitude" autofocus>
                                        {!! $errors->first('longitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="pseudo" class="col-md-4 col-form-label text-md-right">{{ __('subscriber.pseudo') }}</label>
                                    <div class="col-md-6">
                                        <input id="pseudo" type="text" maxlength="30" class="form-control @error('pseudo') is-invalid @enderror" name="pseudo" value="{{ old('pseudo') }}" required autocomplete="pseudo" autofocus>
                                        {!! $errors->first('pseudo', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="dialling_code" class="col-md-4 col-form-label text-md-right">{{ __('subscriber.dialling_code') }}</label>
                                    <div class="col-md-6">
                                        <input id="dialling_code" type="text" class="form-control @error('dialling_code') is-invalid @enderror" name="dialling_code" value="{{ old('dialling_code') }}" required autocomplete="dialling_code" autofocus>
                                        {!! $errors->first('dialling_code', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('subscriber.phone') }}</label>
                                    <div class="col-md-6">
                                        <input id="phone" type="text" maxlength="20" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                        {!! $errors->first('phone', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="airport" class="col-md-4 col-form-label text-md-right">{{ __('subscriber.airport') }}</label>
                                    <div class="col-md-6">
                                        <input id="airport" type="text" maxlength="50" class="form-control @error('airport') is-invalid @enderror" name="airport" value="{{ old('airport') }}" required autocomplete="airport" autofocus>
                                        {!! $errors->first('airport', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                                    </div>
                                </div>
                            @endif


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    @else
                        {{-- Register as a Subscriber --}}
                        <a href="{{ URL::route('register_subscriber') }}" type="button" class="btn btn-link btn-lg btn-block">{{ __('auth.register_subscriber') }}</a>
                        {{-- Register as a Hotelier --}}
                        <a href="{{ URL::route('register_hotelier') }}"type="button" class="btn btn-link btn-lg btn-block">{{ __('auth.register_hotelier') }}</a>
                    @endisset

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
