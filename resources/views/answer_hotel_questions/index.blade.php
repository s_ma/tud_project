@extends('layouts.app')

@section('title', __('answer_hotel_question.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\AnswerHotelQuestion)
            <a href="{{ route('answer_hotel_questions.create') }}" class="btn btn-success">{{ __('answer_hotel_question.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('answer_hotel_question.list') }} <small>{{ __('app.total') }} : {{ $answerHotelQuestions->total() }} {{ __('answer_hotel_question.answer_hotel_question') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('answer_hotel_question.search') }}</label>
                        <input placeholder="{{ __('answer_hotel_question.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('answer_hotel_question.search') }}" class="btn btn-secondary">
                    <a href="{{ route('answer_hotel_questions.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('answer_hotel_question.name') }}</th>
                        <th>{{ __('answer_hotel_question.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($answerHotelQuestions as $key => $answerHotelQuestion)
                    <tr>
                        <td class="text-center">{{ $answerHotelQuestions->firstItem() + $key }}</td>
                        <td>{!! $answerHotelQuestion->name_link !!}</td>
                        <td>{{ $answerHotelQuestion->description }}</td>
                        <td class="text-center">
                            @can('view', $answerHotelQuestion)
                                <a href="{{ route('answer_hotel_questions.show', $answerHotelQuestion) }}" id="show-answer_hotel_question-{{ $answerHotelQuestion->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $answerHotelQuestions->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
