@extends('layouts.app')

@section('title', __('answer_hotel_question.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $answerHotelQuestion)
        @can('delete', $answerHotelQuestion)
            <div class="card">
                <div class="card-header">{{ __('answer_hotel_question.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('answer_hotel_question.name') }}</label>
                    <p>{{ $answerHotelQuestion->name }}</p>
                    <label class="form-label text-primary">{{ __('answer_hotel_question.description') }}</label>
                    <p>{{ $answerHotelQuestion->description }}</p>
                    {!! $errors->first('answer_hotel_question_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('answer_hotel_question.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('answer_hotel_questions.destroy', $answerHotelQuestion) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="answer_hotel_question_id" type="hidden" value="{{ $answerHotelQuestion->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('answer_hotel_questions.edit', $answerHotelQuestion) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('answer_hotel_question.edit') }}</div>
            <form method="POST" action="{{ route('answer_hotel_questions.update', $answerHotelQuestion) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('answer_hotel_question.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $answerHotelQuestion->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('answer_hotel_question.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $answerHotelQuestion->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('answer_hotel_question.update') }}" class="btn btn-success">
                    <a href="{{ route('answer_hotel_questions.show', $answerHotelQuestion) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $answerHotelQuestion)
                        <a href="{{ route('answer_hotel_questions.edit', [$answerHotelQuestion, 'action' => 'delete']) }}" id="del-answer_hotel_question-{{ $answerHotelQuestion->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
