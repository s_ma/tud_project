@extends('layouts.app')

@section('title', __('answer_hotel_question.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('answer_hotel_question.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('answer_hotel_question.name') }}</td><td>{{ $answerHotelQuestion->name }}</td></tr>
                        <tr><td>{{ __('answer_hotel_question.description') }}</td><td>{{ $answerHotelQuestion->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $answerHotelQuestion)
                    <a href="{{ route('answer_hotel_questions.edit', $answerHotelQuestion) }}" id="edit-answer_hotel_question-{{ $answerHotelQuestion->id }}" class="btn btn-warning">{{ __('answer_hotel_question.edit') }}</a>
                @endcan
                <a href="{{ route('answer_hotel_questions.index') }}" class="btn btn-link">{{ __('answer_hotel_question.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
