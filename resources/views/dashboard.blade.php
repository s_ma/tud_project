@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-10">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><b>{{ __('dashboard.hotels') }}</b></h5>
                        <p class="card-text">{{ __('dashboard.hotels_description') }}</p>
                        <a href="{{ route('hotels.index') }}" class="btn btn-primary">{{ __('dashboard.hotels_button') }}</a>
                    </div>
                </div>
            </div>

            @if (Auth::guard('subscriber')->check())
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><b>{{ __('dashboard.account') }}</b></h5>
                            <p class="card-text">{{ __('dashboard.subscriber_account') }}</p>
                            <a href="{{ route('subscribers.show', Auth::guard('subscriber')->user()) }}" id="show-subscriber-{{ Auth::guard('subscriber')->user()->id }}" class="btn btn-primary">{{ __('dashboard.account_button') }}</a>
                        </div>
                    </div>
                </div>
            @elseif(Auth::guard('hotelier')->check())
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><b>{{ __('dashboard.account') }}</b></h5>
                            <p class="card-text">{{ __('dashboard.hotelier_account') }}</p>
                            <a href="{{ route('hoteliers.show', Auth::guard('hotelier')->user()) }}" id="show-hotelier-{{ Auth::guard('hotelier')->user()->id }}" class="btn btn-primary">{{ __('dashboard.account_button') }}</a>
                        </div>
                    </div>
                </div>
            @else
                
            @endif
        </div>

        @if(Auth::guard('hotelier')->check())
            <div class="row mt-5">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><b>{{ __('dashboard.personal_hotels') }}</b></h5>
                            <p class="card-text">{{ __('dashboard.personal_hotels_description') }}</p>
                            {{-- TODO: Set route (once defined) --}}
                            <a href="" class="btn btn-primary">{{ __('dashboard.personal_hotels_button') }}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

  
@endsection