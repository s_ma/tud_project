@extends('layouts.app')

@section('title', __('country.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $country)
        @can('delete', $country)
            <div class="card">
                <div class="card-header">{{ __('country.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('country.name') }}</label>
                    <p>{{ $country->name }}</p>
                    <label class="form-label text-primary">{{ __('country.description') }}</label>
                    <p>{{ $country->description }}</p>
                    {!! $errors->first('country_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('country.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('countries.destroy', $country) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="country_id" type="hidden" value="{{ $country->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('countries.edit', $country) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('country.edit') }}</div>
            <form method="POST" action="{{ route('countries.update', $country) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('country.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $country->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('country.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $country->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('country.update') }}" class="btn btn-success">
                    <a href="{{ route('countries.show', $country) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $country)
                        <a href="{{ route('countries.edit', [$country, 'action' => 'delete']) }}" id="del-country-{{ $country->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
