@extends('layouts.app')

@section('title', __('country.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\Country)
            <a href="{{ route('countries.create') }}" class="btn btn-success">{{ __('country.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('country.list') }} <small>{{ __('app.total') }} : {{ $countries->total() }} {{ __('country.country') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('country.search') }}</label>
                        <input placeholder="{{ __('country.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('country.search') }}" class="btn btn-secondary">
                    <a href="{{ route('countries.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('country.name') }}</th>
                        <th>{{ __('country.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countries as $key => $country)
                    <tr>
                        <td class="text-center">{{ $countries->firstItem() + $key }}</td>
                        <td>{!! $country->name_link !!}</td>
                        <td>{{ $country->description }}</td>
                        <td class="text-center">
                            @can('view', $country)
                                <a href="{{ route('countries.show', $country) }}" id="show-country-{{ $country->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $countries->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
