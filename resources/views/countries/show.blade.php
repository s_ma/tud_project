@extends('layouts.app')

@section('title', __('country.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('country.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('country.name') }}</td><td>{{ $country->name }}</td></tr>
                        <tr><td>{{ __('country.description') }}</td><td>{{ $country->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $country)
                    <a href="{{ route('countries.edit', $country) }}" id="edit-country-{{ $country->id }}" class="btn btn-warning">{{ __('country.edit') }}</a>
                @endcan
                <a href="{{ route('countries.index') }}" class="btn btn-link">{{ __('country.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
