@extends('layouts.app')

@section('title', __('hotel.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-5">
        <div class="card">
            <div class="card-header">{{ __('hotel.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <tbody>
                        <tr><td>{{ __('hotel.name') }}</td><td><b>{!! !empty($hotel->name) ? $hotel->name : '-' !!}</b></td></tr>              
                        <tr><td>{{ __('hotel.stars') }}</td><td>{!! !empty($hotel->stars) ? $hotel->stars : '-' !!}</td></tr>
                        <tr><td>{{ __('price_range.range') }}</td><td>{!! !empty($hotel->price_range->range) ? $hotel->price_range->range : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.description') }}</td><td>{!! !empty($hotel->description) ? $hotel->description : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.number_of_rooms') }}</td><td>{!! !empty($hotel->number_of_rooms) ? $hotel->number_of_rooms : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.city') }}</td><td>{!! !empty($hotel->city) ? $hotel->city : '-' !!}</td></tr>
                        <tr><td>{{ __('country.name') }}</td><td>{!! !empty($hotel->country->name) ? $hotel->country->name : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.address_1') }}</td><td>{!! !empty($hotel->address_1) ? $hotel->address_1 : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.address_2') }}</td><td>{!! !empty($hotel->address_2) ? $hotel->address_2 : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.postcode') }}</td><td>{!! !empty($hotel->postcode) ? $hotel->postcode : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.state') }}</td><td>{!! !empty($hotel->state) ? $hotel->state : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.latitude') }}</td><td>{!! !empty($hotel->latitude) ? $hotel->latitude : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.longitude') }}</td><td>{!! !empty($hotel->longitude) ? $hotel->longitude : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.website') }}</td><td>{!! !empty($hotel->website) ? $hotel->website : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.phone') }}</td><td>+{!! !empty($hotel->dialling_code) ? $hotel->dialling_code : '-' !!}{!! !empty($hotel->phone) ? $hotel->phone : '-' !!}</td></tr>
                        <tr><td>{{ __('hotel.email') }}</td><td>{!! !empty($hotel->email) ? $hotel->email : '-' !!}</td></tr>
                        
                        <tr><td>{{ __('alias.aliases') }}</td>
                            @if(!empty($hotel->aliases))
                                <td>
                                    <ul>
                                        @foreach($hotel->aliases as $alias)
                                            <li>{{ $alias->description }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            @else 
                                <td>{!! '-' !!}</td>
                            @endif
                        </tr>
                        <tr><td>{{ __('service.description') }}</td>
                            @if(!empty($hotel->services))
                                <td>
                                    <ul>
                                        @foreach($hotel->services as $srv)
                                            <li>{{ $srv->description }}</li>
                                        @endforeach
                                    </ul>
                                </td>
                            @else 
                                <td>{!! '-' !!}</td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                {{-- @can('update', $hotel) --}}
                    <a href="{{ route('hotels.edit', $hotel) }}" id="edit-hotel-{{ $hotel->id }}" class="btn btn-warning">{{ __('hotel.edit') }}</a>
                {{-- @endcan --}}
                <a href="{{ route('hotels.index') }}" class="btn btn-link">{{ __('hotel.back_to_index') }}</a>
                
                {{-- @can('delete', $hotel) --}}
                    <a href="{{ route('hotels.edit', [$hotel, 'action' => 'delete']) }}" id="del-hotel-{{ $hotel->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                {{-- @endcan --}}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotel.images') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>

                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>
@endsection
