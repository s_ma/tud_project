@extends('layouts.app')

@section('title', __('hotel.list'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-11">
        <div class="mb-4">
            <div class="float-right">
                {{-- @can('create', new App\Hotel) --}}
                    <a href="{{ route('hotels.create') }}" class="btn btn-success">{{ __('hotel.create') }}</a>
                {{-- @endcan --}}
            </div>
            <h1 class="page-title">{{ __('hotel.list') }} <small>{{ __('app.total') }} : {{ $hotels->total() }} {{ __('hotel.hotel') }}</small></h1>
        </div>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-md-11">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('hotel.search') }}</label>
                        <input placeholder="{{ __('hotel.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('hotel.search') }}" class="btn btn-secondary">
                    <a href="{{ route('hotels.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('hotel.name') }}</th>
                        <th>{{ __('hotel.stars') }}</th>
                        <th>{{ __('hotel.city') }}</th>
                        <th>{{ __('hotel.phone') }}</th>
                        <th>{{ __('hotel.email') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hotels as $key => $hotel)
                    <tr>
                        <td class="text-center">{{ $hotels->firstItem() + $key }}</td>
                        <td>{!! $hotel->name_link !!}</td>
                        <td>{{ $hotel->stars }}</td>
                        <td>{{ $hotel->city }}</td>
                        <td>+{{ $hotel->dialling_code }} {{ $hotel->phone }}</td>
                        <td>{{ $hotel->email }}</td>
                        <td class="text-center">
                            {{-- @can('view', $hotel) --}}
                                <a href="{{ route('hotels.show', $hotel) }}" id="show-hotel-{{ $hotel->id }}">{{ __('app.show') }}</a>
                            {{-- @endcan --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $hotels->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
