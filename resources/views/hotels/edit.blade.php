@extends('layouts.app')

@section('title', __('hotel.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $hotel)
        {{-- @can('delete', $hotel) --}}
            <div class="card">
                <div class="card-header">{{ __('hotel.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('hotel.id') }}</label>
                    <p>{{ $hotel->id }}</p>
                    <label class="form-label text-primary">{{ __('hotel.name') }}</label>
                    <p>{{ $hotel->name }}</p>
                    <label class="form-label text-primary">{{ __('hotel.city') }}</label>
                    <p>{{ $hotel->city }}</p>
                    <label class="form-label text-primary">{{ __('hotel.stars') }}</label>
                    <p>{{ $hotel->stars }}</p>
                    <label class="form-label text-primary">{{ __('hotel.owner') }}</label>
                    <p>{{ $hotel->hotelier->firstname }} {{ $hotel->hotelier->lastname }}</p>
                    {!! $errors->first('hotel_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('hotel.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('hotels.destroy', $hotel) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="hotel_id" type="hidden" value="{{ $hotel->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('hotels.edit', $hotel) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        {{-- @endcan --}}
        @else
        <div class="card">
            <div class="card-header">{{ __('hotel.edit') }}</div>
            <form method="POST" action="{{ route('hotels.update', $hotel) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('hotel.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $hotel->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('hotel.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $hotel->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="address_1" class="form-label">{{ __('hotel.address_1') }} <span class="form-required">*</span></label>
                        <input id="address_1" type="text" maxlength="100" class="form-control{{ $errors->has('address_1') ? ' is-invalid' : '' }}" name="address_1" value="{{ old('address_1', $hotel->address_1) }}" required>
                        {!! $errors->first('address_1', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="address_2" class="form-label">{{ __('hotel.address_2') }}</label>
                        <input id="address_2" type="text" maxlength="100" class="form-control{{ $errors->has('address_2') ? ' is-invalid' : '' }}" name="address_2" value="{{ old('address_2', $hotel->address_2) }}">
                        {!! $errors->first('address_2', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="postcode" class="form-label">{{ __('hotel.postcode') }} <span class="form-required">*</span></label>
                        <input id="postcode" type="text" maxlength="10" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" value="{{ old('postcode', $hotel->postcode) }}" required>
                        {!! $errors->first('postcode', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="city" class="form-label">{{ __('hotel.city') }} <span class="form-required">*</span></label>
                        <input id="city" type="text" maxlength="50" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city', $hotel->postcode) }}" required>
                        {!! $errors->first('city', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="state" class="form-label">{{ __('hotel.state') }}</label>
                        <input id="state" type="text" maxlength="50" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" value="{{ old('state', $hotel->state) }}">
                        {!! $errors->first('state', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="country_id" class="form-label">{{ __('country.country') }} <span class="form-required">*</span></label>
                        <select name="country_id" id="country_id" class="form-control{{ $errors->has('country_id') ? ' is-invalid' : '' }}" value="{{ old('country_id') }}" required>
                            <option value="">{{ __('country.choose') }}</option>
                            @foreach ($countries as $country)
                                <option value={{ $country->id }} {{ old('country_id', $hotel->country_id) == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('country_id', '<div class="invalid-feedback error" role="alert">:message</div>') !!}
                    </div>
                    <div class="form-group">
                        <label for="latitude" class="form-label">{{ __('hotel.latitude') }} <span class="form-required">*</span></label>
                        <input id="latitude" type="number" min="0" step="0.000001" class="form-control{{ $errors->has('latitude') ? ' is-invalid' : '' }}" name="latitude" value="{{ old('latitude', $hotel->latitude) }}" required>
                        {!! $errors->first('latitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="longitude" class="form-label">{{ __('hotel.longitude') }} <span class="form-required">*</span></label>
                        <input id="longitude" type="number" min="0" step="0.000001" class="form-control{{ $errors->has('longitude') ? ' is-invalid' : '' }}" name="longitude" value="{{ old('longitude', $hotel->longitude) }}" required>
                        {!! $errors->first('longitude', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="stars" class="form-label">{{ __('hotel.stars') }} <span class="form-required">*</span></label>
                        <input id="stars" type="number" min="0" max="5" class="form-control{{ $errors->has('stars') ? ' is-invalid' : '' }}" name="stars" value="{{ old('stars', $hotel->stars) }}" required>
                        {!! $errors->first('stars', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="price_range_id" class="form-label">{{ __('price_range.price_range') }} <span class="form-required">*</span></label>
                        <select name="price_range_id" id="price_range_id" class="form-control{{ $errors->has('price_range') ? ' is-invalid' : '' }}" value="{{ old('price_range') }}" required>
                            <option value="">--Please choose an option--</option>
                            @foreach ($price_ranges as $pr)
                                <option value={{ $pr->id }} {{ old('price_range_id', $hotel->price_range_id) == $pr->id ? 'selected' : '' }}>{{ $pr->range }}</option>
                            @endforeach
                        </select>
                        {!! $errors->first('price_range_id', '<div class="invalid-feedback error" role="alert">:message</div>') !!}
                    </div>
                    <div class="form-group">
                        <label for="dialling_code" class="form-label">{{ __('hotel.dialling_code') }} <span class="form-required">*</span></label>
                        <input id="dialling_code" type="number" min="0" class="form-control{{ $errors->has('dialling_code') ? ' is-invalid' : '' }}" name="dialling_code" value="{{ old('dialling_code', $hotel->dialling_code) }}" required>
                        {!! $errors->first('dialling_code', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="phone" class="form-label">{{ __('hotel.phone') }} <span class="form-required">*</span></label>
                        <input id="phone" type="text" maxlength="20" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $hotel->phone) }}" required>
                        {!! $errors->first('phone', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-label">{{ __('hotel.email') }} <span class="form-required">*</span></label>
                        <input id="email" type="email" maxlength="80" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $hotel->email) }}" required>
                        {!! $errors->first('email', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="website" class="form-label">{{ __('hotel.website') }}</label>
                        <input id="website" type="text" maxlength="100" class="form-control{{ $errors->has('website') ? ' is-invalid' : '' }}" name="website" value="{{ old('website', $hotel->website) }}">
                        {!! $errors->first('website', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="number_of_rooms" class="form-label">{{ __('hotel.number_of_rooms') }}</label>
                        <input id="number_of_rooms" type="number" min="0" class="form-control{{ $errors->has('number_of_rooms') ? ' is-invalid' : '' }}" name="number_of_rooms" value="{{ old('number_of_rooms', $hotel->number_of_rooms) }}">
                        {!! $errors->first('number_of_rooms', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('hotel.update') }}" class="btn btn-success">
                    <a href="{{ route('hotels.show', $hotel) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    {{-- @can('delete', $hotel) --}}
                        <a href="{{ route('hotels.edit', [$hotel, 'action' => 'delete']) }}" id="del-hotel-{{ $hotel->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    {{-- @endcan --}}
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
