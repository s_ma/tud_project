@extends('layouts.app')

@section('title', __('visit_type.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('visit_type.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('visit_type.name') }}</td><td>{{ $visitType->name }}</td></tr>
                        <tr><td>{{ __('visit_type.description') }}</td><td>{{ $visitType->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $visitType)
                    <a href="{{ route('visit_types.edit', $visitType) }}" id="edit-visit_type-{{ $visitType->id }}" class="btn btn-warning">{{ __('visit_type.edit') }}</a>
                @endcan
                <a href="{{ route('visit_types.index') }}" class="btn btn-link">{{ __('visit_type.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
