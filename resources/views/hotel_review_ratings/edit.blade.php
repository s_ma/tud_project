@extends('layouts.app')

@section('title', __('hotel_review_rating.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $hotelReviewRating)
        @can('delete', $hotelReviewRating)
            <div class="card">
                <div class="card-header">{{ __('hotel_review_rating.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('hotel_review_rating.name') }}</label>
                    <p>{{ $hotelReviewRating->name }}</p>
                    <label class="form-label text-primary">{{ __('hotel_review_rating.description') }}</label>
                    <p>{{ $hotelReviewRating->description }}</p>
                    {!! $errors->first('hotel_review_rating_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('hotel_review_rating.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('hotel_review_ratings.destroy', $hotelReviewRating) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="hotel_review_rating_id" type="hidden" value="{{ $hotelReviewRating->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('hotel_review_ratings.edit', $hotelReviewRating) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('hotel_review_rating.edit') }}</div>
            <form method="POST" action="{{ route('hotel_review_ratings.update', $hotelReviewRating) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('hotel_review_rating.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $hotelReviewRating->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('hotel_review_rating.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $hotelReviewRating->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('hotel_review_rating.update') }}" class="btn btn-success">
                    <a href="{{ route('hotel_review_ratings.show', $hotelReviewRating) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $hotelReviewRating)
                        <a href="{{ route('hotel_review_ratings.edit', [$hotelReviewRating, 'action' => 'delete']) }}" id="del-hotel_review_rating-{{ $hotelReviewRating->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
