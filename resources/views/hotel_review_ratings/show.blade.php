@extends('layouts.app')

@section('title', __('hotel_review_rating.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('hotel_review_rating.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('hotel_review_rating.name') }}</td><td>{{ $hotelReviewRating->name }}</td></tr>
                        <tr><td>{{ __('hotel_review_rating.description') }}</td><td>{{ $hotelReviewRating->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $hotelReviewRating)
                    <a href="{{ route('hotel_review_ratings.edit', $hotelReviewRating) }}" id="edit-hotel_review_rating-{{ $hotelReviewRating->id }}" class="btn btn-warning">{{ __('hotel_review_rating.edit') }}</a>
                @endcan
                <a href="{{ route('hotel_review_ratings.index') }}" class="btn btn-link">{{ __('hotel_review_rating.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
