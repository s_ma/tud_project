@extends('layouts.app')

@section('title', __('similar_hotel.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\SimilarHotel)
            <a href="{{ route('similar_hotels.create') }}" class="btn btn-success">{{ __('similar_hotel.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('similar_hotel.list') }} <small>{{ __('app.total') }} : {{ $similarHotels->total() }} {{ __('similar_hotel.similar_hotel') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('similar_hotel.search') }}</label>
                        <input placeholder="{{ __('similar_hotel.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('similar_hotel.search') }}" class="btn btn-secondary">
                    <a href="{{ route('similar_hotels.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('similar_hotel.name') }}</th>
                        <th>{{ __('similar_hotel.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($similarHotels as $key => $similarHotel)
                    <tr>
                        <td class="text-center">{{ $similarHotels->firstItem() + $key }}</td>
                        <td>{!! $similarHotel->name_link !!}</td>
                        <td>{{ $similarHotel->description }}</td>
                        <td class="text-center">
                            @can('view', $similarHotel)
                                <a href="{{ route('similar_hotels.show', $similarHotel) }}" id="show-similar_hotel-{{ $similarHotel->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $similarHotels->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
