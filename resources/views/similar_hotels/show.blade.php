@extends('layouts.app')

@section('title', __('similar_hotel.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('similar_hotel.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('similar_hotel.name') }}</td><td>{{ $similarHotel->name }}</td></tr>
                        <tr><td>{{ __('similar_hotel.description') }}</td><td>{{ $similarHotel->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $similarHotel)
                    <a href="{{ route('similar_hotels.edit', $similarHotel) }}" id="edit-similar_hotel-{{ $similarHotel->id }}" class="btn btn-warning">{{ __('similar_hotel.edit') }}</a>
                @endcan
                <a href="{{ route('similar_hotels.index') }}" class="btn btn-link">{{ __('similar_hotel.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
