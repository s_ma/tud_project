@extends('layouts.app')

@section('title', __('similar_hotel.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $similarHotel)
        @can('delete', $similarHotel)
            <div class="card">
                <div class="card-header">{{ __('similar_hotel.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('similar_hotel.name') }}</label>
                    <p>{{ $similarHotel->name }}</p>
                    <label class="form-label text-primary">{{ __('similar_hotel.description') }}</label>
                    <p>{{ $similarHotel->description }}</p>
                    {!! $errors->first('similar_hotel_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('similar_hotel.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('similar_hotels.destroy', $similarHotel) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="similar_hotel_id" type="hidden" value="{{ $similarHotel->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('similar_hotels.edit', $similarHotel) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('similar_hotel.edit') }}</div>
            <form method="POST" action="{{ route('similar_hotels.update', $similarHotel) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('similar_hotel.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $similarHotel->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('similar_hotel.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $similarHotel->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('similar_hotel.update') }}" class="btn btn-success">
                    <a href="{{ route('similar_hotels.show', $similarHotel) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $similarHotel)
                        <a href="{{ route('similar_hotels.edit', [$similarHotel, 'action' => 'delete']) }}" id="del-similar_hotel-{{ $similarHotel->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
