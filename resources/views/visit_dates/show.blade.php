@extends('layouts.app')

@section('title', __('visit_date.detail'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">{{ __('visit_date.detail') }}</div>
            <div class="card-body">
                <table class="table table-sm">
                    <tbody>
                        <tr><td>{{ __('visit_date.name') }}</td><td>{{ $visitDate->name }}</td></tr>
                        <tr><td>{{ __('visit_date.description') }}</td><td>{{ $visitDate->description }}</td></tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                @can('update', $visitDate)
                    <a href="{{ route('visit_dates.edit', $visitDate) }}" id="edit-visit_date-{{ $visitDate->id }}" class="btn btn-warning">{{ __('visit_date.edit') }}</a>
                @endcan
                <a href="{{ route('visit_dates.index') }}" class="btn btn-link">{{ __('visit_date.back_to_index') }}</a>
            </div>
        </div>
    </div>
</div>
@endsection
