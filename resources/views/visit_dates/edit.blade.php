@extends('layouts.app')

@section('title', __('visit_date.edit'))

@section('content')
<div class="row justify-content-center">
    <div class="col-md-6">
        @if (request('action') == 'delete' && $visitDate)
        @can('delete', $visitDate)
            <div class="card">
                <div class="card-header">{{ __('visit_date.delete') }}</div>
                <div class="card-body">
                    <label class="form-label text-primary">{{ __('visit_date.name') }}</label>
                    <p>{{ $visitDate->name }}</p>
                    <label class="form-label text-primary">{{ __('visit_date.description') }}</label>
                    <p>{{ $visitDate->description }}</p>
                    {!! $errors->first('visit_date_id', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                </div>
                <hr style="margin:0">
                <div class="card-body text-danger">{{ __('visit_date.delete_confirm') }}</div>
                <div class="card-footer">
                    <form method="POST" action="{{ route('visit_dates.destroy', $visitDate) }}" accept-charset="UTF-8" onsubmit="return confirm(&quot;{{ __('app.delete_confirm') }}&quot;)" class="del-form float-right" style="display: inline;">
                        {{ csrf_field() }} {{ method_field('delete') }}
                        <input name="visit_date_id" type="hidden" value="{{ $visitDate->id }}">
                        <button type="submit" class="btn btn-danger">{{ __('app.delete_confirm_button') }}</button>
                    </form>
                    <a href="{{ route('visit_dates.edit', $visitDate) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                </div>
            </div>
        @endcan
        @else
        <div class="card">
            <div class="card-header">{{ __('visit_date.edit') }}</div>
            <form method="POST" action="{{ route('visit_dates.update', $visitDate) }}" accept-charset="UTF-8">
                {{ csrf_field() }} {{ method_field('patch') }}
                <div class="card-body">
                    <div class="form-group">
                        <label for="name" class="form-label">{{ __('visit_date.name') }} <span class="form-required">*</span></label>
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $visitDate->name) }}" required>
                        {!! $errors->first('name', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                    <div class="form-group">
                        <label for="description" class="form-label">{{ __('visit_date.description') }}</label>
                        <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" rows="4">{{ old('description', $visitDate->description) }}</textarea>
                        {!! $errors->first('description', '<span class="invalid-feedback" role="alert">:message</span>') !!}
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" value="{{ __('visit_date.update') }}" class="btn btn-success">
                    <a href="{{ route('visit_dates.show', $visitDate) }}" class="btn btn-link">{{ __('app.cancel') }}</a>
                    @can('delete', $visitDate)
                        <a href="{{ route('visit_dates.edit', [$visitDate, 'action' => 'delete']) }}" id="del-visit_date-{{ $visitDate->id }}" class="btn btn-danger float-right">{{ __('app.delete') }}</a>
                    @endcan
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection
