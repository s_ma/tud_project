@extends('layouts.app')

@section('title', __('visit_date.list'))

@section('content')
<div class="mb-3">
    <div class="float-right">
        @can('create', new App\VisitDate)
            <a href="{{ route('visit_dates.create') }}" class="btn btn-success">{{ __('visit_date.create') }}</a>
        @endcan
    </div>
    <h1 class="page-title">{{ __('visit_date.list') }} <small>{{ __('app.total') }} : {{ $visitDates->total() }} {{ __('visit_date.visit_date') }}</small></h1>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <form method="GET" action="" accept-charset="UTF-8" class="form-inline">
                    <div class="form-group">
                        <label for="q" class="form-label">{{ __('visit_date.search') }}</label>
                        <input placeholder="{{ __('visit_date.search_text') }}" name="q" type="text" id="q" class="form-control mx-sm-2" value="{{ request('q') }}">
                    </div>
                    <input type="submit" value="{{ __('visit_date.search') }}" class="btn btn-secondary">
                    <a href="{{ route('visit_dates.index') }}" class="btn btn-link">{{ __('app.reset') }}</a>
                </form>
            </div>
            <table class="table table-sm table-responsive-sm table-hover">
                <thead>
                    <tr>
                        <th class="text-center">{{ __('app.table_no') }}</th>
                        <th>{{ __('visit_date.name') }}</th>
                        <th>{{ __('visit_date.description') }}</th>
                        <th class="text-center">{{ __('app.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($visitDates as $key => $visitDate)
                    <tr>
                        <td class="text-center">{{ $visitDates->firstItem() + $key }}</td>
                        <td>{!! $visitDate->name_link !!}</td>
                        <td>{{ $visitDate->description }}</td>
                        <td class="text-center">
                            @can('view', $visitDate)
                                <a href="{{ route('visit_dates.show', $visitDate) }}" id="show-visit_date-{{ $visitDate->id }}">{{ __('app.show') }}</a>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="card-body">{{ $visitDates->appends(Request::except('page'))->render() }}</div>
        </div>
    </div>
</div>
@endsection
