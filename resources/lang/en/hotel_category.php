<?php

return [
    // Labels
    'hotel_category'     => 'Hotel Category',
    'list'           => 'Hotel Category List',
    'search'         => 'Search Hotel Category',
    'search_text'    => 'Name ...',
    'all'            => 'All Hotel Category',
    'select'         => 'Select Hotel Category',
    'detail'         => 'Hotel Category Detail',
    'not_found'      => 'Hotel Category not found.',
    'empty'          => 'Hotel Category is empty.',
    'back_to_show'   => 'Back to Hotel Category Detail',
    'back_to_index'  => 'Back to Hotel Category List',

    // Actions
    'create'         => 'Create new Hotel Category',
    'created'        => 'A new Hotel Category has been created.',
    'show'           => 'View Hotel Category Detail',
    'edit'           => 'Edit Hotel Category',
    'update'         => 'Update Hotel Category',
    'updated'        => 'Hotel Category data has been updated.',
    'delete'         => 'Delete Hotel Category',
    'delete_confirm' => 'Are you sure to delete this Hotel Category?',
    'deleted'        => 'Hotel Category has been deleted.',
    'undeleted'      => 'Hotel Category not deleted.',
    'undeleteable'   => 'Hotel Category data cannot be deleted.',

    // Attributes
    'name'           => 'Hotel Category Name',
    'description'    => 'Hotel Category Description',
];
