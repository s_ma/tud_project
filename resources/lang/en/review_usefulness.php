<?php

return [
    // Labels
    'review_usefulness'     => 'Review Usefulness',
    'list'           => 'Review Usefulness List',
    'search'         => 'Search Review Usefulness',
    'search_text'    => 'Name ...',
    'all'            => 'All Review Usefulness',
    'select'         => 'Select Review Usefulness',
    'detail'         => 'Review Usefulness Detail',
    'not_found'      => 'Review Usefulness not found.',
    'empty'          => 'Review Usefulness is empty.',
    'back_to_show'   => 'Back to Review Usefulness Detail',
    'back_to_index'  => 'Back to Review Usefulness List',

    // Actions
    'create'         => 'Create new Review Usefulness',
    'created'        => 'A new Review Usefulness has been created.',
    'show'           => 'View Review Usefulness Detail',
    'edit'           => 'Edit Review Usefulness',
    'update'         => 'Update Review Usefulness',
    'updated'        => 'Review Usefulness data has been updated.',
    'delete'         => 'Delete Review Usefulness',
    'delete_confirm' => 'Are you sure to delete this Review Usefulness?',
    'deleted'        => 'Review Usefulness has been deleted.',
    'undeleted'      => 'Review Usefulness not deleted.',
    'undeleteable'   => 'Review Usefulness data cannot be deleted.',

    // Attributes
    'name'           => 'Review Usefulness Name',
    'description'    => 'Review Usefulness Description',
];
