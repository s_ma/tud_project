<?php

return [
    // Labels
    'visit_date'     => 'Visit Date',
    'list'           => 'Visit Date List',
    'search'         => 'Search Visit Date',
    'search_text'    => 'Name ...',
    'all'            => 'All Visit Date',
    'select'         => 'Select Visit Date',
    'detail'         => 'Visit Date Detail',
    'not_found'      => 'Visit Date not found.',
    'empty'          => 'Visit Date is empty.',
    'back_to_show'   => 'Back to Visit Date Detail',
    'back_to_index'  => 'Back to Visit Date List',

    // Actions
    'create'         => 'Create new Visit Date',
    'created'        => 'A new Visit Date has been created.',
    'show'           => 'View Visit Date Detail',
    'edit'           => 'Edit Visit Date',
    'update'         => 'Update Visit Date',
    'updated'        => 'Visit Date data has been updated.',
    'delete'         => 'Delete Visit Date',
    'delete_confirm' => 'Are you sure to delete this Visit Date?',
    'deleted'        => 'Visit Date has been deleted.',
    'undeleted'      => 'Visit Date not deleted.',
    'undeleteable'   => 'Visit Date data cannot be deleted.',

    // Attributes
    'name'           => 'Visit Date Name',
    'description'    => 'Visit Date Description',
];
