<?php

return [
    // Labels
    'hotel_question'     => 'Hotel Question',
    'list'           => 'Hotel Question List',
    'search'         => 'Search Hotel Question',
    'search_text'    => 'Name ...',
    'all'            => 'All Hotel Question',
    'select'         => 'Select Hotel Question',
    'detail'         => 'Hotel Question Detail',
    'not_found'      => 'Hotel Question not found.',
    'empty'          => 'Hotel Question is empty.',
    'back_to_show'   => 'Back to Hotel Question Detail',
    'back_to_index'  => 'Back to Hotel Question List',

    // Actions
    'create'         => 'Create new Hotel Question',
    'created'        => 'A new Hotel Question has been created.',
    'show'           => 'View Hotel Question Detail',
    'edit'           => 'Edit Hotel Question',
    'update'         => 'Update Hotel Question',
    'updated'        => 'Hotel Question data has been updated.',
    'delete'         => 'Delete Hotel Question',
    'delete_confirm' => 'Are you sure to delete this Hotel Question?',
    'deleted'        => 'Hotel Question has been deleted.',
    'undeleted'      => 'Hotel Question not deleted.',
    'undeleteable'   => 'Hotel Question data cannot be deleted.',

    // Attributes
    'name'           => 'Hotel Question Name',
    'description'    => 'Hotel Question Description',
];
