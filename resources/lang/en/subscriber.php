<?php

return [
    // Labels
    'subscriber'        => 'Subscriber',
    'list'              => 'Subscriber List',
    'search'            => 'Search Subscriber',
    'search_text'       => 'Name ...',
    'all'               => 'All Subscriber',
    'select'            => 'Select Subscriber',
    'detail'            => 'Subscriber Detail',
    'not_found'         => 'Subscriber not found.',
    'empty'             => 'Subscriber is empty.',
    'back_to_show'      => 'Back to Subscriber Detail',
    'back_to_index'     => 'Back to Subscriber List',

    // Actions  
    'create'            => 'Create new Subscriber',
    'created'           => 'A new Subscriber has been created.',
    'show'              => 'View Subscriber Detail',
    'edit'              => 'Edit Subscriber',
    'update'            => 'Update Subscriber',
    'updated'           => 'Subscriber data has been updated.',
    'delete'            => 'Delete Subscriber',
    'delete_confirm'    => 'Are you sure to delete this Subscriber?',
    'deleted'           => 'Subscriber has been deleted.',
    'undeleted'         => 'Subscriber not deleted.',
    'undeleteable'      => 'Subscriber data cannot be deleted.',

    // Attributes   
    'name'              => 'Subscriber Name',
    'firstname'         => 'Firstname',
    'lastname'          => 'Lastname',
    'email'             => 'Email',
    'address_1'         => 'Address',
    'address_2'         => 'Address Complement',
    'postcode'          => 'Postcode',
    'city'              => 'City',
    'state'             => 'State',
    'description'       => 'Subscriber Description',
    'latitude'          => 'Latitude',
    'longitude'         => 'Longitude',
    'dialling_code'     => 'Dialling code',
    'phone'             => 'Phone number',
    'airport'           => 'Reference Airport',
    'pseudo'            => 'Pseudo',
];
