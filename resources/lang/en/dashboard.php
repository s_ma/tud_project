<?php

return [
    // Labels
    'hotels'     => 'Hotels',
    'hotels_description'     => 'The latest reviews. The lowest prices.',
    'hotels_button' => 'Find Hotels',

    'account'    => 'My Account',
    'hotelier_account'  => 'Manage your Hotelier account',
    'subscriber_account'  => 'Manage your Subscriber account',
    'account_button' => 'Manage Account',
    
    'personal_hotels'   => 'My Hotels',
    'personal_hotels_description' => 'View and manage your Hotels',
    'personal_hotels_button' => 'Manage Hotels',
    
    // Actions
    

];
