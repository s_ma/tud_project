<?php

return [
    // Labels
    'favourite'     => 'Favourite',
    'list'           => 'Favourite List',
    'search'         => 'Search Favourite',
    'search_text'    => 'Name ...',
    'all'            => 'All Favourite',
    'select'         => 'Select Favourite',
    'detail'         => 'Favourite Detail',
    'not_found'      => 'Favourite not found.',
    'empty'          => 'Favourite is empty.',
    'back_to_show'   => 'Back to Favourite Detail',
    'back_to_index'  => 'Back to Favourite List',

    // Actions
    'create'         => 'Create new Favourite',
    'created'        => 'A new Favourite has been created.',
    'show'           => 'View Favourite Detail',
    'edit'           => 'Edit Favourite',
    'update'         => 'Update Favourite',
    'updated'        => 'Favourite data has been updated.',
    'delete'         => 'Delete Favourite',
    'delete_confirm' => 'Are you sure to delete this Favourite?',
    'deleted'        => 'Favourite has been deleted.',
    'undeleted'      => 'Favourite not deleted.',
    'undeleteable'   => 'Favourite data cannot be deleted.',

    // Attributes
    'name'           => 'Favourite Name',
    'description'    => 'Favourite Description',
];
