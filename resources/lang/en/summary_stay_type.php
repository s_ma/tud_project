<?php

return [
    // Labels
    'summary_stay_type'     => 'Summary Stay Type',
    'list'           => 'Summary Stay Type List',
    'search'         => 'Search Summary Stay Type',
    'search_text'    => 'Name ...',
    'all'            => 'All Summary Stay Type',
    'select'         => 'Select Summary Stay Type',
    'detail'         => 'Summary Stay Type Detail',
    'not_found'      => 'Summary Stay Type not found.',
    'empty'          => 'Summary Stay Type is empty.',
    'back_to_show'   => 'Back to Summary Stay Type Detail',
    'back_to_index'  => 'Back to Summary Stay Type List',

    // Actions
    'create'         => 'Create new Summary Stay Type',
    'created'        => 'A new Summary Stay Type has been created.',
    'show'           => 'View Summary Stay Type Detail',
    'edit'           => 'Edit Summary Stay Type',
    'update'         => 'Update Summary Stay Type',
    'updated'        => 'Summary Stay Type data has been updated.',
    'delete'         => 'Delete Summary Stay Type',
    'delete_confirm' => 'Are you sure to delete this Summary Stay Type?',
    'deleted'        => 'Summary Stay Type has been deleted.',
    'undeleted'      => 'Summary Stay Type not deleted.',
    'undeleteable'   => 'Summary Stay Type data cannot be deleted.',

    // Attributes
    'name'           => 'Summary Stay Type Name',
    'description'    => 'Summary Stay Type Description',
];
