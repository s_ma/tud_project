<?php

return [
    // Labels
    'hotel_review_rating'     => 'Hotel Review Rating',
    'list'           => 'Hotel Review Rating List',
    'search'         => 'Search Hotel Review Rating',
    'search_text'    => 'Name ...',
    'all'            => 'All Hotel Review Rating',
    'select'         => 'Select Hotel Review Rating',
    'detail'         => 'Hotel Review Rating Detail',
    'not_found'      => 'Hotel Review Rating not found.',
    'empty'          => 'Hotel Review Rating is empty.',
    'back_to_show'   => 'Back to Hotel Review Rating Detail',
    'back_to_index'  => 'Back to Hotel Review Rating List',

    // Actions
    'create'         => 'Create new Hotel Review Rating',
    'created'        => 'A new Hotel Review Rating has been created.',
    'show'           => 'View Hotel Review Rating Detail',
    'edit'           => 'Edit Hotel Review Rating',
    'update'         => 'Update Hotel Review Rating',
    'updated'        => 'Hotel Review Rating data has been updated.',
    'delete'         => 'Delete Hotel Review Rating',
    'delete_confirm' => 'Are you sure to delete this Hotel Review Rating?',
    'deleted'        => 'Hotel Review Rating has been deleted.',
    'undeleted'      => 'Hotel Review Rating not deleted.',
    'undeleteable'   => 'Hotel Review Rating data cannot be deleted.',

    // Attributes
    'name'           => 'Hotel Review Rating Name',
    'description'    => 'Hotel Review Rating Description',
];
