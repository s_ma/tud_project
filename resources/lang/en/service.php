<?php

return [
    // Labels
    'service'     => 'Service',
    'list'           => 'Service List',
    'search'         => 'Search Service',
    'search_text'    => 'Name ...',
    'all'            => 'All Service',
    'select'         => 'Select Service',
    'detail'         => 'Service Detail',
    'not_found'      => 'Service not found.',
    'empty'          => 'Service is empty.',
    'back_to_show'   => 'Back to Service Detail',
    'back_to_index'  => 'Back to Service List',

    // Actions
    'create'         => 'Create new Service',
    'created'        => 'A new Service has been created.',
    'show'           => 'View Service Detail',
    'edit'           => 'Edit Service',
    'update'         => 'Update Service',
    'updated'        => 'Service data has been updated.',
    'delete'         => 'Delete Service',
    'delete_confirm' => 'Are you sure to delete this Service?',
    'deleted'        => 'Service has been deleted.',
    'undeleted'      => 'Service not deleted.',
    'undeleteable'   => 'Service data cannot be deleted.',

    // Attributes
    'name'           => 'Service Name',
    'description'    => 'Hotel services',
];
