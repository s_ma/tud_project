<?php

return [
    // Labels
    'similar_hotel'     => 'Similar Hotel',
    'list'           => 'Similar Hotel List',
    'search'         => 'Search Similar Hotel',
    'search_text'    => 'Name ...',
    'all'            => 'All Similar Hotel',
    'select'         => 'Select Similar Hotel',
    'detail'         => 'Similar Hotel Detail',
    'not_found'      => 'Similar Hotel not found.',
    'empty'          => 'Similar Hotel is empty.',
    'back_to_show'   => 'Back to Similar Hotel Detail',
    'back_to_index'  => 'Back to Similar Hotel List',

    // Actions
    'create'         => 'Create new Similar Hotel',
    'created'        => 'A new Similar Hotel has been created.',
    'show'           => 'View Similar Hotel Detail',
    'edit'           => 'Edit Similar Hotel',
    'update'         => 'Update Similar Hotel',
    'updated'        => 'Similar Hotel data has been updated.',
    'delete'         => 'Delete Similar Hotel',
    'delete_confirm' => 'Are you sure to delete this Similar Hotel?',
    'deleted'        => 'Similar Hotel has been deleted.',
    'undeleted'      => 'Similar Hotel not deleted.',
    'undeleteable'   => 'Similar Hotel data cannot be deleted.',

    // Attributes
    'name'           => 'Similar Hotel Name',
    'description'    => 'Similar Hotel Description',
];
