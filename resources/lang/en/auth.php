<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'login_hotelier' => 'Login as a Hotelier',
    'login_subscriber' => 'Login as a Subscriber',

    'register_hotelier' => 'Register as a Hotelier',
    'register_subscriber' => 'Register as a Subscriber',
];
