<?php

return [
    // Labels
    'hotel_review_type'     => 'Hotel Review Type',
    'list'           => 'Hotel Review Type List',
    'search'         => 'Search Hotel Review Type',
    'search_text'    => 'Name ...',
    'all'            => 'All Hotel Review Type',
    'select'         => 'Select Hotel Review Type',
    'detail'         => 'Hotel Review Type Detail',
    'not_found'      => 'Hotel Review Type not found.',
    'empty'          => 'Hotel Review Type is empty.',
    'back_to_show'   => 'Back to Hotel Review Type Detail',
    'back_to_index'  => 'Back to Hotel Review Type List',

    // Actions
    'create'         => 'Create new Hotel Review Type',
    'created'        => 'A new Hotel Review Type has been created.',
    'show'           => 'View Hotel Review Type Detail',
    'edit'           => 'Edit Hotel Review Type',
    'update'         => 'Update Hotel Review Type',
    'updated'        => 'Hotel Review Type data has been updated.',
    'delete'         => 'Delete Hotel Review Type',
    'delete_confirm' => 'Are you sure to delete this Hotel Review Type?',
    'deleted'        => 'Hotel Review Type has been deleted.',
    'undeleted'      => 'Hotel Review Type not deleted.',
    'undeleteable'   => 'Hotel Review Type data cannot be deleted.',

    // Attributes
    'name'           => 'Hotel Review Type Name',
    'description'    => 'Hotel Review Type Description',
];
