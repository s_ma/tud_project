<?php

return [
    // Labels
    'price_range'     => 'Price Range',
    'list'           => 'Price Range List',
    'search'         => 'Search Price Range',
    'search_text'    => 'Name ...',
    'all'            => 'All Price Range',
    'select'         => 'Select Price Range',
    'detail'         => 'Price Range Detail',
    'not_found'      => 'Price Range not found.',
    'empty'          => 'Price Range is empty.',
    'back_to_show'   => 'Back to Price Range Detail',
    'back_to_index'  => 'Back to Price Range List',

    // Actions
    'create'         => 'Create new Price Range',
    'created'        => 'A new Price Range has been created.',
    'show'           => 'View Price Range Detail',
    'edit'           => 'Edit Price Range',
    'update'         => 'Update Price Range',
    'updated'        => 'Price Range data has been updated.',
    'delete'         => 'Delete Price Range',
    'delete_confirm' => 'Are you sure to delete this Price Range?',
    'deleted'        => 'Price Range has been deleted.',
    'undeleted'      => 'Price Range not deleted.',
    'undeleteable'   => 'Price Range data cannot be deleted.',

    // Attributes
    'range'           => 'Price Range',
    'description'    => 'Price Range Description',
];
