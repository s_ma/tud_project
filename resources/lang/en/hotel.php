<?php

return [
    // Labels
    'hotel'             => 'Hotel',
    'list'              => 'Hotel List',
    'search'            => 'Search Hotel',
    'search_text'       => 'Name ...',
    'all'               => 'All Hotel',
    'select'            => 'Select Hotel',
    'detail'            => 'Hotel Detail',
    'not_found'         => 'Hotel not found.',
    'empty'             => 'Hotel is empty.',
    'back_to_show'      => 'Back to Hotel Detail',
    'back_to_index'     => 'Back to Hotel List',
    
    // Actions  
    'create'            => 'Create new Hotel',
    'created'           => 'A new Hotel has been created.',
    'show'              => 'View Hotel Detail',
    'edit'              => 'Edit Hotel',
    'update'            => 'Update Hotel',
    'updated'           => 'Hotel data has been updated.',
    'delete'            => 'Delete Hotel',
    'delete_confirm'    => 'Are you sure to delete this Hotel?',
    'deleted'           => 'Hotel has been deleted.',
    'undeleted'         => 'Hotel not deleted.',
    'undeleteable'      => 'Hotel data cannot be deleted.',
    
    // Attributes   
    'id'                => 'Hotel ID',
    'name'              => 'Hotel Name',
    'description'       => 'Hotel Description',
    'address_1'         => 'Address',
    'address_2'         => 'Address complement',
    'postcode'          => 'Postcode',
    'city'              => 'Hotel City',
    'state'             => 'State',
    'latitude'          => 'Latitude',
    'longitude'         => 'Longitude',
    'stars'             => 'Stars',
    'dialling_code'     => 'Dialling code',
    'phone'             => 'Phone number',
    'email'             => 'Email address',
    'website'           => 'Website',
    'number_of_rooms'   => 'Rooms',
    'images'            => 'Hotel Pictures (TODO)',
    
    // Miscellaneous    
    'owner'             => 'Hotel owner',
    
    
];
