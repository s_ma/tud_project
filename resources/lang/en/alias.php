<?php

return [
    // Labels
    'alias'          => 'Alias',
    'aliases'        => 'Hotel aliases',
    'list'           => 'Alias List',
    'search'         => 'Search Alias',
    'search_text'    => 'Name ...',
    'all'            => 'All Alias',
    'select'         => 'Select Alias',
    'detail'         => 'Alias Detail',
    'not_found'      => 'Alias not found.',
    'empty'          => 'Alias is empty.',
    'back_to_show'   => 'Back to Alias Detail',
    'back_to_index'  => 'Back to Alias List',

    // Actions
    'create'         => 'Create new Alias',
    'created'        => 'A new Alias has been created.',
    'show'           => 'View Alias Detail',
    'edit'           => 'Edit Alias',
    'update'         => 'Update Alias',
    'updated'        => 'Alias data has been updated.',
    'delete'         => 'Delete Alias',
    'delete_confirm' => 'Are you sure to delete this Alias?',
    'deleted'        => 'Alias has been deleted.',
    'undeleted'      => 'Alias not deleted.',
    'undeleteable'   => 'Alias data cannot be deleted.',

    // Attributes
    'name'           => 'Alias Name',
    'description'    => 'Alias Description',
];
