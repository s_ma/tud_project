<?php

return [
    // Labels
    'visit_type'     => 'Visit Type',
    'list'           => 'Visit Type List',
    'search'         => 'Search Visit Type',
    'search_text'    => 'Name ...',
    'all'            => 'All Visit Type',
    'select'         => 'Select Visit Type',
    'detail'         => 'Visit Type Detail',
    'not_found'      => 'Visit Type not found.',
    'empty'          => 'Visit Type is empty.',
    'back_to_show'   => 'Back to Visit Type Detail',
    'back_to_index'  => 'Back to Visit Type List',

    // Actions
    'create'         => 'Create new Visit Type',
    'created'        => 'A new Visit Type has been created.',
    'show'           => 'View Visit Type Detail',
    'edit'           => 'Edit Visit Type',
    'update'         => 'Update Visit Type',
    'updated'        => 'Visit Type data has been updated.',
    'delete'         => 'Delete Visit Type',
    'delete_confirm' => 'Are you sure to delete this Visit Type?',
    'deleted'        => 'Visit Type has been deleted.',
    'undeleted'      => 'Visit Type not deleted.',
    'undeleteable'   => 'Visit Type data cannot be deleted.',

    // Attributes
    'name'           => 'Visit Type Name',
    'description'    => 'Visit Type Description',
];
