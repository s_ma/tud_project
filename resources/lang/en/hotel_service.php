<?php

return [
    // Labels
    'hotel_service'     => 'Hotel Service',
    'list'           => 'Hotel Service List',
    'search'         => 'Search Hotel Service',
    'search_text'    => 'Name ...',
    'all'            => 'All Hotel Service',
    'select'         => 'Select Hotel Service',
    'detail'         => 'Hotel Service Detail',
    'not_found'      => 'Hotel Service not found.',
    'empty'          => 'Hotel Service is empty.',
    'back_to_show'   => 'Back to Hotel Service Detail',
    'back_to_index'  => 'Back to Hotel Service List',

    // Actions
    'create'         => 'Create new Hotel Service',
    'created'        => 'A new Hotel Service has been created.',
    'show'           => 'View Hotel Service Detail',
    'edit'           => 'Edit Hotel Service',
    'update'         => 'Update Hotel Service',
    'updated'        => 'Hotel Service data has been updated.',
    'delete'         => 'Delete Hotel Service',
    'delete_confirm' => 'Are you sure to delete this Hotel Service?',
    'deleted'        => 'Hotel Service has been deleted.',
    'undeleted'      => 'Hotel Service not deleted.',
    'undeleteable'   => 'Hotel Service data cannot be deleted.',

    // Attributes
    'name'           => 'Hotel Service Name',
    'description'    => 'Hotel Service Description',
];
