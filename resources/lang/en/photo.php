<?php

return [
    // Labels
    'photo'          => 'Photo',
    'list'           => 'Photo List',
    'search'         => 'Search Photo',
    'search_text'    => 'Name ...',
    'all'            => 'All Photo',
    'select'         => 'Select Photo',
    'detail'         => 'Photo Detail',
    'not_found'      => 'Photo not found.',
    'empty'          => 'Photo is empty.',
    'back_to_show'   => 'Back to Photo Detail',
    'back_to_index'  => 'Back to Photo List',

    // Actions
    'create'         => 'Create new Photo',
    'created'        => 'A new Photo has been created.',
    'show'           => 'View Photo Detail',
    'edit'           => 'Edit Photo',
    'update'         => 'Update Photo',
    'updated'        => 'Photo data has been updated.',
    'delete'         => 'Delete Photo',
    'delete_confirm' => 'Are you sure to delete this Photo?',
    'deleted'        => 'Photo has been deleted.',
    'undeleted'      => 'Photo not deleted.',
    'undeleteable'   => 'Photo data cannot be deleted.',

    // Attributes
    'name'           => 'Photo Name',
    'description'    => 'Photo Description',
];
