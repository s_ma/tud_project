<?php

return [
    // Labels
    'review'     => 'Review',
    'list'           => 'Review List',
    'search'         => 'Search Review',
    'search_text'    => 'Name ...',
    'all'            => 'All Review',
    'select'         => 'Select Review',
    'detail'         => 'Review Detail',
    'not_found'      => 'Review not found.',
    'empty'          => 'Review is empty.',
    'back_to_show'   => 'Back to Review Detail',
    'back_to_index'  => 'Back to Review List',

    // Actions
    'create'         => 'Create new Review',
    'created'        => 'A new Review has been created.',
    'show'           => 'View Review Detail',
    'edit'           => 'Edit Review',
    'update'         => 'Update Review',
    'updated'        => 'Review data has been updated.',
    'delete'         => 'Delete Review',
    'delete_confirm' => 'Are you sure to delete this Review?',
    'deleted'        => 'Review has been deleted.',
    'undeleted'      => 'Review not deleted.',
    'undeleteable'   => 'Review data cannot be deleted.',

    // Attributes
    'name'           => 'Review Name',
    'description'    => 'Review Description',
];
