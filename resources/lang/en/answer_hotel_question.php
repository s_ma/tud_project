<?php

return [
    // Labels
    'answer_hotel_question'     => 'Answer Hotel Question',
    'list'           => 'Answer Hotel Question List',
    'search'         => 'Search Answer Hotel Question',
    'search_text'    => 'Name ...',
    'all'            => 'All Answer Hotel Question',
    'select'         => 'Select Answer Hotel Question',
    'detail'         => 'Answer Hotel Question Detail',
    'not_found'      => 'Answer Hotel Question not found.',
    'empty'          => 'Answer Hotel Question is empty.',
    'back_to_show'   => 'Back to Answer Hotel Question Detail',
    'back_to_index'  => 'Back to Answer Hotel Question List',

    // Actions
    'create'         => 'Create new Answer Hotel Question',
    'created'        => 'A new Answer Hotel Question has been created.',
    'show'           => 'View Answer Hotel Question Detail',
    'edit'           => 'Edit Answer Hotel Question',
    'update'         => 'Update Answer Hotel Question',
    'updated'        => 'Answer Hotel Question data has been updated.',
    'delete'         => 'Delete Answer Hotel Question',
    'delete_confirm' => 'Are you sure to delete this Answer Hotel Question?',
    'deleted'        => 'Answer Hotel Question has been deleted.',
    'undeleted'      => 'Answer Hotel Question not deleted.',
    'undeleteable'   => 'Answer Hotel Question data cannot be deleted.',

    // Attributes
    'name'           => 'Answer Hotel Question Name',
    'description'    => 'Answer Hotel Question Description',
];
