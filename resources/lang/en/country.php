<?php

return [
    // Labels
    'country'     => 'Country',
    'list'           => 'Country List',
    'search'         => 'Search Country',
    'search_text'    => 'Name ...',
    'all'            => 'All Country',
    'select'         => 'Select Country',
    'detail'         => 'Country Detail',
    'not_found'      => 'Country not found.',
    'empty'          => 'Country is empty.',
    'back_to_show'   => 'Back to Country Detail',
    'back_to_index'  => 'Back to Country List',

    // Actions
    'create'         => 'Create new Country',
    'created'        => 'A new Country has been created.',
    'show'           => 'View Country Detail',
    'edit'           => 'Edit Country',
    'update'         => 'Update Country',
    'updated'        => 'Country data has been updated.',
    'delete'         => 'Delete Country',
    'delete_confirm' => 'Are you sure to delete this Country?',
    'deleted'        => 'Country has been deleted.',
    'undeleted'      => 'Country not deleted.',
    'undeleteable'   => 'Country data cannot be deleted.',
    'choose'         => '--Please choose an option--',

    // Attributes
    'name'           => 'Country',
    'description'    => 'Country Description',
];
