<?php

return [
    // Labels
    'hotelier'          => 'Hotelier',
    'name'              => 'Name',
    'list'              => 'Hotelier List',
    'search'            => 'Search Hotelier',
    'search_text'       => 'Lastname',
    'all'               => 'All Hotelier',
    'select'            => 'Select Hotelier',
    'detail'            => 'Hotelier Detail',
    'not_found'         => 'Hotelier not found.',
    'empty'             => 'Hotelier is empty.',
    'back_to_show'      => 'Back to Hotelier Detail',
    'back_to_index'     => 'Back to Hotelier List',

    // Actions  
    'create'            => 'Create new Hotelier',
    'created'           => 'A new Hotelier has been created.',
    'show'              => 'View Hotelier Detail',
    'edit'              => 'Edit Hotelier',
    'update'            => 'Update Hotelier',
    'updated'           => 'Hotelier data has been updated.',
    'delete'            => 'Delete Hotelier',
    'delete_confirm'    => 'Are you sure to delete this Hotelier?',
    'deleted'           => 'Hotelier has been deleted.',
    'undeleted'         => 'Hotelier not deleted.',
    'undeleteable'      => 'Hotelier data cannot be deleted.',
    'confirm_password'  => 'Confirm password',

    // Attributes
    'email'             => 'E-Mail Address',
    'password'          => 'Password',
    'lastname'          => 'Lastname',
    'firstname'         => 'Firstname',
    'address_1'         => 'Address',
    'address_2'         => 'Address Complement',
    'postcode'          => 'Postcode',
    'city'              => 'City',
    'state'             => 'State',

];
