<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class VisitDate extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('visit_date.visit_date'),
        ]);
        $link = '<a href="'.route('visit_dates.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Reviews for the VisitDate.
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
