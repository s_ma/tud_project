<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class AnswerHotelQuestion extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('answer_hotel_question.answer_hotel_question'),
        ]);
        $link = '<a href="'.route('answer_hotel_questions.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
