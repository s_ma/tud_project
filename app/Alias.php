<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Alias extends Model
{
    protected $fillable = ['hotel_id', 'description'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('alias.alias'),
        ]);
        $link = '<a href="'.route('aliases.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Hotel associated with the Alias.
     */
    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }
}
