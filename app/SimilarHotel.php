<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SimilarHotel extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('similar_hotel.similar_hotel'),
        ]);
        $link = '<a href="'.route('similar_hotels.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
