<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\VisitType' => 'App\Policies\VisitTypePolicy',
        'App\HotelReviewType' => 'App\Policies\HotelReviewTypePolicy',
        'App\Service' => 'App\Policies\ServicePolicy',
        'App\HotelQuestion' => 'App\Policies\HotelQuestionPolicy',
        'App\VisitDate' => 'App\Policies\VisitDatePolicy',
        'App\Country' => 'App\Policies\CountryPolicy',
        'App\PriceRange' => 'App\Policies\PriceRangePolicy',
        'App\HotelCategory' => 'App\Policies\HotelCategoryPolicy',
        'App\ReviewUsefulness' => 'App\Policies\ReviewUsefulnessPolicy',
        'App\HotelService' => 'App\Policies\HotelServicePolicy',
        'App\SummaryStayType' => 'App\Policies\SummaryStayTypePolicy',
        'App\AnswerHotelQuestion' => 'App\Policies\AnswerHotelQuestionPolicy',
        'App\HotelReviewRating' => 'App\Policies\HotelReviewRatingPolicy',
        'App\SimilarHotel' => 'App\Policies\SimilarHotelPolicy',
        'App\Favourite' => 'App\Policies\FavouritePolicy',
        'App\Photo' => 'App\Policies\PhotoPolicy',
        'App\Hotel' => 'App\Policies\HotelPolicy',
        'App\Hotelier' => 'App\Policies\HotelierPolicy',
        'App\Review' => 'App\Policies\ReviewPolicy',
        'App\Alias' => 'App\Policies\AliasPolicy',
        'App\Subscriber' => 'App\Policies\SubscriberPolicy',
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
