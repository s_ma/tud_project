<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class HotelReviewType extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('hotel_review_type.hotel_review_type'),
        ]);
        $link = '<a href="'.route('hotel_review_types.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The Reviews that belong to the HotelReviewType.
     */
    public function reviews()
    {
        return $this->belongsToMany(Review::class, 'hotel_review_ratings');
    }
}
