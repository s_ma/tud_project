<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hotelier_id',
        'price_range_id',
        'name', 
        'description', 
        'address_1',
        'address_2',
        'postcode',
        'city',
        'state',
        'country_id',
        'latitude',
        'longitude',
        'stars',
        'dialling_code',
        'phone',
        'email',
        'website',
        'number_of_rooms'
    ];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('hotel.hotel'),
        ]);
        $link = '<a href="'.route('hotels.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the price range associated with the Hotel.
     */
    public function price_range()
    {
        return $this->belongsTo(PriceRange::class);
    }

    /**
     * Get the Country associated with the Hotel.
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Get the Hotelier associated with the Hotel.
     */
    public function hotelier()
    {
        return $this->belongsTo(Hotelier::class);
    }

    /**
     * Get the HotelCategory associated with the Hotel.
     */
    public function hotel_category()
    {
        return $this->belongsTo(HotelCategory::class);
    }

    /**
     * The Services that belong to the Hotel.
     */
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    /**
     * The Similar Hotels that belong to the Hotel. (reflexive relationship) 
     */
    public function hotels()
    {
        return $this->belongsToMany(Hotel::class, 'similar_hotels');
    }

    /**
     * The Subscribers that belong to the Hotel.
     */
    public function subscribers()
    {
        return $this->belongsToMany(Subscriber::class, 'favourites');
    }

    /**
     * The VisitTypes that belong to the Hotel.
     */
    public function visit_types()
    {
        return $this->belongsToMany(VisitType::class, 'summary_stay_types');
    }



    /**
     * Get the Aliases for the Hotel.
     */
    public function aliases()
    {
        return $this->hasMany(Alias::class);
    }

    /**
     * Get the Reviews for the Hotel.
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    /**
     * Get the Photos for the Hotel.
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }
}
