<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ReviewUsefulness extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'review_usefulness';

    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('review_usefulness.review_usefulness'),
        ]);
        $link = '<a href="'.route('review_usefulnesses.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
