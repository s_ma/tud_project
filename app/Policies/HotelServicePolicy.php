<?php

namespace App\Policies;

use App\User;
use App\HotelService;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelServicePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotel_service.
     *
     * @param  \App\User  $user
     * @param  \App\HotelService  $hotelService
     * @return mixed
     */
    public function view(User $user, HotelService $hotelService)
    {
        // Update $user authorization to view $hotelService here.
        return true;
    }

    /**
     * Determine whether the user can create hotel_service.
     *
     * @param  \App\User  $user
     * @param  \App\HotelService  $hotelService
     * @return mixed
     */
    public function create(User $user, HotelService $hotelService)
    {
        // Update $user authorization to create $hotelService here.
        return true;
    }

    /**
     * Determine whether the user can update the hotel_service.
     *
     * @param  \App\User  $user
     * @param  \App\HotelService  $hotelService
     * @return mixed
     */
    public function update(User $user, HotelService $hotelService)
    {
        // Update $user authorization to update $hotelService here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotel_service.
     *
     * @param  \App\User  $user
     * @param  \App\HotelService  $hotelService
     * @return mixed
     */
    public function delete(User $user, HotelService $hotelService)
    {
        // Update $user authorization to delete $hotelService here.
        return true;
    }
}
