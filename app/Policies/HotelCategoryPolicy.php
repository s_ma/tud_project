<?php

namespace App\Policies;

use App\User;
use App\HotelCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotel_category.
     *
     * @param  \App\User  $user
     * @param  \App\HotelCategory  $hotelCategory
     * @return mixed
     */
    public function view(User $user, HotelCategory $hotelCategory)
    {
        // Update $user authorization to view $hotelCategory here.
        return true;
    }

    /**
     * Determine whether the user can create hotel_category.
     *
     * @param  \App\User  $user
     * @param  \App\HotelCategory  $hotelCategory
     * @return mixed
     */
    public function create(User $user, HotelCategory $hotelCategory)
    {
        // Update $user authorization to create $hotelCategory here.
        return true;
    }

    /**
     * Determine whether the user can update the hotel_category.
     *
     * @param  \App\User  $user
     * @param  \App\HotelCategory  $hotelCategory
     * @return mixed
     */
    public function update(User $user, HotelCategory $hotelCategory)
    {
        // Update $user authorization to update $hotelCategory here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotel_category.
     *
     * @param  \App\User  $user
     * @param  \App\HotelCategory  $hotelCategory
     * @return mixed
     */
    public function delete(User $user, HotelCategory $hotelCategory)
    {
        // Update $user authorization to delete $hotelCategory here.
        return true;
    }
}
