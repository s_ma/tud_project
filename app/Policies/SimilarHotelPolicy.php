<?php

namespace App\Policies;

use App\User;
use App\SimilarHotel;
use Illuminate\Auth\Access\HandlesAuthorization;

class SimilarHotelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the similar_hotel.
     *
     * @param  \App\User  $user
     * @param  \App\SimilarHotel  $similarHotel
     * @return mixed
     */
    public function view(User $user, SimilarHotel $similarHotel)
    {
        // Update $user authorization to view $similarHotel here.
        return true;
    }

    /**
     * Determine whether the user can create similar_hotel.
     *
     * @param  \App\User  $user
     * @param  \App\SimilarHotel  $similarHotel
     * @return mixed
     */
    public function create(User $user, SimilarHotel $similarHotel)
    {
        // Update $user authorization to create $similarHotel here.
        return true;
    }

    /**
     * Determine whether the user can update the similar_hotel.
     *
     * @param  \App\User  $user
     * @param  \App\SimilarHotel  $similarHotel
     * @return mixed
     */
    public function update(User $user, SimilarHotel $similarHotel)
    {
        // Update $user authorization to update $similarHotel here.
        return true;
    }

    /**
     * Determine whether the user can delete the similar_hotel.
     *
     * @param  \App\User  $user
     * @param  \App\SimilarHotel  $similarHotel
     * @return mixed
     */
    public function delete(User $user, SimilarHotel $similarHotel)
    {
        // Update $user authorization to delete $similarHotel here.
        return true;
    }
}
