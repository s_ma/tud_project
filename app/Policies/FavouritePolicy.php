<?php

namespace App\Policies;

use App\User;
use App\Favourite;
use Illuminate\Auth\Access\HandlesAuthorization;

class FavouritePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the favourite.
     *
     * @param  \App\User  $user
     * @param  \App\Favourite  $favourite
     * @return mixed
     */
    public function view(User $user, Favourite $favourite)
    {
        // Update $user authorization to view $favourite here.
        return true;
    }

    /**
     * Determine whether the user can create favourite.
     *
     * @param  \App\User  $user
     * @param  \App\Favourite  $favourite
     * @return mixed
     */
    public function create(User $user, Favourite $favourite)
    {
        // Update $user authorization to create $favourite here.
        return true;
    }

    /**
     * Determine whether the user can update the favourite.
     *
     * @param  \App\User  $user
     * @param  \App\Favourite  $favourite
     * @return mixed
     */
    public function update(User $user, Favourite $favourite)
    {
        // Update $user authorization to update $favourite here.
        return true;
    }

    /**
     * Determine whether the user can delete the favourite.
     *
     * @param  \App\User  $user
     * @param  \App\Favourite  $favourite
     * @return mixed
     */
    public function delete(User $user, Favourite $favourite)
    {
        // Update $user authorization to delete $favourite here.
        return true;
    }
}
