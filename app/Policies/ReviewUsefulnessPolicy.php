<?php

namespace App\Policies;

use App\User;
use App\ReviewUsefulness;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewUsefulnessPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the review_usefulness.
     *
     * @param  \App\User  $user
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return mixed
     */
    public function view(User $user, ReviewUsefulness $reviewUsefulness)
    {
        // Update $user authorization to view $reviewUsefulness here.
        return true;
    }

    /**
     * Determine whether the user can create review_usefulness.
     *
     * @param  \App\User  $user
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return mixed
     */
    public function create(User $user, ReviewUsefulness $reviewUsefulness)
    {
        // Update $user authorization to create $reviewUsefulness here.
        return true;
    }

    /**
     * Determine whether the user can update the review_usefulness.
     *
     * @param  \App\User  $user
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return mixed
     */
    public function update(User $user, ReviewUsefulness $reviewUsefulness)
    {
        // Update $user authorization to update $reviewUsefulness here.
        return true;
    }

    /**
     * Determine whether the user can delete the review_usefulness.
     *
     * @param  \App\User  $user
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return mixed
     */
    public function delete(User $user, ReviewUsefulness $reviewUsefulness)
    {
        // Update $user authorization to delete $reviewUsefulness here.
        return true;
    }
}
