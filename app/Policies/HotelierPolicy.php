<?php

namespace App\Policies;

use App\User;
use App\Hotelier;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelierPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotelier.
     *
     * @param  \App\User  $user
     * @param  \App\Hotelier  $hotelier
     * @return mixed
     */
    public function view(User $user, Hotelier $hotelier)
    {
        // Update $user authorization to view $hotelier here.
        return true;
    }

    /**
     * Determine whether the user can create hotelier.
     *
     * @param  \App\User  $user
     * @param  \App\Hotelier  $hotelier
     * @return mixed
     */
    public function create(User $user, Hotelier $hotelier)
    {
        // Update $user authorization to create $hotelier here.
        return true;
    }

    /**
     * Determine whether the user can update the hotelier.
     *
     * @param  \App\User  $user
     * @param  \App\Hotelier  $hotelier
     * @return mixed
     */
    public function update(User $user, Hotelier $hotelier)
    {
        // Update $user authorization to update $hotelier here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotelier.
     *
     * @param  \App\User  $user
     * @param  \App\Hotelier  $hotelier
     * @return mixed
     */
    public function delete(User $user, Hotelier $hotelier)
    {
        // Update $user authorization to delete $hotelier here.
        return true;
    }
}
