<?php

namespace App\Policies;

use App\User;
use App\HotelReviewRating;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelReviewRatingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotel_review_rating.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return mixed
     */
    public function view(User $user, HotelReviewRating $hotelReviewRating)
    {
        // Update $user authorization to view $hotelReviewRating here.
        return true;
    }

    /**
     * Determine whether the user can create hotel_review_rating.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return mixed
     */
    public function create(User $user, HotelReviewRating $hotelReviewRating)
    {
        // Update $user authorization to create $hotelReviewRating here.
        return true;
    }

    /**
     * Determine whether the user can update the hotel_review_rating.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return mixed
     */
    public function update(User $user, HotelReviewRating $hotelReviewRating)
    {
        // Update $user authorization to update $hotelReviewRating here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotel_review_rating.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return mixed
     */
    public function delete(User $user, HotelReviewRating $hotelReviewRating)
    {
        // Update $user authorization to delete $hotelReviewRating here.
        return true;
    }
}
