<?php

namespace App\Policies;

use App\User;
use App\Hotel;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotel.
     *
     * @param  \App\User  $user
     * @param  \App\Hotel  $hotel
     * @return mixed
     */
    public function view(User $user, Hotel $hotel)
    {
        // Update $user authorization to view $hotel here.
        return true;
    }

    /**
     * Determine whether the user can create hotel.
     *
     * @param  \App\User  $user
     * @param  \App\Hotel  $hotel
     * @return mixed
     */
    public function create(User $user, Hotel $hotel)
    {
        // Update $user authorization to create $hotel here.
        return true;

        // Only a Hotelier may be able to create a Hotel
        // return Auth::guard('hotelier')->check()->user();
    }

    /**
     * Determine whether the user can update the hotel.
     *
     * @param  \App\User  $user
     * @param  \App\Hotel  $hotel
     * @return mixed
     */
    public function update(User $user, Hotel $hotel)
    {
        // Update $user authorization to update $hotel here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotel.
     *
     * @param  \App\User  $user
     * @param  \App\Hotel  $hotel
     * @return mixed
     */
    public function delete(User $user, Hotel $hotel)
    {
        // Update $user authorization to delete $hotel here.
        return true;
    }
}
