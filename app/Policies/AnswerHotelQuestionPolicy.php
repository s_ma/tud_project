<?php

namespace App\Policies;

use App\User;
use App\AnswerHotelQuestion;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnswerHotelQuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the answer_hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return mixed
     */
    public function view(User $user, AnswerHotelQuestion $answerHotelQuestion)
    {
        // Update $user authorization to view $answerHotelQuestion here.
        return true;
    }

    /**
     * Determine whether the user can create answer_hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return mixed
     */
    public function create(User $user, AnswerHotelQuestion $answerHotelQuestion)
    {
        // Update $user authorization to create $answerHotelQuestion here.
        return true;
    }

    /**
     * Determine whether the user can update the answer_hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return mixed
     */
    public function update(User $user, AnswerHotelQuestion $answerHotelQuestion)
    {
        // Update $user authorization to update $answerHotelQuestion here.
        return true;
    }

    /**
     * Determine whether the user can delete the answer_hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return mixed
     */
    public function delete(User $user, AnswerHotelQuestion $answerHotelQuestion)
    {
        // Update $user authorization to delete $answerHotelQuestion here.
        return true;
    }
}
