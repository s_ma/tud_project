<?php

namespace App\Policies;

use App\User;
use App\VisitDate;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitDatePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the visit_date.
     *
     * @param  \App\User  $user
     * @param  \App\VisitDate  $visitDate
     * @return mixed
     */
    public function view(User $user, VisitDate $visitDate)
    {
        // Update $user authorization to view $visitDate here.
        return true;
    }

    /**
     * Determine whether the user can create visit_date.
     *
     * @param  \App\User  $user
     * @param  \App\VisitDate  $visitDate
     * @return mixed
     */
    public function create(User $user, VisitDate $visitDate)
    {
        // Update $user authorization to create $visitDate here.
        return true;
    }

    /**
     * Determine whether the user can update the visit_date.
     *
     * @param  \App\User  $user
     * @param  \App\VisitDate  $visitDate
     * @return mixed
     */
    public function update(User $user, VisitDate $visitDate)
    {
        // Update $user authorization to update $visitDate here.
        return true;
    }

    /**
     * Determine whether the user can delete the visit_date.
     *
     * @param  \App\User  $user
     * @param  \App\VisitDate  $visitDate
     * @return mixed
     */
    public function delete(User $user, VisitDate $visitDate)
    {
        // Update $user authorization to delete $visitDate here.
        return true;
    }
}
