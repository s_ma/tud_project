<?php

namespace App\Policies;

use App\User;
use App\PriceRange;
use Illuminate\Auth\Access\HandlesAuthorization;

class PriceRangePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the price_range.
     *
     * @param  \App\User  $user
     * @param  \App\PriceRange  $priceRange
     * @return mixed
     */
    public function view(User $user, PriceRange $priceRange)
    {
        // Update $user authorization to view $priceRange here.
        return true;
    }

    /**
     * Determine whether the user can create price_range.
     *
     * @param  \App\User  $user
     * @param  \App\PriceRange  $priceRange
     * @return mixed
     */
    public function create(User $user, PriceRange $priceRange)
    {
        // Update $user authorization to create $priceRange here.
        return true;
    }

    /**
     * Determine whether the user can update the price_range.
     *
     * @param  \App\User  $user
     * @param  \App\PriceRange  $priceRange
     * @return mixed
     */
    public function update(User $user, PriceRange $priceRange)
    {
        // Update $user authorization to update $priceRange here.
        return true;
    }

    /**
     * Determine whether the user can delete the price_range.
     *
     * @param  \App\User  $user
     * @param  \App\PriceRange  $priceRange
     * @return mixed
     */
    public function delete(User $user, PriceRange $priceRange)
    {
        // Update $user authorization to delete $priceRange here.
        return true;
    }
}
