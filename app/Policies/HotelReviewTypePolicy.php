<?php

namespace App\Policies;

use App\User;
use App\HotelReviewType;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelReviewTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotel_review_type.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return mixed
     */
    public function view(User $user, HotelReviewType $hotelReviewType)
    {
        // Update $user authorization to view $hotelReviewType here.
        return true;
    }

    /**
     * Determine whether the user can create hotel_review_type.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return mixed
     */
    public function create(User $user, HotelReviewType $hotelReviewType)
    {
        // Update $user authorization to create $hotelReviewType here.
        return true;
    }

    /**
     * Determine whether the user can update the hotel_review_type.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return mixed
     */
    public function update(User $user, HotelReviewType $hotelReviewType)
    {
        // Update $user authorization to update $hotelReviewType here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotel_review_type.
     *
     * @param  \App\User  $user
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return mixed
     */
    public function delete(User $user, HotelReviewType $hotelReviewType)
    {
        // Update $user authorization to delete $hotelReviewType here.
        return true;
    }
}
