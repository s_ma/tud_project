<?php

namespace App\Policies;

use App\User;
use App\SummaryStayType;
use Illuminate\Auth\Access\HandlesAuthorization;

class SummaryStayTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the summary_stay_type.
     *
     * @param  \App\User  $user
     * @param  \App\SummaryStayType  $summaryStayType
     * @return mixed
     */
    public function view(User $user, SummaryStayType $summaryStayType)
    {
        // Update $user authorization to view $summaryStayType here.
        return true;
    }

    /**
     * Determine whether the user can create summary_stay_type.
     *
     * @param  \App\User  $user
     * @param  \App\SummaryStayType  $summaryStayType
     * @return mixed
     */
    public function create(User $user, SummaryStayType $summaryStayType)
    {
        // Update $user authorization to create $summaryStayType here.
        return true;
    }

    /**
     * Determine whether the user can update the summary_stay_type.
     *
     * @param  \App\User  $user
     * @param  \App\SummaryStayType  $summaryStayType
     * @return mixed
     */
    public function update(User $user, SummaryStayType $summaryStayType)
    {
        // Update $user authorization to update $summaryStayType here.
        return true;
    }

    /**
     * Determine whether the user can delete the summary_stay_type.
     *
     * @param  \App\User  $user
     * @param  \App\SummaryStayType  $summaryStayType
     * @return mixed
     */
    public function delete(User $user, SummaryStayType $summaryStayType)
    {
        // Update $user authorization to delete $summaryStayType here.
        return true;
    }
}
