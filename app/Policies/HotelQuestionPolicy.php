<?php

namespace App\Policies;

use App\User;
use App\HotelQuestion;
use Illuminate\Auth\Access\HandlesAuthorization;

class HotelQuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return mixed
     */
    public function view(User $user, HotelQuestion $hotelQuestion)
    {
        // Update $user authorization to view $hotelQuestion here.
        return true;
    }

    /**
     * Determine whether the user can create hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return mixed
     */
    public function create(User $user, HotelQuestion $hotelQuestion)
    {
        // Update $user authorization to create $hotelQuestion here.
        return true;
    }

    /**
     * Determine whether the user can update the hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return mixed
     */
    public function update(User $user, HotelQuestion $hotelQuestion)
    {
        // Update $user authorization to update $hotelQuestion here.
        return true;
    }

    /**
     * Determine whether the user can delete the hotel_question.
     *
     * @param  \App\User  $user
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return mixed
     */
    public function delete(User $user, HotelQuestion $hotelQuestion)
    {
        // Update $user authorization to delete $hotelQuestion here.
        return true;
    }
}
