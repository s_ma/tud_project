<?php

namespace App\Policies;

use App\User;
use App\VisitType;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the visit_type.
     *
     * @param  \App\User  $user
     * @param  \App\VisitType  $visitType
     * @return mixed
     */
    public function view(User $user, VisitType $visitType)
    {
        // Update $user authorization to view $visitType here.
        return true;
    }

    /**
     * Determine whether the user can create visit_type.
     *
     * @param  \App\User  $user
     * @param  \App\VisitType  $visitType
     * @return mixed
     */
    public function create(User $user, VisitType $visitType)
    {
        // Update $user authorization to create $visitType here.
        return true;
    }

    /**
     * Determine whether the user can update the visit_type.
     *
     * @param  \App\User  $user
     * @param  \App\VisitType  $visitType
     * @return mixed
     */
    public function update(User $user, VisitType $visitType)
    {
        // Update $user authorization to update $visitType here.
        return true;
    }

    /**
     * Determine whether the user can delete the visit_type.
     *
     * @param  \App\User  $user
     * @param  \App\VisitType  $visitType
     * @return mixed
     */
    public function delete(User $user, VisitType $visitType)
    {
        // Update $user authorization to delete $visitType here.
        return true;
    }
}
