<?php

namespace App\Policies;

use App\User;
use App\Alias;
use Illuminate\Auth\Access\HandlesAuthorization;

class AliasPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the alias.
     *
     * @param  \App\User  $user
     * @param  \App\Alias  $alias
     * @return mixed
     */
    public function view(User $user, Alias $alias)
    {
        // Update $user authorization to view $alias here.
        return true;
    }

    /**
     * Determine whether the user can create alias.
     *
     * @param  \App\User  $user
     * @param  \App\Alias  $alias
     * @return mixed
     */
    public function create(User $user, Alias $alias)
    {
        // Update $user authorization to create $alias here.
        return true;
    }

    /**
     * Determine whether the user can update the alias.
     *
     * @param  \App\User  $user
     * @param  \App\Alias  $alias
     * @return mixed
     */
    public function update(User $user, Alias $alias)
    {
        // Update $user authorization to update $alias here.
        return true;
    }

    /**
     * Determine whether the user can delete the alias.
     *
     * @param  \App\User  $user
     * @param  \App\Alias  $alias
     * @return mixed
     */
    public function delete(User $user, Alias $alias)
    {
        // Update $user authorization to delete $alias here.
        return true;
    }
}
