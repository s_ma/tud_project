<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class SummaryStayType extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('summary_stay_type.summary_stay_type'),
        ]);
        $link = '<a href="'.route('summary_stay_types.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
