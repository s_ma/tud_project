<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Hotelier extends Authenticatable
{
    use Notifiable;
    
    /**
     * Authentication Guard
     */
    protected $guard = 'hotelier';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 
        'password', 
        'lastname',
        'firstname',
        'address_1',
        'address_2',
        'postcode',
        'city',
        'state',
        'country_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('hotelier.hotelier'),
        ]);
        $link = '<a href="'.route('hoteliers.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Hotels for the Hotelier.
     */
    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }

    /**
     * Get the Country associated with the Hotelier.
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

}
