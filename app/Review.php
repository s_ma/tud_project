<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('review.review'),
        ]);
        $link = '<a href="'.route('reviews.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the VisitDate associated with the Review.
     */
    public function visit_date()
    {
        return $this->belongsTo(VisitDate::class);
    }

    /**
     * Get the VisitType associated with the Review.
     */
    public function visit_type()
    {
        return $this->belongsTo(VisitType::class);
    }

    /**
     * Get the Hotel associated with the Review.
     */
    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    /**
     * The Subscribers who marked the Review as usefull.
     */
    public function subscribers()
    {
        return $this->belongsToMany(Subscriber::class, 'review_usefulness');
    }

    /**
     * The HotelQuestions that belong to the Review.
     */
    public function hotel_questions()
    {
        return $this->belongsToMany(HotelQuestion::class, 'answer_hotel_questions');
    }

    /**
     * The HotelReviewTypes that belong to the Review.
     */
    public function hotel_review_types()
    {
        return $this->belongsToMany(HotelReviewType::class, 'hotel_review_ratings');
    }

    /**
     * Get the Photos for the Review.
     */
    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

}
