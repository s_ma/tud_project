<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('country.country'),
        ]);
        $link = '<a href="'.route('countries.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Hotels for the Country.
     */
    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }

    /**
     * Get the Hoteliers for the Country.
     */
    public function hoteliers()
    {
        return $this->hasMany(Hotelier::class);
    }

    /**
     * Get the Subscribers for the Country.
     */
    public function subscribers()
    {
        return $this->hasMany(Subscriber::class);
    }
}
