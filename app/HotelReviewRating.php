<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class HotelReviewRating extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('hotel_review_rating.hotel_review_rating'),
        ]);
        $link = '<a href="'.route('hotel_review_ratings.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }
}
