<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Subscriber extends Authenticatable
{
    use Notifiable;

    /**
     * Authentication Guard
     */
    protected $guard = 'subscriber';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pseudo', 
        'password',
        'email',
        'lastname',
        'firstname',
        'address_1',
        'address_2',
        'postcode', 
        'city',
        'state',
        'country_id',
        'latitude',
        'longitude',
        'dialling_code',
        'phone',
        'airport'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('subscriber.subscriber'),
        ]);
        $link = '<a href="'.route('subscribers.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Country associated with the Subscriber.
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * The favourite Hotels that belong to the Subscriber.
     */
    public function favourite_hotels()
    {
        return $this->belongsToMany(Hotel::class, 'favourites');
    }

    /**
     * The useful Reviews that belong to the Subscriber.
     */
    public function useful_reviews()
    {
        return $this->belongsToMany(Review::class, 'review_usefulness');
    }
    
}
