<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class HotelCategory extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('hotel_category.hotel_category'),
        ]);
        $link = '<a href="'.route('hotel_categories.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Hotels for the HotelCategory.
     */
    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }
}
