<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PriceRange extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('price_range.price_range'),
        ]);
        $link = '<a href="'.route('price_ranges.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Hotels for the price range.
     */
    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }
}
