<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class HotelQuestion extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('hotel_question.hotel_question'),
        ]);
        $link = '<a href="'.route('hotel_questions.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The Reviews that belong to the HotelQuestion.
     */
    public function reviews()
    {
        return $this->belongsToMany(Review::class, 'answer_hotel_questions');
    }
}
