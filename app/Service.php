<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('service.service'),
        ]);
        $link = '<a href="'.route('services.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The Hotels that belong to the Service.
     */
    public function hotels()
    {
        return $this->belongsToMany(Hotel::class);
    }
}
