<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class VisitType extends Model
{
    protected $fillable = ['name', 'description', 'creator_id'];

    public function getNameLinkAttribute()
    {
        $title = __('app.show_detail_title', [
            'name' => $this->name, 'type' => __('visit_type.visit_type'),
        ]);
        $link = '<a href="'.route('visit_types.show', $this).'"';
        $link .= ' title="'.$title.'">';
        $link .= $this->name;
        $link .= '</a>';

        return $link;
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the Reviews for the VisitType.
     */
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    /**
     * The Hotels that belong to the VisitType.
     */
    public function hotels()
    {
        return $this->belongsToMany(Hotel::class, 'summary_stay_types');
    }
}
