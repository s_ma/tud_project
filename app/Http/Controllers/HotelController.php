<?php

namespace App\Http\Controllers;

use App\Hotel;
use App\Country;
use App\PriceRange;
use Auth;
use Redirect;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    /**
     * Display a listing of the hotel.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelQuery = Hotel::query();
        $hotelQuery->where('name', 'like', '%'.request('q').'%');
        $hotels = $hotelQuery->paginate(25);

        return view('hotels.index', compact('hotels'));
    }

    /**
     * Show the form for creating a new hotel.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // UNCOMMENT LATER
        // $this->authorize('create', new Hotel);

        // Data to pass to the create view
        $countries = Country::all();
        $price_ranges = PriceRange::all();

        // Return the view with the data
        return view('hotels.create', compact(['countries', 'price_ranges']));
    }

    /**
     * Store a newly created hotel in storage.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // TODO: UNCOMMENT LATER
        // $this->authorize('create', new Hotel);

        // Additional verification: Check that the user is logged as a Hotelier
        if(empty(Auth::guard('hotelier')->user()))
            Redirect::back()->withErrors(['msg', 'Please log in as a Hotelier before creating a Hotel!']);
        
        // Validate the request parameters
        $newHotel = $request->validate([
            'name'              => 'required|string|max:100',
            'description'       => 'nullable|string|max:500',
            'address_1'         => 'required|string|max:100',
            'address_2'         => 'required|string|max:100',
            'address_2'         => 'nullable|string|max:100',
            'postcode'          => 'required|string|max:10',
            'city'              => 'required|string|max:50',
            'state'             => 'nullable|string|max:50',
            'latitude'          => 'required|numeric',
            'longitude'         => 'required|numeric',
            'stars'             => 'required|numeric|max:5',
            'dialling_code'     => 'required|numeric',
            'phone'             => 'required|string|max:20',
            'email'             => 'required|string|max:80',
            'website'           => 'nullable|string|max:100',
            'number_of_rooms'   => 'nullable|numeric',
            'price_range_id'    => 'required|numeric',
            'country_id'        => 'required|numeric',
        ]);

        // hotelier_id = logged Hotelier's ID
        $newHotel['hotelier_id'] = Auth::guard('hotelier')->user()->id;

        // Create the Hotel
        $hotel = Hotel::create($newHotel);

        return redirect()->route('hotels.show', $hotel);
    }

    /**
     * Display the specified hotel.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\View\View
     */
    public function show(Hotel $hotel)
    {
        return view('hotels.show', compact('hotel'));
    }

    /**
     * Show the form for editing the specified hotel.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\View\View
     */
    public function edit(Hotel $hotel)
    {
        // TODO: UNCOMMENT LATER
        // $this->authorize('update', $hotel);

        // Data to pass to the create view
        $countries = Country::all();
        $price_ranges = PriceRange::all();

        return view('hotels.edit', compact(['hotel', 'countries', 'price_ranges']));
    }

    /**
     * Update the specified hotel in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Hotel $hotel)
    {
        // TODO: UNCOMMENT LATER
        // $this->authorize('update', $hotel);

        // Additional verification: Check that the user is logged as a Hotelier
        if(empty(Auth::guard('hotelier')->user()))
            Redirect::back()->withErrors(['msg', 'Please log in as a Hotelier before creating a Hotel!']);
        
        // Validate the request parameters
        $hotelData = $request->validate([
            'name'              => 'required|string|max:100',
            'description'       => 'nullable|string|max:500',
            'address_1'         => 'required|string|max:100',
            'address_2'         => 'required|string|max:100',
            'address_2'         => 'nullable|string|max:100',
            'postcode'          => 'required|string|max:10',
            'city'              => 'required|string|max:50',
            'state'             => 'nullable|string|max:50',
            'latitude'          => 'required|numeric',
            'longitude'         => 'required|numeric',
            'stars'             => 'required|numeric|max:5',
            'dialling_code'     => 'required|numeric',
            'phone'             => 'required|string|max:20',
            'email'             => 'required|string|max:80',
            'website'           => 'nullable|string|max:100',
            'number_of_rooms'   => 'nullable|numeric',
            'price_range_id'    => 'required|numeric',
            'country_id'        => 'required|numeric',
        ]);

        // Perform the update
        $hotel->update($hotelData);

        return redirect()->route('hotels.show', $hotel);
    }

    /**
     * Remove the specified hotel from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Hotel $hotel)
    {
        // TODO: UNCOMMENT LATER
        // $this->authorize('delete', $hotel);

        $request->validate(['hotel_id' => 'required']);

        if ($request->get('hotel_id') == $hotel->id && $hotel->delete()) {
            return redirect()->route('hotels.index');
        }

        return back();
    }
}
