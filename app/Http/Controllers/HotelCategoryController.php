<?php

namespace App\Http\Controllers;

use App\HotelCategory;
use Illuminate\Http\Request;

class HotelCategoryController extends Controller
{
    /**
     * Display a listing of the hotelCategory.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelCategoryQuery = HotelCategory::query();
        $hotelCategoryQuery->where('name', 'like', '%'.request('q').'%');
        $hotelCategories = $hotelCategoryQuery->paginate(25);

        return view('hotel_categories.index', compact('hotelCategories'));
    }

    /**
     * Show the form for creating a new hotelCategory.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new HotelCategory);

        return view('hotel_categories.create');
    }

    /**
     * Store a newly created hotelCategory in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new HotelCategory);

        $newHotelCategory = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newHotelCategory['creator_id'] = auth()->id();

        $hotelCategory = HotelCategory::create($newHotelCategory);

        return redirect()->route('hotel_categories.show', $hotelCategory);
    }

    /**
     * Display the specified hotelCategory.
     *
     * @param  \App\HotelCategory  $hotelCategory
     * @return \Illuminate\View\View
     */
    public function show(HotelCategory $hotelCategory)
    {
        return view('hotel_categories.show', compact('hotelCategory'));
    }

    /**
     * Show the form for editing the specified hotelCategory.
     *
     * @param  \App\HotelCategory  $hotelCategory
     * @return \Illuminate\View\View
     */
    public function edit(HotelCategory $hotelCategory)
    {
        $this->authorize('update', $hotelCategory);

        return view('hotel_categories.edit', compact('hotelCategory'));
    }

    /**
     * Update the specified hotelCategory in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelCategory  $hotelCategory
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, HotelCategory $hotelCategory)
    {
        $this->authorize('update', $hotelCategory);

        $hotelCategoryData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $hotelCategory->update($hotelCategoryData);

        return redirect()->route('hotel_categories.show', $hotelCategory);
    }

    /**
     * Remove the specified hotelCategory from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelCategory  $hotelCategory
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, HotelCategory $hotelCategory)
    {
        $this->authorize('delete', $hotelCategory);

        $request->validate(['hotel_category_id' => 'required']);

        if ($request->get('hotel_category_id') == $hotelCategory->id && $hotelCategory->delete()) {
            return redirect()->route('hotel_categories.index');
        }

        return back();
    }
}
