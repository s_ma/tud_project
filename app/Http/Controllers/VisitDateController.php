<?php

namespace App\Http\Controllers;

use App\VisitDate;
use Illuminate\Http\Request;

class VisitDateController extends Controller
{
    /**
     * Display a listing of the visitDate.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $visitDateQuery = VisitDate::query();
        $visitDateQuery->where('name', 'like', '%'.request('q').'%');
        $visitDates = $visitDateQuery->paginate(25);

        return view('visit_dates.index', compact('visitDates'));
    }

    /**
     * Show the form for creating a new visitDate.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new VisitDate);

        return view('visit_dates.create');
    }

    /**
     * Store a newly created visitDate in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new VisitDate);

        $newVisitDate = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newVisitDate['creator_id'] = auth()->id();

        $visitDate = VisitDate::create($newVisitDate);

        return redirect()->route('visit_dates.show', $visitDate);
    }

    /**
     * Display the specified visitDate.
     *
     * @param  \App\VisitDate  $visitDate
     * @return \Illuminate\View\View
     */
    public function show(VisitDate $visitDate)
    {
        return view('visit_dates.show', compact('visitDate'));
    }

    /**
     * Show the form for editing the specified visitDate.
     *
     * @param  \App\VisitDate  $visitDate
     * @return \Illuminate\View\View
     */
    public function edit(VisitDate $visitDate)
    {
        $this->authorize('update', $visitDate);

        return view('visit_dates.edit', compact('visitDate'));
    }

    /**
     * Update the specified visitDate in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VisitDate  $visitDate
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, VisitDate $visitDate)
    {
        $this->authorize('update', $visitDate);

        $visitDateData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $visitDate->update($visitDateData);

        return redirect()->route('visit_dates.show', $visitDate);
    }

    /**
     * Remove the specified visitDate from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VisitDate  $visitDate
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, VisitDate $visitDate)
    {
        $this->authorize('delete', $visitDate);

        $request->validate(['visit_date_id' => 'required']);

        if ($request->get('visit_date_id') == $visitDate->id && $visitDate->delete()) {
            return redirect()->route('visit_dates.index');
        }

        return back();
    }
}
