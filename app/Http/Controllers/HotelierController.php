<?php

namespace App\Http\Controllers;

use App\Hotelier;
use App\Country;
use Illuminate\Http\Request;
use Auth;
use Redirect;

class HotelierController extends Controller
{
    /**
     * Display a listing of the hotelier.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelierQuery = Hotelier::query();
        $hotelierQuery->where('lastname', 'like', '%'.request('q').'%');
        $hoteliers = $hotelierQuery->paginate(25);

        return view('hoteliers.index', compact('hoteliers'));
    }

    /**
     * Show the form for creating a new hotelier.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // $this->authorize('create', new Hotelier);

        return view('hoteliers.create');
    }

    /**
     * Store a newly created hotelier in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // $this->authorize('create', new Hotelier);

        $newHotelier = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newHotelier['creator_id'] = auth()->id();

        $hotelier = Hotelier::create($newHotelier);

        return redirect()->route('hoteliers.show', $hotelier);
    }

    /**
     * Display the specified hotelier.
     *
     * @param  \App\Hotelier  $hotelier
     * @return \Illuminate\View\View
     */
    public function show(Hotelier $hotelier)
    {
        return view('hoteliers.show', compact('hotelier'));
    }

    /**
     * Show the form for editing the specified hotelier.
     *
     * @param  \App\Hotelier  $hotelier
     * @return \Illuminate\View\View
     */
    public function edit(Hotelier $hotelier)
    {
        // $this->authorize('update', $hotelier);

        // Data to pass to the create view
        $countries = Country::all();

        return view('hoteliers.edit', compact(['hotelier', 'countries']));
    }

    /**
     * Update the specified hotelier in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotelier  $hotelier
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Hotelier $hotelier)
    {
        // $this->authorize('update', $hotelier);

        $hotelierData = $request->validate([
            'lastname'      => 'required|string|max:80',
            'firstname'     => 'required|string|max:80',
            'address_1'     => 'required|string|max:100',
            'address_2'     => 'nullable|string|max:100',
            'postcode'      => 'required|string|max:10',
            'city'          => 'required|string|max:50',
            'state'         => 'nullable|string|max:50',
            'country_id'    => 'required|numeric',
        ]);

        // Perform the update
        $hotelier->update($hotelierData);

        return redirect()->route('hoteliers.show', $hotelier)->with('success', 'Hotelier account successfully updated!');
    }

    /**
     * Remove the specified hotelier from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotelier  $hotelier
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Hotelier $hotelier)
    {
        // $this->authorize('delete', $hotelier);

        $request->validate(['hotelier_id' => 'required']);

        if ($request->get('hotelier_id') == $hotelier->id && $hotelier->delete()) {
            return redirect()->route('hoteliers.index');
        }

        return back();
    }
}
