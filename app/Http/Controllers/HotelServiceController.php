<?php

namespace App\Http\Controllers;

use App\HotelService;
use Illuminate\Http\Request;

class HotelServiceController extends Controller
{
    /**
     * Display a listing of the hotelService.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelServiceQuery = HotelService::query();
        $hotelServiceQuery->where('name', 'like', '%'.request('q').'%');
        $hotelServices = $hotelServiceQuery->paginate(25);

        return view('hotel_services.index', compact('hotelServices'));
    }

    /**
     * Show the form for creating a new hotelService.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new HotelService);

        return view('hotel_services.create');
    }

    /**
     * Store a newly created hotelService in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new HotelService);

        $newHotelService = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newHotelService['creator_id'] = auth()->id();

        $hotelService = HotelService::create($newHotelService);

        return redirect()->route('hotel_services.show', $hotelService);
    }

    /**
     * Display the specified hotelService.
     *
     * @param  \App\HotelService  $hotelService
     * @return \Illuminate\View\View
     */
    public function show(HotelService $hotelService)
    {
        return view('hotel_services.show', compact('hotelService'));
    }

    /**
     * Show the form for editing the specified hotelService.
     *
     * @param  \App\HotelService  $hotelService
     * @return \Illuminate\View\View
     */
    public function edit(HotelService $hotelService)
    {
        $this->authorize('update', $hotelService);

        return view('hotel_services.edit', compact('hotelService'));
    }

    /**
     * Update the specified hotelService in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelService  $hotelService
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, HotelService $hotelService)
    {
        $this->authorize('update', $hotelService);

        $hotelServiceData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $hotelService->update($hotelServiceData);

        return redirect()->route('hotel_services.show', $hotelService);
    }

    /**
     * Remove the specified hotelService from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelService  $hotelService
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, HotelService $hotelService)
    {
        $this->authorize('delete', $hotelService);

        $request->validate(['hotel_service_id' => 'required']);

        if ($request->get('hotel_service_id') == $hotelService->id && $hotelService->delete()) {
            return redirect()->route('hotel_services.index');
        }

        return back();
    }
}
