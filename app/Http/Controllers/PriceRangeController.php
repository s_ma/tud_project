<?php

namespace App\Http\Controllers;

use App\PriceRange;
use Illuminate\Http\Request;

class PriceRangeController extends Controller
{
    /**
     * Display a listing of the priceRange.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $priceRangeQuery = PriceRange::query();
        $priceRangeQuery->where('name', 'like', '%'.request('q').'%');
        $priceRanges = $priceRangeQuery->paginate(25);

        return view('price_ranges.index', compact('priceRanges'));
    }

    /**
     * Show the form for creating a new priceRange.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new PriceRange);

        return view('price_ranges.create');
    }

    /**
     * Store a newly created priceRange in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new PriceRange);

        $newPriceRange = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newPriceRange['creator_id'] = auth()->id();

        $priceRange = PriceRange::create($newPriceRange);

        return redirect()->route('price_ranges.show', $priceRange);
    }

    /**
     * Display the specified priceRange.
     *
     * @param  \App\PriceRange  $priceRange
     * @return \Illuminate\View\View
     */
    public function show(PriceRange $priceRange)
    {
        return view('price_ranges.show', compact('priceRange'));
    }

    /**
     * Show the form for editing the specified priceRange.
     *
     * @param  \App\PriceRange  $priceRange
     * @return \Illuminate\View\View
     */
    public function edit(PriceRange $priceRange)
    {
        $this->authorize('update', $priceRange);

        return view('price_ranges.edit', compact('priceRange'));
    }

    /**
     * Update the specified priceRange in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PriceRange  $priceRange
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, PriceRange $priceRange)
    {
        $this->authorize('update', $priceRange);

        $priceRangeData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $priceRange->update($priceRangeData);

        return redirect()->route('price_ranges.show', $priceRange);
    }

    /**
     * Remove the specified priceRange from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PriceRange  $priceRange
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, PriceRange $priceRange)
    {
        $this->authorize('delete', $priceRange);

        $request->validate(['price_range_id' => 'required']);

        if ($request->get('price_range_id') == $priceRange->id && $priceRange->delete()) {
            return redirect()->route('price_ranges.index');
        }

        return back();
    }
}
