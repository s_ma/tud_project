<?php

namespace App\Http\Controllers;

use App\Subscriber;
use App\Country;
use Illuminate\Http\Request;
use Auth;
use Redirect;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the subscriber.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $subscriberQuery = Subscriber::query();
        $subscriberQuery->where('name', 'like', '%'.request('q').'%');
        $subscribers = $subscriberQuery->paginate(25);

        return view('subscribers.index', compact('subscribers'));
    }

    /**
     * Show the form for creating a new subscriber.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // $this->authorize('create', new Subscriber);

        return view('subscribers.create');
    }

    /**
     * Store a newly created subscriber in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        // $this->authorize('create', new Subscriber);

        $newSubscriber = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newSubscriber['creator_id'] = auth()->id();

        $subscriber = Subscriber::create($newSubscriber);

        return redirect()->route('subscribers.show', $subscriber);
    }

    /**
     * Display the specified subscriber.
     *
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\View\View
     */
    public function show(Subscriber $subscriber)
    {
        return view('subscribers.show', compact('subscriber'));
    }

    /**
     * Show the form for editing the specified subscriber.
     *
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\View\View
     */
    public function edit(Subscriber $subscriber)
    {
        // $this->authorize('update', $subscriber);

        // Data to pass to the create view
        $countries = Country::all();

        return view('subscribers.edit', compact(['subscriber', 'countries']));
    }

    /**
     * Update the specified subscriber in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Subscriber $subscriber)
    {
        // $this->authorize('update', $subscriber);

        // Validate the request parameters
        $subscriberData = $request->validate([
            'pseudo'        => 'required|string|max:30',
            'lastname'      => 'required|string|max:80',
            'firstname'     => 'required|string|max:80',
            'address_1'     => 'required|string|max:100',
            'address_2'     => 'nullable|string|max:100',
            'postcode'      => 'required|string|max:10',
            'city'          => 'required|string|max:50',
            'state'         => 'nullable|string|max:50',
            'country_id'    => 'required|numeric',
            'latitude'      => 'required|numeric',
            'longitude'     => 'required|numeric',
            'dialling_code' => 'required|numeric',
            'phone'         => 'required|string|max:20',
            'airport'       => 'nullable|string|max:50',
        ]);

        // Perform the update
        $subscriber->update($subscriberData);

        return redirect()->route('subscribers.show', $subscriber)->with('success','Subscriber account successfully updated!');
    }

    /**
     * Remove the specified subscriber from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Subscriber  $subscriber
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Subscriber $subscriber)
    {
        // $this->authorize('delete', $subscriber);

        $request->validate(['subscriber_id' => 'required']);

        if ($request->get('subscriber_id') == $subscriber->id && $subscriber->delete()) {
            return redirect()->route('subscribers.index');
        }

        return back();
    }
}
