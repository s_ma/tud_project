<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the service.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $serviceQuery = Service::query();
        $serviceQuery->where('name', 'like', '%'.request('q').'%');
        $services = $serviceQuery->paginate(25);

        return view('services.index', compact('services'));
    }

    /**
     * Show the form for creating a new service.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new Service);

        return view('services.create');
    }

    /**
     * Store a newly created service in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Service);

        $newService = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newService['creator_id'] = auth()->id();

        $service = Service::create($newService);

        return redirect()->route('services.show', $service);
    }

    /**
     * Display the specified service.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\View\View
     */
    public function show(Service $service)
    {
        return view('services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified service.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\View\View
     */
    public function edit(Service $service)
    {
        $this->authorize('update', $service);

        return view('services.edit', compact('service'));
    }

    /**
     * Update the specified service in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Service $service)
    {
        $this->authorize('update', $service);

        $serviceData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $service->update($serviceData);

        return redirect()->route('services.show', $service);
    }

    /**
     * Remove the specified service from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Service $service)
    {
        $this->authorize('delete', $service);

        $request->validate(['service_id' => 'required']);

        if ($request->get('service_id') == $service->id && $service->delete()) {
            return redirect()->route('services.index');
        }

        return back();
    }
}
