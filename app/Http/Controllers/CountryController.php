<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * Display a listing of the country.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $countryQuery = Country::query();
        $countryQuery->where('name', 'like', '%'.request('q').'%');
        $countries = $countryQuery->paginate(25);

        return view('countries.index', compact('countries'));
    }

    /**
     * Show the form for creating a new country.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new Country);

        return view('countries.create');
    }

    /**
     * Store a newly created country in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Country);

        $newCountry = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newCountry['creator_id'] = auth()->id();

        $country = Country::create($newCountry);

        return redirect()->route('countries.show', $country);
    }

    /**
     * Display the specified country.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\View\View
     */
    public function show(Country $country)
    {
        return view('countries.show', compact('country'));
    }

    /**
     * Show the form for editing the specified country.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\View\View
     */
    public function edit(Country $country)
    {
        $this->authorize('update', $country);

        return view('countries.edit', compact('country'));
    }

    /**
     * Update the specified country in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Country $country)
    {
        $this->authorize('update', $country);

        $countryData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $country->update($countryData);

        return redirect()->route('countries.show', $country);
    }

    /**
     * Remove the specified country from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Country $country)
    {
        $this->authorize('delete', $country);

        $request->validate(['country_id' => 'required']);

        if ($request->get('country_id') == $country->id && $country->delete()) {
            return redirect()->route('countries.index');
        }

        return back();
    }
}
