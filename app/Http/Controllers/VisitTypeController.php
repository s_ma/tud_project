<?php

namespace App\Http\Controllers;

use App\VisitType;
use Illuminate\Http\Request;

class VisitTypeController extends Controller
{
    /**
     * Display a listing of the visitType.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $visitTypeQuery = VisitType::query();
        $visitTypeQuery->where('name', 'like', '%'.request('q').'%');
        $visitTypes = $visitTypeQuery->paginate(25);

        return view('visit_types.index', compact('visitTypes'));
    }

    /**
     * Show the form for creating a new visitType.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new VisitType);

        return view('visit_types.create');
    }

    /**
     * Store a newly created visitType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new VisitType);

        $newVisitType = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newVisitType['creator_id'] = auth()->id();

        $visitType = VisitType::create($newVisitType);

        return redirect()->route('visit_types.show', $visitType);
    }

    /**
     * Display the specified visitType.
     *
     * @param  \App\VisitType  $visitType
     * @return \Illuminate\View\View
     */
    public function show(VisitType $visitType)
    {
        return view('visit_types.show', compact('visitType'));
    }

    /**
     * Show the form for editing the specified visitType.
     *
     * @param  \App\VisitType  $visitType
     * @return \Illuminate\View\View
     */
    public function edit(VisitType $visitType)
    {
        $this->authorize('update', $visitType);

        return view('visit_types.edit', compact('visitType'));
    }

    /**
     * Update the specified visitType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VisitType  $visitType
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, VisitType $visitType)
    {
        $this->authorize('update', $visitType);

        $visitTypeData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $visitType->update($visitTypeData);

        return redirect()->route('visit_types.show', $visitType);
    }

    /**
     * Remove the specified visitType from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VisitType  $visitType
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, VisitType $visitType)
    {
        $this->authorize('delete', $visitType);

        $request->validate(['visit_type_id' => 'required']);

        if ($request->get('visit_type_id') == $visitType->id && $visitType->delete()) {
            return redirect()->route('visit_types.index');
        }

        return back();
    }
}
