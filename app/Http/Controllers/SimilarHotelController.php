<?php

namespace App\Http\Controllers;

use App\SimilarHotel;
use Illuminate\Http\Request;

class SimilarHotelController extends Controller
{
    /**
     * Display a listing of the similarHotel.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $similarHotelQuery = SimilarHotel::query();
        $similarHotelQuery->where('name', 'like', '%'.request('q').'%');
        $similarHotels = $similarHotelQuery->paginate(25);

        return view('similar_hotels.index', compact('similarHotels'));
    }

    /**
     * Show the form for creating a new similarHotel.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new SimilarHotel);

        return view('similar_hotels.create');
    }

    /**
     * Store a newly created similarHotel in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new SimilarHotel);

        $newSimilarHotel = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newSimilarHotel['creator_id'] = auth()->id();

        $similarHotel = SimilarHotel::create($newSimilarHotel);

        return redirect()->route('similar_hotels.show', $similarHotel);
    }

    /**
     * Display the specified similarHotel.
     *
     * @param  \App\SimilarHotel  $similarHotel
     * @return \Illuminate\View\View
     */
    public function show(SimilarHotel $similarHotel)
    {
        return view('similar_hotels.show', compact('similarHotel'));
    }

    /**
     * Show the form for editing the specified similarHotel.
     *
     * @param  \App\SimilarHotel  $similarHotel
     * @return \Illuminate\View\View
     */
    public function edit(SimilarHotel $similarHotel)
    {
        $this->authorize('update', $similarHotel);

        return view('similar_hotels.edit', compact('similarHotel'));
    }

    /**
     * Update the specified similarHotel in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimilarHotel  $similarHotel
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, SimilarHotel $similarHotel)
    {
        $this->authorize('update', $similarHotel);

        $similarHotelData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $similarHotel->update($similarHotelData);

        return redirect()->route('similar_hotels.show', $similarHotel);
    }

    /**
     * Remove the specified similarHotel from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SimilarHotel  $similarHotel
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, SimilarHotel $similarHotel)
    {
        $this->authorize('delete', $similarHotel);

        $request->validate(['similar_hotel_id' => 'required']);

        if ($request->get('similar_hotel_id') == $similarHotel->id && $similarHotel->delete()) {
            return redirect()->route('similar_hotels.index');
        }

        return back();
    }
}
