<?php

namespace App\Http\Controllers;

use App\HotelQuestion;
use Illuminate\Http\Request;

class HotelQuestionController extends Controller
{
    /**
     * Display a listing of the hotelQuestion.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelQuestionQuery = HotelQuestion::query();
        $hotelQuestionQuery->where('name', 'like', '%'.request('q').'%');
        $hotelQuestions = $hotelQuestionQuery->paginate(25);

        return view('hotel_questions.index', compact('hotelQuestions'));
    }

    /**
     * Show the form for creating a new hotelQuestion.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new HotelQuestion);

        return view('hotel_questions.create');
    }

    /**
     * Store a newly created hotelQuestion in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new HotelQuestion);

        $newHotelQuestion = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newHotelQuestion['creator_id'] = auth()->id();

        $hotelQuestion = HotelQuestion::create($newHotelQuestion);

        return redirect()->route('hotel_questions.show', $hotelQuestion);
    }

    /**
     * Display the specified hotelQuestion.
     *
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return \Illuminate\View\View
     */
    public function show(HotelQuestion $hotelQuestion)
    {
        return view('hotel_questions.show', compact('hotelQuestion'));
    }

    /**
     * Show the form for editing the specified hotelQuestion.
     *
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return \Illuminate\View\View
     */
    public function edit(HotelQuestion $hotelQuestion)
    {
        $this->authorize('update', $hotelQuestion);

        return view('hotel_questions.edit', compact('hotelQuestion'));
    }

    /**
     * Update the specified hotelQuestion in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, HotelQuestion $hotelQuestion)
    {
        $this->authorize('update', $hotelQuestion);

        $hotelQuestionData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $hotelQuestion->update($hotelQuestionData);

        return redirect()->route('hotel_questions.show', $hotelQuestion);
    }

    /**
     * Remove the specified hotelQuestion from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelQuestion  $hotelQuestion
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, HotelQuestion $hotelQuestion)
    {
        $this->authorize('delete', $hotelQuestion);

        $request->validate(['hotel_question_id' => 'required']);

        if ($request->get('hotel_question_id') == $hotelQuestion->id && $hotelQuestion->delete()) {
            return redirect()->route('hotel_questions.index');
        }

        return back();
    }
}
