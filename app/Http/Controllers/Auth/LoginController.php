<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:subscriber')->except('logout');
        $this->middleware('guest:hotelier')->except('logout');
    }

    /**
     * Show the login form for the Subscribers.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showSubscriberLoginForm()
    {
        return view('auth.login', ['url' => 'subscriber']);
    }

    /**
     * Attempt to log in a Subscriber user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function subscriberLogin(Request $request)
    {
        // dd($request);
        // Check that the right credentials are supplied
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:8'
        ]);

        // Success log in
        if (Auth::guard('subscriber')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('/dashboard')->with('statusSubscriber', 'Welcome ');
        }

        // Bad credentials
        return back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }



    /**
     * Show the login form for the Hoteliers.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showHotelierLoginForm()
    {
        return view('auth.login', ['url' => 'hotelier']);
    }

    /**
     * Attempt to log in a Hotelier user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function hotelierLogin(Request $request)
    {
        // Check that the right credentials are supplied
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:8'
        ]);

        // Success log in
        if (Auth::guard('hotelier')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return redirect()->intended('/dashboard')->with('statusHotelier', 'Welcome ');
        }
        
        // Bad credentials
        return back()
            ->withInput($request->only('email', 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }
}
