<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Subscriber;
use App\Hotelier;
use App\Country;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Redirect;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:hotelier');
        $this->middleware('guest:subscriber');
    }

    /**
     * Show the register form for the Subscribers.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showSubscriberRegisterForm()
    {
        // Data to pass to the create view
        $countries = Country::all();

        return view('auth.register', ['url' => 'subscriber'])
            ->with('countries', $countries);
    }

    /**
     * Show the register form for the Hoteliers.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showHotelierRegisterForm()
    {
        // Data to pass to the create view
        $countries = Country::all();

        return view('auth.register', ['url' => 'hotelier'])
            ->with('countries', $countries);
    }

    /**
     * Get a validator for an incoming Subscriber registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorSubscriber(array $data)
    {
        return Validator::make($data, [
            'pseudo'        => ['required', 'string', 'max:30'],
            'password'      => ['required', 'string', 'min:8', 'confirmed'],
            'email'         => ['required', 'string', 'email', 'max:100', 'unique:subscribers'],
            'lastname'      => ['required', 'string', 'max:80'],
            'firstname'     => ['required', 'string', 'max:80'],
            'address_1'     => ['required', 'string', 'max:100'],
            'address_2'     => ['nullable', 'string', 'max:100'],
            'postcode'      => ['required', 'string', 'max:10'],
            'city'          => ['required', 'string', 'max:50'],
            'state'         => ['nullable', 'string', 'max:50'],
            'country_id'    => ['required', 'numeric'],
            'latitude'      => ['required', 'numeric'],
            'longitude'     => ['required', 'numeric'],
            'dialling_code' => ['required', 'numeric'],
            'phone'         => ['required', 'string', 'max:20'],
            'airport'       => ['nullable', 'string', 'max:50'],
        ]);       
    }

    /**
     * Get a validator for an incoming Hotelier registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorHotelier(array $data)
    {
        return Validator::make($data, [
            'password'      => ['required', 'string', 'min:8', 'confirmed'],
            'email'         => ['required', 'string', 'email', 'max:100', 'unique:subscribers'],
            'lastname'      => ['required', 'string', 'max:80'],
            'firstname'     => ['required', 'string', 'max:80'],
            'address_1'     => ['required', 'string', 'max:100'],
            'address_2'     => ['nullable', 'string', 'max:100'],
            'postcode'      => ['required', 'string', 'max:10'],
            'city'          => ['required', 'string', 'max:50'],
            'state'         => ['nullable', 'string', 'max:50'],
            'country_id'    => ['required', 'numeric'],
        ]);       
    }

    /**
     * Create a new Subscriber instance after a valid registration.
     *
     * @param  Request  $request
     * @return \App\Subscriber
     */
    protected function createSubscriber(Request $request)
    {
        // Validate the request fields
        $this->validatorSubscriber($request->all())->validate();

        // Create the Subscriber if success
        $subscriber = Subscriber::create([
            'pseudo'        => $request['pseudo'],
            'password'      => Hash::make($request['password']),
            'email'         => $request['email'],
            'lastname'      => $request['lastname'],
            'firstname'     => $request['firstname'],
            'address_1'     => $request['address_1'],
            'address_2'     => $request['address_2'],
            'postcode'      => $request['postcode'],
            'city'          => $request['city'],
            'state'         => $request['state'],
            'country_id'    => $request['country_id'],
            'latitude'      => $request['latitude'],
            'longitude'     => $request['longitude'],
            'dialling_code' => $request['dialling_code'],
            'phone'         => $request['phone'],
            'airport'       => $request['airport'],
        ]);

        return redirect()->route('subscribers.show', $subscriber);
    }   

    /**
     * Create a new Hotelier instance after a valid registration.
     *
     * @param  Request  $request
     * @return \App\Hotelier
     */
    protected function createHotelier(Request $request)
    {
        // Validate the request fields
        $this->validatorHotelier($request->all())->validate();

        // Create the Hotelier if success
        $hotelier = Hotelier::create([
            'password'      => Hash::make($request['password']),
            'email'         => $request['email'],
            'lastname'      => $request['lastname'],
            'firstname'     => $request['firstname'],
            'address_1'     => $request['address_1'],
            'address_2'     => $request['address_2'],
            'postcode'      => $request['postcode'],
            'city'          => $request['city'],
            'state'         => $request['state'],
            'country_id'    => $request['country_id'],
        ]);

        return redirect()->route('hoteliers.show', $hotelier);
    }

}
