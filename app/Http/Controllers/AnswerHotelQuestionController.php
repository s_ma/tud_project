<?php

namespace App\Http\Controllers;

use App\AnswerHotelQuestion;
use Illuminate\Http\Request;

class AnswerHotelQuestionController extends Controller
{
    /**
     * Display a listing of the answerHotelQuestion.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $answerHotelQuestionQuery = AnswerHotelQuestion::query();
        $answerHotelQuestionQuery->where('name', 'like', '%'.request('q').'%');
        $answerHotelQuestions = $answerHotelQuestionQuery->paginate(25);

        return view('answer_hotel_questions.index', compact('answerHotelQuestions'));
    }

    /**
     * Show the form for creating a new answerHotelQuestion.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new AnswerHotelQuestion);

        return view('answer_hotel_questions.create');
    }

    /**
     * Store a newly created answerHotelQuestion in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new AnswerHotelQuestion);

        $newAnswerHotelQuestion = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newAnswerHotelQuestion['creator_id'] = auth()->id();

        $answerHotelQuestion = AnswerHotelQuestion::create($newAnswerHotelQuestion);

        return redirect()->route('answer_hotel_questions.show', $answerHotelQuestion);
    }

    /**
     * Display the specified answerHotelQuestion.
     *
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return \Illuminate\View\View
     */
    public function show(AnswerHotelQuestion $answerHotelQuestion)
    {
        return view('answer_hotel_questions.show', compact('answerHotelQuestion'));
    }

    /**
     * Show the form for editing the specified answerHotelQuestion.
     *
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return \Illuminate\View\View
     */
    public function edit(AnswerHotelQuestion $answerHotelQuestion)
    {
        $this->authorize('update', $answerHotelQuestion);

        return view('answer_hotel_questions.edit', compact('answerHotelQuestion'));
    }

    /**
     * Update the specified answerHotelQuestion in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, AnswerHotelQuestion $answerHotelQuestion)
    {
        $this->authorize('update', $answerHotelQuestion);

        $answerHotelQuestionData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $answerHotelQuestion->update($answerHotelQuestionData);

        return redirect()->route('answer_hotel_questions.show', $answerHotelQuestion);
    }

    /**
     * Remove the specified answerHotelQuestion from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AnswerHotelQuestion  $answerHotelQuestion
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, AnswerHotelQuestion $answerHotelQuestion)
    {
        $this->authorize('delete', $answerHotelQuestion);

        $request->validate(['answer_hotel_question_id' => 'required']);

        if ($request->get('answer_hotel_question_id') == $answerHotelQuestion->id && $answerHotelQuestion->delete()) {
            return redirect()->route('answer_hotel_questions.index');
        }

        return back();
    }
}
