<?php

namespace App\Http\Controllers;

use App\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * Display a listing of the photo.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $photoQuery = Photo::query();
        $photoQuery->where('name', 'like', '%'.request('q').'%');
        $photos = $photoQuery->paginate(25);

        return view('photos.index', compact('photos'));
    }

    /**
     * Show the form for creating a new photo.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new Photo);

        return view('photos.create');
    }

    /**
     * Store a newly created photo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Photo);

        $newPhoto = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newPhoto['creator_id'] = auth()->id();

        $photo = Photo::create($newPhoto);

        return redirect()->route('photos.show', $photo);
    }

    /**
     * Display the specified photo.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\View\View
     */
    public function show(Photo $photo)
    {
        return view('photos.show', compact('photo'));
    }

    /**
     * Show the form for editing the specified photo.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\View\View
     */
    public function edit(Photo $photo)
    {
        $this->authorize('update', $photo);

        return view('photos.edit', compact('photo'));
    }

    /**
     * Update the specified photo in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Photo $photo)
    {
        $this->authorize('update', $photo);

        $photoData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $photo->update($photoData);

        return redirect()->route('photos.show', $photo);
    }

    /**
     * Remove the specified photo from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Photo $photo)
    {
        $this->authorize('delete', $photo);

        $request->validate(['photo_id' => 'required']);

        if ($request->get('photo_id') == $photo->id && $photo->delete()) {
            return redirect()->route('photos.index');
        }

        return back();
    }
}
