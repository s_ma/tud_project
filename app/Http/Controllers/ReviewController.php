<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    /**
     * Display a listing of the review.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $reviewQuery = Review::query();
        $reviewQuery->where('name', 'like', '%'.request('q').'%');
        $reviews = $reviewQuery->paginate(25);

        return view('reviews.index', compact('reviews'));
    }

    /**
     * Show the form for creating a new review.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new Review);

        return view('reviews.create');
    }

    /**
     * Store a newly created review in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Review);

        $newReview = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newReview['creator_id'] = auth()->id();

        $review = Review::create($newReview);

        return redirect()->route('reviews.show', $review);
    }

    /**
     * Display the specified review.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\View\View
     */
    public function show(Review $review)
    {
        return view('reviews.show', compact('review'));
    }

    /**
     * Show the form for editing the specified review.
     *
     * @param  \App\Review  $review
     * @return \Illuminate\View\View
     */
    public function edit(Review $review)
    {
        $this->authorize('update', $review);

        return view('reviews.edit', compact('review'));
    }

    /**
     * Update the specified review in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Review $review)
    {
        $this->authorize('update', $review);

        $reviewData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $review->update($reviewData);

        return redirect()->route('reviews.show', $review);
    }

    /**
     * Remove the specified review from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Review  $review
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Review $review)
    {
        $this->authorize('delete', $review);

        $request->validate(['review_id' => 'required']);

        if ($request->get('review_id') == $review->id && $review->delete()) {
            return redirect()->route('reviews.index');
        }

        return back();
    }
}
