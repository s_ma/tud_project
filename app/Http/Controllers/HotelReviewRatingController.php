<?php

namespace App\Http\Controllers;

use App\HotelReviewRating;
use Illuminate\Http\Request;

class HotelReviewRatingController extends Controller
{
    /**
     * Display a listing of the hotelReviewRating.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelReviewRatingQuery = HotelReviewRating::query();
        $hotelReviewRatingQuery->where('name', 'like', '%'.request('q').'%');
        $hotelReviewRatings = $hotelReviewRatingQuery->paginate(25);

        return view('hotel_review_ratings.index', compact('hotelReviewRatings'));
    }

    /**
     * Show the form for creating a new hotelReviewRating.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new HotelReviewRating);

        return view('hotel_review_ratings.create');
    }

    /**
     * Store a newly created hotelReviewRating in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new HotelReviewRating);

        $newHotelReviewRating = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newHotelReviewRating['creator_id'] = auth()->id();

        $hotelReviewRating = HotelReviewRating::create($newHotelReviewRating);

        return redirect()->route('hotel_review_ratings.show', $hotelReviewRating);
    }

    /**
     * Display the specified hotelReviewRating.
     *
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return \Illuminate\View\View
     */
    public function show(HotelReviewRating $hotelReviewRating)
    {
        return view('hotel_review_ratings.show', compact('hotelReviewRating'));
    }

    /**
     * Show the form for editing the specified hotelReviewRating.
     *
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return \Illuminate\View\View
     */
    public function edit(HotelReviewRating $hotelReviewRating)
    {
        $this->authorize('update', $hotelReviewRating);

        return view('hotel_review_ratings.edit', compact('hotelReviewRating'));
    }

    /**
     * Update the specified hotelReviewRating in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, HotelReviewRating $hotelReviewRating)
    {
        $this->authorize('update', $hotelReviewRating);

        $hotelReviewRatingData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $hotelReviewRating->update($hotelReviewRatingData);

        return redirect()->route('hotel_review_ratings.show', $hotelReviewRating);
    }

    /**
     * Remove the specified hotelReviewRating from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelReviewRating  $hotelReviewRating
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, HotelReviewRating $hotelReviewRating)
    {
        $this->authorize('delete', $hotelReviewRating);

        $request->validate(['hotel_review_rating_id' => 'required']);

        if ($request->get('hotel_review_rating_id') == $hotelReviewRating->id && $hotelReviewRating->delete()) {
            return redirect()->route('hotel_review_ratings.index');
        }

        return back();
    }
}
