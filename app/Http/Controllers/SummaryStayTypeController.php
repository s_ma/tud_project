<?php

namespace App\Http\Controllers;

use App\SummaryStayType;
use Illuminate\Http\Request;

class SummaryStayTypeController extends Controller
{
    /**
     * Display a listing of the summaryStayType.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $summaryStayTypeQuery = SummaryStayType::query();
        $summaryStayTypeQuery->where('name', 'like', '%'.request('q').'%');
        $summaryStayTypes = $summaryStayTypeQuery->paginate(25);

        return view('summary_stay_types.index', compact('summaryStayTypes'));
    }

    /**
     * Show the form for creating a new summaryStayType.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new SummaryStayType);

        return view('summary_stay_types.create');
    }

    /**
     * Store a newly created summaryStayType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new SummaryStayType);

        $newSummaryStayType = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newSummaryStayType['creator_id'] = auth()->id();

        $summaryStayType = SummaryStayType::create($newSummaryStayType);

        return redirect()->route('summary_stay_types.show', $summaryStayType);
    }

    /**
     * Display the specified summaryStayType.
     *
     * @param  \App\SummaryStayType  $summaryStayType
     * @return \Illuminate\View\View
     */
    public function show(SummaryStayType $summaryStayType)
    {
        return view('summary_stay_types.show', compact('summaryStayType'));
    }

    /**
     * Show the form for editing the specified summaryStayType.
     *
     * @param  \App\SummaryStayType  $summaryStayType
     * @return \Illuminate\View\View
     */
    public function edit(SummaryStayType $summaryStayType)
    {
        $this->authorize('update', $summaryStayType);

        return view('summary_stay_types.edit', compact('summaryStayType'));
    }

    /**
     * Update the specified summaryStayType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SummaryStayType  $summaryStayType
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, SummaryStayType $summaryStayType)
    {
        $this->authorize('update', $summaryStayType);

        $summaryStayTypeData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $summaryStayType->update($summaryStayTypeData);

        return redirect()->route('summary_stay_types.show', $summaryStayType);
    }

    /**
     * Remove the specified summaryStayType from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SummaryStayType  $summaryStayType
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, SummaryStayType $summaryStayType)
    {
        $this->authorize('delete', $summaryStayType);

        $request->validate(['summary_stay_type_id' => 'required']);

        if ($request->get('summary_stay_type_id') == $summaryStayType->id && $summaryStayType->delete()) {
            return redirect()->route('summary_stay_types.index');
        }

        return back();
    }
}
