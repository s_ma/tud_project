<?php

namespace App\Http\Controllers;

use App\ReviewUsefulness;
use Illuminate\Http\Request;

class ReviewUsefulnessController extends Controller
{
    /**
     * Display a listing of the reviewUsefulness.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $reviewUsefulnessQuery = ReviewUsefulness::query();
        $reviewUsefulnessQuery->where('name', 'like', '%'.request('q').'%');
        $reviewUsefulnesses = $reviewUsefulnessQuery->paginate(25);

        return view('review_usefulnesses.index', compact('reviewUsefulnesses'));
    }

    /**
     * Show the form for creating a new reviewUsefulness.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new ReviewUsefulness);

        return view('review_usefulnesses.create');
    }

    /**
     * Store a newly created reviewUsefulness in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new ReviewUsefulness);

        $newReviewUsefulness = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newReviewUsefulness['creator_id'] = auth()->id();

        $reviewUsefulness = ReviewUsefulness::create($newReviewUsefulness);

        return redirect()->route('review_usefulnesses.show', $reviewUsefulness);
    }

    /**
     * Display the specified reviewUsefulness.
     *
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return \Illuminate\View\View
     */
    public function show(ReviewUsefulness $reviewUsefulness)
    {
        return view('review_usefulnesses.show', compact('reviewUsefulness'));
    }

    /**
     * Show the form for editing the specified reviewUsefulness.
     *
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return \Illuminate\View\View
     */
    public function edit(ReviewUsefulness $reviewUsefulness)
    {
        $this->authorize('update', $reviewUsefulness);

        return view('review_usefulnesses.edit', compact('reviewUsefulness'));
    }

    /**
     * Update the specified reviewUsefulness in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, ReviewUsefulness $reviewUsefulness)
    {
        $this->authorize('update', $reviewUsefulness);

        $reviewUsefulnessData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $reviewUsefulness->update($reviewUsefulnessData);

        return redirect()->route('review_usefulnesses.show', $reviewUsefulness);
    }

    /**
     * Remove the specified reviewUsefulness from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReviewUsefulness  $reviewUsefulness
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, ReviewUsefulness $reviewUsefulness)
    {
        $this->authorize('delete', $reviewUsefulness);

        $request->validate(['review_usefulness_id' => 'required']);

        if ($request->get('review_usefulness_id') == $reviewUsefulness->id && $reviewUsefulness->delete()) {
            return redirect()->route('review_usefulnesses.index');
        }

        return back();
    }
}
