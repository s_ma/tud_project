<?php

namespace App\Http\Controllers;

use App\Favourite;
use Illuminate\Http\Request;

class FavouriteController extends Controller
{
    /**
     * Display a listing of the favourite.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $favouriteQuery = Favourite::query();
        $favouriteQuery->where('name', 'like', '%'.request('q').'%');
        $favourites = $favouriteQuery->paginate(25);

        return view('favourites.index', compact('favourites'));
    }

    /**
     * Show the form for creating a new favourite.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new Favourite);

        return view('favourites.create');
    }

    /**
     * Store a newly created favourite in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Favourite);

        $newFavourite = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newFavourite['creator_id'] = auth()->id();

        $favourite = Favourite::create($newFavourite);

        return redirect()->route('favourites.show', $favourite);
    }

    /**
     * Display the specified favourite.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\View\View
     */
    public function show(Favourite $favourite)
    {
        return view('favourites.show', compact('favourite'));
    }

    /**
     * Show the form for editing the specified favourite.
     *
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\View\View
     */
    public function edit(Favourite $favourite)
    {
        $this->authorize('update', $favourite);

        return view('favourites.edit', compact('favourite'));
    }

    /**
     * Update the specified favourite in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Favourite $favourite)
    {
        $this->authorize('update', $favourite);

        $favouriteData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $favourite->update($favouriteData);

        return redirect()->route('favourites.show', $favourite);
    }

    /**
     * Remove the specified favourite from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favourite  $favourite
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Favourite $favourite)
    {
        $this->authorize('delete', $favourite);

        $request->validate(['favourite_id' => 'required']);

        if ($request->get('favourite_id') == $favourite->id && $favourite->delete()) {
            return redirect()->route('favourites.index');
        }

        return back();
    }
}
