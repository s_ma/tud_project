<?php

namespace App\Http\Controllers;

use App\Alias;
use Illuminate\Http\Request;

class AliasController extends Controller
{
    /**
     * Display a listing of the alias.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $aliasQuery = Alias::query();
        $aliasQuery->where('name', 'like', '%'.request('q').'%');
        $aliases = $aliasQuery->paginate(25);

        return view('aliases.index', compact('aliases'));
    }

    /**
     * Show the form for creating a new alias.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new Alias);

        return view('aliases.create');
    }

    /**
     * Store a newly created alias in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new Alias);

        $newAlias = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newAlias['creator_id'] = auth()->id();

        $alias = Alias::create($newAlias);

        return redirect()->route('aliases.show', $alias);
    }

    /**
     * Display the specified alias.
     *
     * @param  \App\Alias  $alias
     * @return \Illuminate\View\View
     */
    public function show(Alias $alias)
    {
        return view('aliases.show', compact('alias'));
    }

    /**
     * Show the form for editing the specified alias.
     *
     * @param  \App\Alias  $alias
     * @return \Illuminate\View\View
     */
    public function edit(Alias $alias)
    {
        $this->authorize('update', $alias);

        return view('aliases.edit', compact('alias'));
    }

    /**
     * Update the specified alias in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alias  $alias
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, Alias $alias)
    {
        $this->authorize('update', $alias);

        $aliasData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $alias->update($aliasData);

        return redirect()->route('aliases.show', $alias);
    }

    /**
     * Remove the specified alias from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alias  $alias
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, Alias $alias)
    {
        $this->authorize('delete', $alias);

        $request->validate(['alias_id' => 'required']);

        if ($request->get('alias_id') == $alias->id && $alias->delete()) {
            return redirect()->route('aliases.index');
        }

        return back();
    }
}
