<?php

namespace App\Http\Controllers;

use App\HotelReviewType;
use Illuminate\Http\Request;

class HotelReviewTypeController extends Controller
{
    /**
     * Display a listing of the hotelReviewType.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $hotelReviewTypeQuery = HotelReviewType::query();
        $hotelReviewTypeQuery->where('name', 'like', '%'.request('q').'%');
        $hotelReviewTypes = $hotelReviewTypeQuery->paginate(25);

        return view('hotel_review_types.index', compact('hotelReviewTypes'));
    }

    /**
     * Show the form for creating a new hotelReviewType.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->authorize('create', new HotelReviewType);

        return view('hotel_review_types.create');
    }

    /**
     * Store a newly created hotelReviewType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->authorize('create', new HotelReviewType);

        $newHotelReviewType = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $newHotelReviewType['creator_id'] = auth()->id();

        $hotelReviewType = HotelReviewType::create($newHotelReviewType);

        return redirect()->route('hotel_review_types.show', $hotelReviewType);
    }

    /**
     * Display the specified hotelReviewType.
     *
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return \Illuminate\View\View
     */
    public function show(HotelReviewType $hotelReviewType)
    {
        return view('hotel_review_types.show', compact('hotelReviewType'));
    }

    /**
     * Show the form for editing the specified hotelReviewType.
     *
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return \Illuminate\View\View
     */
    public function edit(HotelReviewType $hotelReviewType)
    {
        $this->authorize('update', $hotelReviewType);

        return view('hotel_review_types.edit', compact('hotelReviewType'));
    }

    /**
     * Update the specified hotelReviewType in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return \Illuminate\Routing\Redirector
     */
    public function update(Request $request, HotelReviewType $hotelReviewType)
    {
        $this->authorize('update', $hotelReviewType);

        $hotelReviewTypeData = $request->validate([
            'name'        => 'required|max:60',
            'description' => 'nullable|max:255',
        ]);
        $hotelReviewType->update($hotelReviewTypeData);

        return redirect()->route('hotel_review_types.show', $hotelReviewType);
    }

    /**
     * Remove the specified hotelReviewType from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HotelReviewType  $hotelReviewType
     * @return \Illuminate\Routing\Redirector
     */
    public function destroy(Request $request, HotelReviewType $hotelReviewType)
    {
        $this->authorize('delete', $hotelReviewType);

        $request->validate(['hotel_review_type_id' => 'required']);

        if ($request->get('hotel_review_type_id') == $hotelReviewType->id && $hotelReviewType->delete()) {
            return redirect()->route('hotel_review_types.index');
        }

        return back();
    }
}
