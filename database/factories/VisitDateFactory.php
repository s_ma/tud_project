<?php

use App\User;
use App\VisitDate;
use Faker\Generator as Faker;

$factory->define(VisitDate::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        'creator_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});
