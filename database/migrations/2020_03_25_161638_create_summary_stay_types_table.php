<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryStayTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * JOIN table
         */
        Schema::create('summary_stay_types', function (Blueprint $table) {
            // Columns
            $table->unsignedInteger('hotel_id');
            $table->unsignedInteger('visit_type_id');
            $table->integer('review_type_count');
            $table->timestamps();

            // PK Constraints
            $table->primary(['hotel_id', 'visit_type_id']);

            // FK Constraints
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
            $table->foreign('visit_type_id')->references('id')->on('visit_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_stay_types');
    }
}
