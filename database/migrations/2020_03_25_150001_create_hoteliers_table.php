<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHoteliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoteliers', function (Blueprint $table) {
            // Columns
            $table->increments('id');   // PK
            $table->string('email', 100);
            $table->string('password');
            $table->string('lastname', 80);
            $table->string('firstname', 80);
            $table->string('address_1', 100);
            $table->string('address_2', 100)->nullable();
            $table->string('postcode', 10);
            $table->string('city', 50);
            $table->string('state', 50)->nullable();
            $table->unsignedInteger('country_id');
            $table->rememberToken();
            $table->timestamps();

            // FK Constraints
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoteliers');
    }
}
