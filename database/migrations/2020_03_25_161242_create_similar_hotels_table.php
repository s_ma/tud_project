<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimilarHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * JOIN table (Reflexive relationship)
         */
        Schema::create('similar_hotels', function (Blueprint $table) {
            // Columns
            $table->unsignedInteger('hotel_id');
            $table->unsignedInteger('similar_hotel_id');
            $table->timestamps();

            // PK Constraints
            $table->primary(['hotel_id', 'similar_hotel_id']);

            // FK Constraints
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
            $table->foreign('similar_hotel_id')->references('id')->on('hotels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('similar_hotels');
    }
}
