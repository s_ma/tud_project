<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerHotelQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * JOIN table
         */
        Schema::create('answer_hotel_questions', function (Blueprint $table) {
            // Columns
            $table->unsignedInteger('review_id');
            $table->unsignedInteger('hotel_question_id');
            $table->string('answer', 20);
            $table->timestamps();

            // PK Constraints
            $table->primary(['review_id', 'hotel_question_id']);

            // FK Constraints
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
            $table->foreign('hotel_question_id')->references('id')->on('hotel_questions')->onDelete('cascade');
        });

        // Check constraint (cannot add it in Blueprint)
        // DB::statement("ALTER TABLE answer_hotel_questions ADD CONSTRAINT CK_AHQ_ANSWER CHECK answer in ('Yes','No','Unknown');");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_hotel_questions');
    }
}
