<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelReviewRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * JOIN table
         */  
        Schema::create('hotel_review_ratings', function (Blueprint $table) {
            // Columns
            $table->unsignedInteger('review_id');
            $table->unsignedInteger('hotel_review_type_id');
            $table->integer('score');
            $table->timestamps();

            // PK Constraints
            $table->primary(['review_id', 'hotel_review_type_id']);

            // FK Constraints
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
            $table->foreign('hotel_review_type_id')->references('id')->on('hotel_review_types')->onDelete('cascade');
        });

        // Check constraint (cannot add it in Blueprint)
        DB::statement('ALTER TABLE hotel_review_ratings ADD CONSTRAINT CK_HRR_SCORE CHECK (score between 1 and 5);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel_review_ratings');
    }
}
