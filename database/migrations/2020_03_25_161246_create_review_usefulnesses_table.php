<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewUsefulnessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * JOIN table
         */
        Schema::create('review_usefulness', function (Blueprint $table) {
            // Columns
            $table->unsignedInteger('subscriber_id');
            $table->unsignedInteger('review_id');
            $table->timestamps();

            // PK Constraints
            $table->primary(['subscriber_id', 'review_id']);

            // FK Constraints
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onDelete('cascade');
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_usefulnesses');
    }
}
