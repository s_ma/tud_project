<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            // Columns
            $table->increments('id');   // PK
            $table->unsignedInteger('hotelier_id');
            $table->unsignedInteger('price_range_id');
            $table->string('name', 100);
            $table->string('description', 500)->nullable();
            $table->string('address_1', 100);
            $table->string('address_2', 100)->nullable();
            $table->string('postcode', 10);
            $table->string('city', 50);
            $table->string('state', 50)->nullable();
            $table->unsignedInteger('country_id');
            $table->float('latitude', 8, 6);
            $table->float('longitude', 8, 6);
            $table->unsignedInteger('stars');
            $table->integer('dialling_code');
            $table->string('phone', 20);
            $table->string('email', 80);
            $table->string('website', 100)->nullable();
            $table->integer('number_of_rooms')->nullable();
            $table->timestamps();

            // FK Constraints
            $table->foreign('hotelier_id')->references('id')->on('hoteliers')->onDelete('restrict');
            $table->foreign('price_range_id')->references('id')->on('price_ranges')->onDelete('restrict');
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('restrict');
            $table->foreign('stars')->references('stars_number')->on('hotel_categories')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotels');
    }
}
