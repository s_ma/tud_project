<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            // Columns
            $table->increments('id');   // PK
            $table->unsignedInteger('hotel_id')->nullable();
            $table->unsignedInteger('review_id')->nullable();
            $table->string('url', 150);
            $table->timestamps();

            // FK Constraints
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('restrict');
            $table->foreign('review_id')->references('id')->on('reviews')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
