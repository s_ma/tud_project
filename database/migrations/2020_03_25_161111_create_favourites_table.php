<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavouritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * JOIN table
         */   
        Schema::create('favourites', function (Blueprint $table) {        
            // Columns
            $table->unsignedInteger('subscriber_id');
            $table->unsignedInteger('hotel_id');
            $table->timestamps();

            // PK Constraints
            $table->primary(['subscriber_id', 'hotel_id']);

            // FK Constraints
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onDelete('cascade');
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favourites');
    }
}
