<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            // Columns
            $table->increments('id');   // PK
            $table->unsignedInteger('subscriber_id');
            $table->unsignedInteger('hotel_id');
            $table->unsignedInteger('visit_type_id');
            $table->unsignedInteger('visit_date_id');
            $table->date('review_date');
            $table->string('language', 50);
            $table->string('title', 100);
            $table->string('detail', 1500);
            $table->integer('global_score');
            $table->string('advice', 300)->nullable();
            $table->string('review_answer', 1000)->nullable();
            $table->timestamps();

            // FK Constraints
            $table->foreign('subscriber_id')->references('id')->on('subscribers')->onDelete('cascade');
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
            $table->foreign('visit_type_id')->references('id')->on('visit_types')->onDelete('cascade');
            $table->foreign('visit_date_id')->references('id')->on('visit_dates')->onDelete('cascade');
        });

        // Check constraint (cannot add it in Blueprint)
        // DB::statement("ALTER TABLE reviews ADD CONSTRAINT CK_ANS_GLOBAL_SCORE CHECK (global_score BETWEEN 1 AND 5);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
