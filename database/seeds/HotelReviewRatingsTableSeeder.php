<?php

use Illuminate\Database\Seeder;

class HotelReviewRatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\HotelReviewRating::create([
            'review_id'              => 1,
            'hotel_review_type_id'   => 1,
            'score'                  => 5
        ]);
        App\HotelReviewRating::create([
            'review_id'              => 1,
            'hotel_review_type_id'   => 2,
            'score'                  => 5
        ]);
    }
}
