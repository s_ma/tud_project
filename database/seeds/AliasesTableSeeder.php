<?php

use Illuminate\Database\Seeder;

class AliasesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Alias::create([
            'hotel_id'      => 1,
            'description'   => 'Carlton PA.',
        ]);
        App\Alias::create([
            'hotel_id'      => 1,
            'description'   => 'ParisCarltonHotel',
        ]);
        App\Alias::create([
            'hotel_id'      => 1,
            'description'   => 'PCH',
        ]);       
    }
}
