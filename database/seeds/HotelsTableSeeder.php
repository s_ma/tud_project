<?php

use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Hotel::create([
            'hotelier_id'       => 1,
            'price_range_id'    => 1,
            'name'              => 'Carlton Paris',
            'description'       => 'The hotel reference in Paris!',
            'address_1'         => '75 rue des clos',
            'postcode'          => '75001',
            'city'              => 'Paris',
            'country_id'        => 81,
            'latitude'          => 45.921256,
            'longitude'         => 6.144095,
            'stars'             => 4,
            'dialling_code'     => 33,
            'phone'             => '550369845',
            'email'             => 'carltonparis@outlook.com',
            'website'           => 'carlton-paris-hotel.com',
            'number_of_rooms'    => 258,
        ]);
        App\Hotel::create([
            'hotelier_id'       => 2,
            'price_range_id'    => 2,
            'name'              => 'Le Clos des Sens',
            'address_1'         => '13 Rue Jean Mermoz',
            'postcode'          => '74940',
            'city'              => 'Annecy-le-Vieux',
            'country_id'        => 81,
            'latitude'          => 45.917074,
            'longitude'         => 6.14548,
            'stars'             => 3,
            'dialling_code'     => 33,
            'phone'             => '450230790',
            'email'             => 'closdessens@gmail.com',
            'website'           => 'http://www.closdessens.com',
            'number_of_rooms'    => 15,
        ]);
    }
}
