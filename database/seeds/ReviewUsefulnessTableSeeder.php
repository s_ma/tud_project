<?php

use Illuminate\Database\Seeder;

class ReviewUsefulnessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\ReviewUsefulness::create([
            'subscriber_id' => 1,
            'review_id'     => 1
        ]);
    }
}
