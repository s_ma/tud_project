<?php

use Illuminate\Database\Seeder;

class HotelServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\HotelService::create([
            'hotel_id'      => 1,
            'service_id'    => 2
        ]);
        App\HotelService::create([
            'hotel_id'      => 1,
            'service_id'    => 3
        ]);
        App\HotelService::create([
            'hotel_id'      => 2,
            'service_id'    => 4
        ]);
    }
}
