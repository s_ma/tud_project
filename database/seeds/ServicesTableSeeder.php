<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Service::create([
            'description' => 'Free High Speed Internet'
        ]);
        App\Service::create([
            'description' => 'Room Service'
        ]);
        App\Service::create([
            'description' => 'Swimming pool'
        ]);
        App\Service::create([
            'description' => 'Restaurant'
        ]);
        App\Service::create([
            'description' => 'Suite'
        ]);
        App\Service::create([
            'description' => 'Disabled access'
        ]);
        App\Service::create([
            'description' => 'Bar/Lounge'
        ]);
        App\Service::create([
            'description' => 'Car Park'
        ]);
        App\Service::create([
            'description' => 'Bus service'
        ]);    
        App\Service::create([
            'description' => 'Airport Shuttle'
        ]);
        App\Service::create([
            'description' => 'Casino and gambling'
        ]);
        App\Service::create([
            'description' => 'Fitness centre'
        ]);

    }
}
