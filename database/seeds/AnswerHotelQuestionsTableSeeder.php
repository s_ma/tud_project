<?php

use Illuminate\Database\Seeder;

class AnswerHotelQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\AnswerHotelQuestion::create([
            'review_id'             => 1,
            'hotel_question_id'     => 1,
            'answer'                => 'Yes'
        ]);
    }
}
