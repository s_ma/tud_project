<?php

use Illuminate\Database\Seeder;

class HotelCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\HotelCategory::create([]);  // 1 star
        App\HotelCategory::create([]);  // 2 stars
        App\HotelCategory::create([]);  // 3 stars
        App\HotelCategory::create([]);  // 4 stars
        App\HotelCategory::create([]);  // 5 stars
    }
}
