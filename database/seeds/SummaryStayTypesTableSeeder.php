<?php

use Illuminate\Database\Seeder;

class SummaryStayTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\SummaryStayType::create([
            'hotel_id'              => 1,
            'visit_type_id'         => 1,
            'review_type_count'     => 50
        ]);
    }
}
