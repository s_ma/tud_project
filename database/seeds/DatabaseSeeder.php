<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HotelCategoriesTableSeeder::class);
        $this->call(PriceRangesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(VisitDatesTableSeeder::class);
        $this->call(HotelQuestionsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(HotelReviewTypesTableSeeder::class);
        $this->call(VisitTypesTableSeeder::class);

        $this->call(HoteliersTableSeeder::class);
        $this->call(SubscribersTableSeeder::class);                 
        $this->call(HotelsTableSeeder::class);           
        $this->call(AliasesTableSeeder::class);                 
        $this->call(ReviewsTableSeeder::class);        

        $this->call(FavouritesTableSeeder::class);                 
        $this->call(SimilarHotelsTableSeeder::class);         
        $this->call(HotelServiceTableSeeder::class);                 
        $this->call(ReviewUsefulnessTableSeeder::class);                 
        $this->call(HotelReviewRatingsTableSeeder::class);                 
        $this->call(AnswerHotelQuestionsTableSeeder::class);                 
        $this->call(SummaryStayTypesTableSeeder::class);                 

        
        
    }
}
