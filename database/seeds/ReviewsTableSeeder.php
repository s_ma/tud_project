<?php

use Illuminate\Database\Seeder;

class ReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Review::create([
            'subscriber_id'     => 1,
            'hotel_id'          => 1,
            'visit_type_id'     => 1,
            'visit_date_id'     => 1,
            'review_date'       => date('Y-m-d H:i:s'),
            'language'          => 'English',
            'title'             => 'Superb!',
            'detail'            => 'Terrace overlooking Annecy. Spending an evening in this place was marvelous.',
            'global_score'      => 5,
            'advice'            => 'For a romantic weekend'
        ]);
    }
}
