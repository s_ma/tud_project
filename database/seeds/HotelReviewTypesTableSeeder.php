<?php

use Illuminate\Database\Seeder;

class HotelReviewTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\HotelReviewType::create([
            'description' => 'Service'
        ]);
        App\HotelReviewType::create([
            'description' => 'Room'
        ]);
        App\HotelReviewType::create([
            'description' => 'Location'
        ]);
    }
}
