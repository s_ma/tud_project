<?php

use Illuminate\Database\Seeder;

class PriceRangesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\PriceRange::create([
            'range' => '0€ - 52€'
        ]);
        App\PriceRange::create([
            'range' => '52€ - 117€'
        ]);
        App\PriceRange::create([
            'range' => '117€ - 169€'
        ]);
        App\PriceRange::create([
            'range' => '+169 €'
        ]);
    }
}
