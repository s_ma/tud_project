<?php

use Illuminate\Database\Seeder;

class SimilarHotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\SimilarHotel::create([
            'hotel_id' => 1,
            'similar_hotel_id' => 2
        ]);
    }
}
