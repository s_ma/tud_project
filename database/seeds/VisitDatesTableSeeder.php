<?php

use Carbon\Carbon;

use Illuminate\Database\Seeder;

class VisitDatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2010-01-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2011-02-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2012-03-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2013-04-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2014-05-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2015-06-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2016-07-01') ]);
        DB::table('visit_dates')->insert([ 'visit_date' => Carbon::parse('2017-08-01') ]);
    }
}
