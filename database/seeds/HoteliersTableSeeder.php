<?php

use Illuminate\Database\Seeder;

class HoteliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Hotelier::create([
            'email'         => 'john.doe@gmail.com',
            'password'      =>  bcrypt('password1'),
            'lastname'      => 'Doe',
            'firstname'     => 'John',
            'address_1'     => 'Avenue des Champs Elysées',
            'address_2'     => 'Berriere',
            'postcode'      => '75000',
            'city'          => 'Paris',
            'state'         => 'Ile-de-France',
            'country_id'    => 81
        ]);

        App\Hotelier::create([
            'email'         => 'margaret.burns@gmail.com',
            'password'      => bcrypt('password2'),
            'lastname'      => 'Burns',
            'firstname'     => 'Margaret',
            'address_1'     => 'Avenue de la Paix',
            'postcode'      => '89632',
            'city'          => 'Rouen',
            'country_id'    => 81
        ]);
    }
}
