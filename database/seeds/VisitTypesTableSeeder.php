<?php

use Illuminate\Database\Seeder;

class VisitTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\VisitType::create([
            'description' => 'Couple'
        ]);
        App\VisitType::create([
            'description' => 'Family'
        ]);
        App\VisitType::create([
            'description' => 'Friends'
        ]);
        App\VisitType::create([
            'description' => 'Business'
        ]);
        App\VisitType::create([
            'description' => 'Solo'
        ]);


    }
}
