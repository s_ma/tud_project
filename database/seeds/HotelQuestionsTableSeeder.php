<?php

use Illuminate\Database\Seeder;

class HotelQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
            'Does this hotel offer room service?',
            'Is this hotel luxurious?',
            'Is this a non-smoking hotel?',
            'Does this hotel have a common room for families?',
            'Is this hotel modern in style?',
            'Is this hotel suitable for work?',
            'Does this hotel offer free internet access?'
        ];

        foreach ($questions as $q) {
            App\HotelQuestion::create([
                'question' => $q
            ]);
        }
        
    }
}
