<?php

use Illuminate\Database\Seeder;

class FavouritesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Favourite::create([
            'subscriber_id' => 1,
            'hotel_id'      => 1
        ]);
        App\Favourite::create([
            'subscriber_id' => 1,
            'hotel_id'      => 2
        ]);
    }
}
