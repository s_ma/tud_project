<?php

use Illuminate\Database\Seeder;

class SubscribersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Subscriber::create([
            'pseudo'        => 'alysx',
            'password'      => bcrypt('subscriber'),
            'email'         => 'alys.fehn@gmail.com',
            'lastname'      => 'Fehn',
            'firstname'     => 'Alys',
            'address_1'     => 'A41 Watford Bypass',
            'postcode'      => 'WD25 8JH',
            'city'          => 'Watford',
            'country_id'    => 25,
            'latitude'      => 45.145131,
            'longitude'     => 6.150237,
            'dialling_code' => '+353',
            'phone'         => '450230793',
            'airport'       => 'Dublin Airport'
        ]);

    }
}
