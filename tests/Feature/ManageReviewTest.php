<?php

namespace Tests\Feature;

use App\Review;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageReviewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_review_list_in_review_index_page()
    {
        $review = factory(Review::class)->create();

        $this->loginAsUser();
        $this->visitRoute('reviews.index');
        $this->see($review->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Review 1 name',
            'description' => 'Review 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_review()
    {
        $this->loginAsUser();
        $this->visitRoute('reviews.index');

        $this->click(__('review.create'));
        $this->seeRouteIs('reviews.create');

        $this->submitForm(__('review.create'), $this->getCreateFields());

        $this->seeRouteIs('reviews.show', Review::first());

        $this->seeInDatabase('reviews', $this->getCreateFields());
    }

    /** @test */
    public function validate_review_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('reviews.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('reviews.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('reviews.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Review 1 name',
            'description' => 'Review 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_review()
    {
        $this->loginAsUser();
        $review = factory(Review::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('reviews.show', $review);
        $this->click('edit-review-'.$review->id);
        $this->seeRouteIs('reviews.edit', $review);

        $this->submitForm(__('review.update'), $this->getEditFields());

        $this->seeRouteIs('reviews.show', $review);

        $this->seeInDatabase('reviews', $this->getEditFields([
            'id' => $review->id,
        ]));
    }

    /** @test */
    public function validate_review_name_update_is_required()
    {
        $this->loginAsUser();
        $review = factory(Review::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('reviews.update', $review), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $review = factory(Review::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('reviews.update', $review), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $review = factory(Review::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('reviews.update', $review), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_review()
    {
        $this->loginAsUser();
        $review = factory(Review::class)->create();
        factory(Review::class)->create();

        $this->visitRoute('reviews.edit', $review);
        $this->click('del-review-'.$review->id);
        $this->seeRouteIs('reviews.edit', [$review, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('reviews', [
            'id' => $review->id,
        ]);
    }
}
