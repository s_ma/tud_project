<?php

namespace Tests\Feature;

use App\SimilarHotel;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageSimilarHotelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_similar_hotel_list_in_similar_hotel_index_page()
    {
        $similarHotel = factory(SimilarHotel::class)->create();

        $this->loginAsUser();
        $this->visitRoute('similar_hotels.index');
        $this->see($similarHotel->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'SimilarHotel 1 name',
            'description' => 'SimilarHotel 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_similar_hotel()
    {
        $this->loginAsUser();
        $this->visitRoute('similar_hotels.index');

        $this->click(__('similar_hotel.create'));
        $this->seeRouteIs('similar_hotels.create');

        $this->submitForm(__('similar_hotel.create'), $this->getCreateFields());

        $this->seeRouteIs('similar_hotels.show', SimilarHotel::first());

        $this->seeInDatabase('similar_hotels', $this->getCreateFields());
    }

    /** @test */
    public function validate_similar_hotel_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('similar_hotels.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_similar_hotel_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('similar_hotels.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_similar_hotel_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('similar_hotels.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'SimilarHotel 1 name',
            'description' => 'SimilarHotel 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_similar_hotel()
    {
        $this->loginAsUser();
        $similarHotel = factory(SimilarHotel::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('similar_hotels.show', $similarHotel);
        $this->click('edit-similar_hotel-'.$similarHotel->id);
        $this->seeRouteIs('similar_hotels.edit', $similarHotel);

        $this->submitForm(__('similar_hotel.update'), $this->getEditFields());

        $this->seeRouteIs('similar_hotels.show', $similarHotel);

        $this->seeInDatabase('similar_hotels', $this->getEditFields([
            'id' => $similarHotel->id,
        ]));
    }

    /** @test */
    public function validate_similar_hotel_name_update_is_required()
    {
        $this->loginAsUser();
        $similar_hotel = factory(SimilarHotel::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('similar_hotels.update', $similar_hotel), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_similar_hotel_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $similar_hotel = factory(SimilarHotel::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('similar_hotels.update', $similar_hotel), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_similar_hotel_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $similar_hotel = factory(SimilarHotel::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('similar_hotels.update', $similar_hotel), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_similar_hotel()
    {
        $this->loginAsUser();
        $similarHotel = factory(SimilarHotel::class)->create();
        factory(SimilarHotel::class)->create();

        $this->visitRoute('similar_hotels.edit', $similarHotel);
        $this->click('del-similar_hotel-'.$similarHotel->id);
        $this->seeRouteIs('similar_hotels.edit', [$similarHotel, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('similar_hotels', [
            'id' => $similarHotel->id,
        ]);
    }
}
