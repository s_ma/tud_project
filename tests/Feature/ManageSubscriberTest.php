<?php

namespace Tests\Feature;

use App\Subscriber;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageSubscriberTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_subscriber_list_in_subscriber_index_page()
    {
        $subscriber = factory(Subscriber::class)->create();

        $this->loginAsUser();
        $this->visitRoute('subscribers.index');
        $this->see($subscriber->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Subscriber 1 name',
            'description' => 'Subscriber 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_subscriber()
    {
        $this->loginAsUser();
        $this->visitRoute('subscribers.index');

        $this->click(__('subscriber.create'));
        $this->seeRouteIs('subscribers.create');

        $this->submitForm(__('subscriber.create'), $this->getCreateFields());

        $this->seeRouteIs('subscribers.show', Subscriber::first());

        $this->seeInDatabase('subscribers', $this->getCreateFields());
    }

    /** @test */
    public function validate_subscriber_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('subscribers.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_subscriber_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('subscribers.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_subscriber_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('subscribers.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Subscriber 1 name',
            'description' => 'Subscriber 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_subscriber()
    {
        $this->loginAsUser();
        $subscriber = factory(Subscriber::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('subscribers.show', $subscriber);
        $this->click('edit-subscriber-'.$subscriber->id);
        $this->seeRouteIs('subscribers.edit', $subscriber);

        $this->submitForm(__('subscriber.update'), $this->getEditFields());

        $this->seeRouteIs('subscribers.show', $subscriber);

        $this->seeInDatabase('subscribers', $this->getEditFields([
            'id' => $subscriber->id,
        ]));
    }

    /** @test */
    public function validate_subscriber_name_update_is_required()
    {
        $this->loginAsUser();
        $subscriber = factory(Subscriber::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('subscribers.update', $subscriber), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_subscriber_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $subscriber = factory(Subscriber::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('subscribers.update', $subscriber), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_subscriber_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $subscriber = factory(Subscriber::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('subscribers.update', $subscriber), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_subscriber()
    {
        $this->loginAsUser();
        $subscriber = factory(Subscriber::class)->create();
        factory(Subscriber::class)->create();

        $this->visitRoute('subscribers.edit', $subscriber);
        $this->click('del-subscriber-'.$subscriber->id);
        $this->seeRouteIs('subscribers.edit', [$subscriber, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('subscribers', [
            'id' => $subscriber->id,
        ]);
    }
}
