<?php

namespace Tests\Feature;

use App\PriceRange;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManagePriceRangeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_price_range_list_in_price_range_index_page()
    {
        $priceRange = factory(PriceRange::class)->create();

        $this->loginAsUser();
        $this->visitRoute('price_ranges.index');
        $this->see($priceRange->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'PriceRange 1 name',
            'description' => 'PriceRange 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_price_range()
    {
        $this->loginAsUser();
        $this->visitRoute('price_ranges.index');

        $this->click(__('price_range.create'));
        $this->seeRouteIs('price_ranges.create');

        $this->submitForm(__('price_range.create'), $this->getCreateFields());

        $this->seeRouteIs('price_ranges.show', PriceRange::first());

        $this->seeInDatabase('price_ranges', $this->getCreateFields());
    }

    /** @test */
    public function validate_price_range_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('price_ranges.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_price_range_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('price_ranges.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_price_range_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('price_ranges.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'PriceRange 1 name',
            'description' => 'PriceRange 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_price_range()
    {
        $this->loginAsUser();
        $priceRange = factory(PriceRange::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('price_ranges.show', $priceRange);
        $this->click('edit-price_range-'.$priceRange->id);
        $this->seeRouteIs('price_ranges.edit', $priceRange);

        $this->submitForm(__('price_range.update'), $this->getEditFields());

        $this->seeRouteIs('price_ranges.show', $priceRange);

        $this->seeInDatabase('price_ranges', $this->getEditFields([
            'id' => $priceRange->id,
        ]));
    }

    /** @test */
    public function validate_price_range_name_update_is_required()
    {
        $this->loginAsUser();
        $price_range = factory(PriceRange::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('price_ranges.update', $price_range), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_price_range_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $price_range = factory(PriceRange::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('price_ranges.update', $price_range), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_price_range_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $price_range = factory(PriceRange::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('price_ranges.update', $price_range), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_price_range()
    {
        $this->loginAsUser();
        $priceRange = factory(PriceRange::class)->create();
        factory(PriceRange::class)->create();

        $this->visitRoute('price_ranges.edit', $priceRange);
        $this->click('del-price_range-'.$priceRange->id);
        $this->seeRouteIs('price_ranges.edit', [$priceRange, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('price_ranges', [
            'id' => $priceRange->id,
        ]);
    }
}
