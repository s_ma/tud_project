<?php

namespace Tests\Feature;

use App\HotelReviewType;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHotelReviewTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_hotel_review_type_list_in_hotel_review_type_index_page()
    {
        $hotelReviewType = factory(HotelReviewType::class)->create();

        $this->loginAsUser();
        $this->visitRoute('hotel_review_types.index');
        $this->see($hotelReviewType->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelReviewType 1 name',
            'description' => 'HotelReviewType 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_hotel_review_type()
    {
        $this->loginAsUser();
        $this->visitRoute('hotel_review_types.index');

        $this->click(__('hotel_review_type.create'));
        $this->seeRouteIs('hotel_review_types.create');

        $this->submitForm(__('hotel_review_type.create'), $this->getCreateFields());

        $this->seeRouteIs('hotel_review_types.show', HotelReviewType::first());

        $this->seeInDatabase('hotel_review_types', $this->getCreateFields());
    }

    /** @test */
    public function validate_hotel_review_type_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('hotel_review_types.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_type_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('hotel_review_types.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_type_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('hotel_review_types.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelReviewType 1 name',
            'description' => 'HotelReviewType 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_hotel_review_type()
    {
        $this->loginAsUser();
        $hotelReviewType = factory(HotelReviewType::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('hotel_review_types.show', $hotelReviewType);
        $this->click('edit-hotel_review_type-'.$hotelReviewType->id);
        $this->seeRouteIs('hotel_review_types.edit', $hotelReviewType);

        $this->submitForm(__('hotel_review_type.update'), $this->getEditFields());

        $this->seeRouteIs('hotel_review_types.show', $hotelReviewType);

        $this->seeInDatabase('hotel_review_types', $this->getEditFields([
            'id' => $hotelReviewType->id,
        ]));
    }

    /** @test */
    public function validate_hotel_review_type_name_update_is_required()
    {
        $this->loginAsUser();
        $hotel_review_type = factory(HotelReviewType::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('hotel_review_types.update', $hotel_review_type), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_type_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $hotel_review_type = factory(HotelReviewType::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('hotel_review_types.update', $hotel_review_type), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_type_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $hotel_review_type = factory(HotelReviewType::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('hotel_review_types.update', $hotel_review_type), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_hotel_review_type()
    {
        $this->loginAsUser();
        $hotelReviewType = factory(HotelReviewType::class)->create();
        factory(HotelReviewType::class)->create();

        $this->visitRoute('hotel_review_types.edit', $hotelReviewType);
        $this->click('del-hotel_review_type-'.$hotelReviewType->id);
        $this->seeRouteIs('hotel_review_types.edit', [$hotelReviewType, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('hotel_review_types', [
            'id' => $hotelReviewType->id,
        ]);
    }
}
