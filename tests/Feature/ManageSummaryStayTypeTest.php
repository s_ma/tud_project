<?php

namespace Tests\Feature;

use App\SummaryStayType;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageSummaryStayTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_summary_stay_type_list_in_summary_stay_type_index_page()
    {
        $summaryStayType = factory(SummaryStayType::class)->create();

        $this->loginAsUser();
        $this->visitRoute('summary_stay_types.index');
        $this->see($summaryStayType->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'SummaryStayType 1 name',
            'description' => 'SummaryStayType 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_summary_stay_type()
    {
        $this->loginAsUser();
        $this->visitRoute('summary_stay_types.index');

        $this->click(__('summary_stay_type.create'));
        $this->seeRouteIs('summary_stay_types.create');

        $this->submitForm(__('summary_stay_type.create'), $this->getCreateFields());

        $this->seeRouteIs('summary_stay_types.show', SummaryStayType::first());

        $this->seeInDatabase('summary_stay_types', $this->getCreateFields());
    }

    /** @test */
    public function validate_summary_stay_type_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('summary_stay_types.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_summary_stay_type_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('summary_stay_types.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_summary_stay_type_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('summary_stay_types.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'SummaryStayType 1 name',
            'description' => 'SummaryStayType 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_summary_stay_type()
    {
        $this->loginAsUser();
        $summaryStayType = factory(SummaryStayType::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('summary_stay_types.show', $summaryStayType);
        $this->click('edit-summary_stay_type-'.$summaryStayType->id);
        $this->seeRouteIs('summary_stay_types.edit', $summaryStayType);

        $this->submitForm(__('summary_stay_type.update'), $this->getEditFields());

        $this->seeRouteIs('summary_stay_types.show', $summaryStayType);

        $this->seeInDatabase('summary_stay_types', $this->getEditFields([
            'id' => $summaryStayType->id,
        ]));
    }

    /** @test */
    public function validate_summary_stay_type_name_update_is_required()
    {
        $this->loginAsUser();
        $summary_stay_type = factory(SummaryStayType::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('summary_stay_types.update', $summary_stay_type), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_summary_stay_type_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $summary_stay_type = factory(SummaryStayType::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('summary_stay_types.update', $summary_stay_type), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_summary_stay_type_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $summary_stay_type = factory(SummaryStayType::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('summary_stay_types.update', $summary_stay_type), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_summary_stay_type()
    {
        $this->loginAsUser();
        $summaryStayType = factory(SummaryStayType::class)->create();
        factory(SummaryStayType::class)->create();

        $this->visitRoute('summary_stay_types.edit', $summaryStayType);
        $this->click('del-summary_stay_type-'.$summaryStayType->id);
        $this->seeRouteIs('summary_stay_types.edit', [$summaryStayType, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('summary_stay_types', [
            'id' => $summaryStayType->id,
        ]);
    }
}
