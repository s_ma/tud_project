<?php

namespace Tests\Feature;

use App\ReviewUsefulness;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageReviewUsefulnessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_review_usefulness_list_in_review_usefulness_index_page()
    {
        $reviewUsefulness = factory(ReviewUsefulness::class)->create();

        $this->loginAsUser();
        $this->visitRoute('review_usefulnesses.index');
        $this->see($reviewUsefulness->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'ReviewUsefulness 1 name',
            'description' => 'ReviewUsefulness 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_review_usefulness()
    {
        $this->loginAsUser();
        $this->visitRoute('review_usefulnesses.index');

        $this->click(__('review_usefulness.create'));
        $this->seeRouteIs('review_usefulnesses.create');

        $this->submitForm(__('review_usefulness.create'), $this->getCreateFields());

        $this->seeRouteIs('review_usefulnesses.show', ReviewUsefulness::first());

        $this->seeInDatabase('review_usefulnesses', $this->getCreateFields());
    }

    /** @test */
    public function validate_review_usefulness_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('review_usefulnesses.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_usefulness_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('review_usefulnesses.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_usefulness_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('review_usefulnesses.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'ReviewUsefulness 1 name',
            'description' => 'ReviewUsefulness 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_review_usefulness()
    {
        $this->loginAsUser();
        $reviewUsefulness = factory(ReviewUsefulness::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('review_usefulnesses.show', $reviewUsefulness);
        $this->click('edit-review_usefulness-'.$reviewUsefulness->id);
        $this->seeRouteIs('review_usefulnesses.edit', $reviewUsefulness);

        $this->submitForm(__('review_usefulness.update'), $this->getEditFields());

        $this->seeRouteIs('review_usefulnesses.show', $reviewUsefulness);

        $this->seeInDatabase('review_usefulnesses', $this->getEditFields([
            'id' => $reviewUsefulness->id,
        ]));
    }

    /** @test */
    public function validate_review_usefulness_name_update_is_required()
    {
        $this->loginAsUser();
        $review_usefulness = factory(ReviewUsefulness::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('review_usefulnesses.update', $review_usefulness), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_usefulness_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $review_usefulness = factory(ReviewUsefulness::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('review_usefulnesses.update', $review_usefulness), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_review_usefulness_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $review_usefulness = factory(ReviewUsefulness::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('review_usefulnesses.update', $review_usefulness), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_review_usefulness()
    {
        $this->loginAsUser();
        $reviewUsefulness = factory(ReviewUsefulness::class)->create();
        factory(ReviewUsefulness::class)->create();

        $this->visitRoute('review_usefulnesses.edit', $reviewUsefulness);
        $this->click('del-review_usefulness-'.$reviewUsefulness->id);
        $this->seeRouteIs('review_usefulnesses.edit', [$reviewUsefulness, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('review_usefulnesses', [
            'id' => $reviewUsefulness->id,
        ]);
    }
}
