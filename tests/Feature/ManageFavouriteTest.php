<?php

namespace Tests\Feature;

use App\Favourite;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageFavouriteTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_favourite_list_in_favourite_index_page()
    {
        $favourite = factory(Favourite::class)->create();

        $this->loginAsUser();
        $this->visitRoute('favourites.index');
        $this->see($favourite->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Favourite 1 name',
            'description' => 'Favourite 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_favourite()
    {
        $this->loginAsUser();
        $this->visitRoute('favourites.index');

        $this->click(__('favourite.create'));
        $this->seeRouteIs('favourites.create');

        $this->submitForm(__('favourite.create'), $this->getCreateFields());

        $this->seeRouteIs('favourites.show', Favourite::first());

        $this->seeInDatabase('favourites', $this->getCreateFields());
    }

    /** @test */
    public function validate_favourite_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('favourites.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_favourite_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('favourites.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_favourite_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('favourites.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Favourite 1 name',
            'description' => 'Favourite 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_favourite()
    {
        $this->loginAsUser();
        $favourite = factory(Favourite::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('favourites.show', $favourite);
        $this->click('edit-favourite-'.$favourite->id);
        $this->seeRouteIs('favourites.edit', $favourite);

        $this->submitForm(__('favourite.update'), $this->getEditFields());

        $this->seeRouteIs('favourites.show', $favourite);

        $this->seeInDatabase('favourites', $this->getEditFields([
            'id' => $favourite->id,
        ]));
    }

    /** @test */
    public function validate_favourite_name_update_is_required()
    {
        $this->loginAsUser();
        $favourite = factory(Favourite::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('favourites.update', $favourite), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_favourite_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $favourite = factory(Favourite::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('favourites.update', $favourite), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_favourite_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $favourite = factory(Favourite::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('favourites.update', $favourite), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_favourite()
    {
        $this->loginAsUser();
        $favourite = factory(Favourite::class)->create();
        factory(Favourite::class)->create();

        $this->visitRoute('favourites.edit', $favourite);
        $this->click('del-favourite-'.$favourite->id);
        $this->seeRouteIs('favourites.edit', [$favourite, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('favourites', [
            'id' => $favourite->id,
        ]);
    }
}
