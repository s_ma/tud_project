<?php

namespace Tests\Feature;

use App\Service;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_service_list_in_service_index_page()
    {
        $service = factory(Service::class)->create();

        $this->loginAsUser();
        $this->visitRoute('services.index');
        $this->see($service->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Service 1 name',
            'description' => 'Service 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_service()
    {
        $this->loginAsUser();
        $this->visitRoute('services.index');

        $this->click(__('service.create'));
        $this->seeRouteIs('services.create');

        $this->submitForm(__('service.create'), $this->getCreateFields());

        $this->seeRouteIs('services.show', Service::first());

        $this->seeInDatabase('services', $this->getCreateFields());
    }

    /** @test */
    public function validate_service_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('services.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_service_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('services.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_service_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('services.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Service 1 name',
            'description' => 'Service 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_service()
    {
        $this->loginAsUser();
        $service = factory(Service::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('services.show', $service);
        $this->click('edit-service-'.$service->id);
        $this->seeRouteIs('services.edit', $service);

        $this->submitForm(__('service.update'), $this->getEditFields());

        $this->seeRouteIs('services.show', $service);

        $this->seeInDatabase('services', $this->getEditFields([
            'id' => $service->id,
        ]));
    }

    /** @test */
    public function validate_service_name_update_is_required()
    {
        $this->loginAsUser();
        $service = factory(Service::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('services.update', $service), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_service_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $service = factory(Service::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('services.update', $service), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_service_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $service = factory(Service::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('services.update', $service), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_service()
    {
        $this->loginAsUser();
        $service = factory(Service::class)->create();
        factory(Service::class)->create();

        $this->visitRoute('services.edit', $service);
        $this->click('del-service-'.$service->id);
        $this->seeRouteIs('services.edit', [$service, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('services', [
            'id' => $service->id,
        ]);
    }
}
