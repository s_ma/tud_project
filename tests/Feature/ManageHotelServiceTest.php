<?php

namespace Tests\Feature;

use App\HotelService;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHotelServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_hotel_service_list_in_hotel_service_index_page()
    {
        $hotelService = factory(HotelService::class)->create();

        $this->loginAsUser();
        $this->visitRoute('hotel_services.index');
        $this->see($hotelService->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelService 1 name',
            'description' => 'HotelService 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_hotel_service()
    {
        $this->loginAsUser();
        $this->visitRoute('hotel_services.index');

        $this->click(__('hotel_service.create'));
        $this->seeRouteIs('hotel_services.create');

        $this->submitForm(__('hotel_service.create'), $this->getCreateFields());

        $this->seeRouteIs('hotel_services.show', HotelService::first());

        $this->seeInDatabase('hotel_services', $this->getCreateFields());
    }

    /** @test */
    public function validate_hotel_service_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('hotel_services.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_service_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('hotel_services.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_service_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('hotel_services.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelService 1 name',
            'description' => 'HotelService 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_hotel_service()
    {
        $this->loginAsUser();
        $hotelService = factory(HotelService::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('hotel_services.show', $hotelService);
        $this->click('edit-hotel_service-'.$hotelService->id);
        $this->seeRouteIs('hotel_services.edit', $hotelService);

        $this->submitForm(__('hotel_service.update'), $this->getEditFields());

        $this->seeRouteIs('hotel_services.show', $hotelService);

        $this->seeInDatabase('hotel_services', $this->getEditFields([
            'id' => $hotelService->id,
        ]));
    }

    /** @test */
    public function validate_hotel_service_name_update_is_required()
    {
        $this->loginAsUser();
        $hotel_service = factory(HotelService::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('hotel_services.update', $hotel_service), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_service_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $hotel_service = factory(HotelService::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('hotel_services.update', $hotel_service), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_service_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $hotel_service = factory(HotelService::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('hotel_services.update', $hotel_service), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_hotel_service()
    {
        $this->loginAsUser();
        $hotelService = factory(HotelService::class)->create();
        factory(HotelService::class)->create();

        $this->visitRoute('hotel_services.edit', $hotelService);
        $this->click('del-hotel_service-'.$hotelService->id);
        $this->seeRouteIs('hotel_services.edit', [$hotelService, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('hotel_services', [
            'id' => $hotelService->id,
        ]);
    }
}
