<?php

namespace Tests\Feature;

use App\Hotelier;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHotelierTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_hotelier_list_in_hotelier_index_page()
    {
        $hotelier = factory(Hotelier::class)->create();

        $this->loginAsUser();
        $this->visitRoute('hoteliers.index');
        $this->see($hotelier->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Hotelier 1 name',
            'description' => 'Hotelier 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_hotelier()
    {
        $this->loginAsUser();
        $this->visitRoute('hoteliers.index');

        $this->click(__('hotelier.create'));
        $this->seeRouteIs('hoteliers.create');

        $this->submitForm(__('hotelier.create'), $this->getCreateFields());

        $this->seeRouteIs('hoteliers.show', Hotelier::first());

        $this->seeInDatabase('hoteliers', $this->getCreateFields());
    }

    /** @test */
    public function validate_hotelier_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('hoteliers.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotelier_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('hoteliers.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotelier_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('hoteliers.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Hotelier 1 name',
            'description' => 'Hotelier 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_hotelier()
    {
        $this->loginAsUser();
        $hotelier = factory(Hotelier::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('hoteliers.show', $hotelier);
        $this->click('edit-hotelier-'.$hotelier->id);
        $this->seeRouteIs('hoteliers.edit', $hotelier);

        $this->submitForm(__('hotelier.update'), $this->getEditFields());

        $this->seeRouteIs('hoteliers.show', $hotelier);

        $this->seeInDatabase('hoteliers', $this->getEditFields([
            'id' => $hotelier->id,
        ]));
    }

    /** @test */
    public function validate_hotelier_name_update_is_required()
    {
        $this->loginAsUser();
        $hotelier = factory(Hotelier::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('hoteliers.update', $hotelier), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotelier_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $hotelier = factory(Hotelier::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('hoteliers.update', $hotelier), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotelier_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $hotelier = factory(Hotelier::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('hoteliers.update', $hotelier), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_hotelier()
    {
        $this->loginAsUser();
        $hotelier = factory(Hotelier::class)->create();
        factory(Hotelier::class)->create();

        $this->visitRoute('hoteliers.edit', $hotelier);
        $this->click('del-hotelier-'.$hotelier->id);
        $this->seeRouteIs('hoteliers.edit', [$hotelier, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('hoteliers', [
            'id' => $hotelier->id,
        ]);
    }
}
