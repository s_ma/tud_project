<?php

namespace Tests\Feature;

use App\VisitType;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageVisitTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_visit_type_list_in_visit_type_index_page()
    {
        $visitType = factory(VisitType::class)->create();

        $this->loginAsUser();
        $this->visitRoute('visit_types.index');
        $this->see($visitType->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'VisitType 1 name',
            'description' => 'VisitType 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_visit_type()
    {
        $this->loginAsUser();
        $this->visitRoute('visit_types.index');

        $this->click(__('visit_type.create'));
        $this->seeRouteIs('visit_types.create');

        $this->submitForm(__('visit_type.create'), $this->getCreateFields());

        $this->seeRouteIs('visit_types.show', VisitType::first());

        $this->seeInDatabase('visit_types', $this->getCreateFields());
    }

    /** @test */
    public function validate_visit_type_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('visit_types.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_type_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('visit_types.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_type_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('visit_types.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'VisitType 1 name',
            'description' => 'VisitType 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_visit_type()
    {
        $this->loginAsUser();
        $visitType = factory(VisitType::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('visit_types.show', $visitType);
        $this->click('edit-visit_type-'.$visitType->id);
        $this->seeRouteIs('visit_types.edit', $visitType);

        $this->submitForm(__('visit_type.update'), $this->getEditFields());

        $this->seeRouteIs('visit_types.show', $visitType);

        $this->seeInDatabase('visit_types', $this->getEditFields([
            'id' => $visitType->id,
        ]));
    }

    /** @test */
    public function validate_visit_type_name_update_is_required()
    {
        $this->loginAsUser();
        $visit_type = factory(VisitType::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('visit_types.update', $visit_type), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_type_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $visit_type = factory(VisitType::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('visit_types.update', $visit_type), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_type_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $visit_type = factory(VisitType::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('visit_types.update', $visit_type), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_visit_type()
    {
        $this->loginAsUser();
        $visitType = factory(VisitType::class)->create();
        factory(VisitType::class)->create();

        $this->visitRoute('visit_types.edit', $visitType);
        $this->click('del-visit_type-'.$visitType->id);
        $this->seeRouteIs('visit_types.edit', [$visitType, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('visit_types', [
            'id' => $visitType->id,
        ]);
    }
}
