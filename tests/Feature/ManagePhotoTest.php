<?php

namespace Tests\Feature;

use App\Photo;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManagePhotoTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_photo_list_in_photo_index_page()
    {
        $photo = factory(Photo::class)->create();

        $this->loginAsUser();
        $this->visitRoute('photos.index');
        $this->see($photo->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Photo 1 name',
            'description' => 'Photo 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_photo()
    {
        $this->loginAsUser();
        $this->visitRoute('photos.index');

        $this->click(__('photo.create'));
        $this->seeRouteIs('photos.create');

        $this->submitForm(__('photo.create'), $this->getCreateFields());

        $this->seeRouteIs('photos.show', Photo::first());

        $this->seeInDatabase('photos', $this->getCreateFields());
    }

    /** @test */
    public function validate_photo_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('photos.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_photo_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('photos.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_photo_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('photos.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Photo 1 name',
            'description' => 'Photo 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_photo()
    {
        $this->loginAsUser();
        $photo = factory(Photo::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('photos.show', $photo);
        $this->click('edit-photo-'.$photo->id);
        $this->seeRouteIs('photos.edit', $photo);

        $this->submitForm(__('photo.update'), $this->getEditFields());

        $this->seeRouteIs('photos.show', $photo);

        $this->seeInDatabase('photos', $this->getEditFields([
            'id' => $photo->id,
        ]));
    }

    /** @test */
    public function validate_photo_name_update_is_required()
    {
        $this->loginAsUser();
        $photo = factory(Photo::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('photos.update', $photo), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_photo_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $photo = factory(Photo::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('photos.update', $photo), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_photo_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $photo = factory(Photo::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('photos.update', $photo), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_photo()
    {
        $this->loginAsUser();
        $photo = factory(Photo::class)->create();
        factory(Photo::class)->create();

        $this->visitRoute('photos.edit', $photo);
        $this->click('del-photo-'.$photo->id);
        $this->seeRouteIs('photos.edit', [$photo, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('photos', [
            'id' => $photo->id,
        ]);
    }
}
