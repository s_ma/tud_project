<?php

namespace Tests\Feature;

use App\VisitDate;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageVisitDateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_visit_date_list_in_visit_date_index_page()
    {
        $visitDate = factory(VisitDate::class)->create();

        $this->loginAsUser();
        $this->visitRoute('visit_dates.index');
        $this->see($visitDate->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'VisitDate 1 name',
            'description' => 'VisitDate 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_visit_date()
    {
        $this->loginAsUser();
        $this->visitRoute('visit_dates.index');

        $this->click(__('visit_date.create'));
        $this->seeRouteIs('visit_dates.create');

        $this->submitForm(__('visit_date.create'), $this->getCreateFields());

        $this->seeRouteIs('visit_dates.show', VisitDate::first());

        $this->seeInDatabase('visit_dates', $this->getCreateFields());
    }

    /** @test */
    public function validate_visit_date_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('visit_dates.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_date_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('visit_dates.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_date_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('visit_dates.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'VisitDate 1 name',
            'description' => 'VisitDate 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_visit_date()
    {
        $this->loginAsUser();
        $visitDate = factory(VisitDate::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('visit_dates.show', $visitDate);
        $this->click('edit-visit_date-'.$visitDate->id);
        $this->seeRouteIs('visit_dates.edit', $visitDate);

        $this->submitForm(__('visit_date.update'), $this->getEditFields());

        $this->seeRouteIs('visit_dates.show', $visitDate);

        $this->seeInDatabase('visit_dates', $this->getEditFields([
            'id' => $visitDate->id,
        ]));
    }

    /** @test */
    public function validate_visit_date_name_update_is_required()
    {
        $this->loginAsUser();
        $visit_date = factory(VisitDate::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('visit_dates.update', $visit_date), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_date_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $visit_date = factory(VisitDate::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('visit_dates.update', $visit_date), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_visit_date_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $visit_date = factory(VisitDate::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('visit_dates.update', $visit_date), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_visit_date()
    {
        $this->loginAsUser();
        $visitDate = factory(VisitDate::class)->create();
        factory(VisitDate::class)->create();

        $this->visitRoute('visit_dates.edit', $visitDate);
        $this->click('del-visit_date-'.$visitDate->id);
        $this->seeRouteIs('visit_dates.edit', [$visitDate, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('visit_dates', [
            'id' => $visitDate->id,
        ]);
    }
}
