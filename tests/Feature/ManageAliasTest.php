<?php

namespace Tests\Feature;

use App\Alias;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageAliasTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_alias_list_in_alias_index_page()
    {
        $alias = factory(Alias::class)->create();

        $this->loginAsUser();
        $this->visitRoute('aliases.index');
        $this->see($alias->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Alias 1 name',
            'description' => 'Alias 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_alias()
    {
        $this->loginAsUser();
        $this->visitRoute('aliases.index');

        $this->click(__('alias.create'));
        $this->seeRouteIs('aliases.create');

        $this->submitForm(__('alias.create'), $this->getCreateFields());

        $this->seeRouteIs('aliases.show', Alias::first());

        $this->seeInDatabase('aliases', $this->getCreateFields());
    }

    /** @test */
    public function validate_alias_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('aliases.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_alias_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('aliases.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_alias_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('aliases.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Alias 1 name',
            'description' => 'Alias 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_alias()
    {
        $this->loginAsUser();
        $alias = factory(Alias::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('aliases.show', $alias);
        $this->click('edit-alias-'.$alias->id);
        $this->seeRouteIs('aliases.edit', $alias);

        $this->submitForm(__('alias.update'), $this->getEditFields());

        $this->seeRouteIs('aliases.show', $alias);

        $this->seeInDatabase('aliases', $this->getEditFields([
            'id' => $alias->id,
        ]));
    }

    /** @test */
    public function validate_alias_name_update_is_required()
    {
        $this->loginAsUser();
        $alias = factory(Alias::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('aliases.update', $alias), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_alias_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $alias = factory(Alias::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('aliases.update', $alias), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_alias_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $alias = factory(Alias::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('aliases.update', $alias), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_alias()
    {
        $this->loginAsUser();
        $alias = factory(Alias::class)->create();
        factory(Alias::class)->create();

        $this->visitRoute('aliases.edit', $alias);
        $this->click('del-alias-'.$alias->id);
        $this->seeRouteIs('aliases.edit', [$alias, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('aliases', [
            'id' => $alias->id,
        ]);
    }
}
