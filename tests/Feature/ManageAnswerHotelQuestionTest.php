<?php

namespace Tests\Feature;

use App\AnswerHotelQuestion;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageAnswerHotelQuestionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_answer_hotel_question_list_in_answer_hotel_question_index_page()
    {
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create();

        $this->loginAsUser();
        $this->visitRoute('answer_hotel_questions.index');
        $this->see($answerHotelQuestion->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'AnswerHotelQuestion 1 name',
            'description' => 'AnswerHotelQuestion 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_answer_hotel_question()
    {
        $this->loginAsUser();
        $this->visitRoute('answer_hotel_questions.index');

        $this->click(__('answer_hotel_question.create'));
        $this->seeRouteIs('answer_hotel_questions.create');

        $this->submitForm(__('answer_hotel_question.create'), $this->getCreateFields());

        $this->seeRouteIs('answer_hotel_questions.show', AnswerHotelQuestion::first());

        $this->seeInDatabase('answer_hotel_questions', $this->getCreateFields());
    }

    /** @test */
    public function validate_answer_hotel_question_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('answer_hotel_questions.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_answer_hotel_question_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('answer_hotel_questions.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_answer_hotel_question_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('answer_hotel_questions.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'AnswerHotelQuestion 1 name',
            'description' => 'AnswerHotelQuestion 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_answer_hotel_question()
    {
        $this->loginAsUser();
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('answer_hotel_questions.show', $answerHotelQuestion);
        $this->click('edit-answer_hotel_question-'.$answerHotelQuestion->id);
        $this->seeRouteIs('answer_hotel_questions.edit', $answerHotelQuestion);

        $this->submitForm(__('answer_hotel_question.update'), $this->getEditFields());

        $this->seeRouteIs('answer_hotel_questions.show', $answerHotelQuestion);

        $this->seeInDatabase('answer_hotel_questions', $this->getEditFields([
            'id' => $answerHotelQuestion->id,
        ]));
    }

    /** @test */
    public function validate_answer_hotel_question_name_update_is_required()
    {
        $this->loginAsUser();
        $answer_hotel_question = factory(AnswerHotelQuestion::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('answer_hotel_questions.update', $answer_hotel_question), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_answer_hotel_question_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $answer_hotel_question = factory(AnswerHotelQuestion::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('answer_hotel_questions.update', $answer_hotel_question), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_answer_hotel_question_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $answer_hotel_question = factory(AnswerHotelQuestion::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('answer_hotel_questions.update', $answer_hotel_question), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_answer_hotel_question()
    {
        $this->loginAsUser();
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create();
        factory(AnswerHotelQuestion::class)->create();

        $this->visitRoute('answer_hotel_questions.edit', $answerHotelQuestion);
        $this->click('del-answer_hotel_question-'.$answerHotelQuestion->id);
        $this->seeRouteIs('answer_hotel_questions.edit', [$answerHotelQuestion, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('answer_hotel_questions', [
            'id' => $answerHotelQuestion->id,
        ]);
    }
}
