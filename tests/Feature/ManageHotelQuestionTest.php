<?php

namespace Tests\Feature;

use App\HotelQuestion;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHotelQuestionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_hotel_question_list_in_hotel_question_index_page()
    {
        $hotelQuestion = factory(HotelQuestion::class)->create();

        $this->loginAsUser();
        $this->visitRoute('hotel_questions.index');
        $this->see($hotelQuestion->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelQuestion 1 name',
            'description' => 'HotelQuestion 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_hotel_question()
    {
        $this->loginAsUser();
        $this->visitRoute('hotel_questions.index');

        $this->click(__('hotel_question.create'));
        $this->seeRouteIs('hotel_questions.create');

        $this->submitForm(__('hotel_question.create'), $this->getCreateFields());

        $this->seeRouteIs('hotel_questions.show', HotelQuestion::first());

        $this->seeInDatabase('hotel_questions', $this->getCreateFields());
    }

    /** @test */
    public function validate_hotel_question_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('hotel_questions.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_question_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('hotel_questions.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_question_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('hotel_questions.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelQuestion 1 name',
            'description' => 'HotelQuestion 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_hotel_question()
    {
        $this->loginAsUser();
        $hotelQuestion = factory(HotelQuestion::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('hotel_questions.show', $hotelQuestion);
        $this->click('edit-hotel_question-'.$hotelQuestion->id);
        $this->seeRouteIs('hotel_questions.edit', $hotelQuestion);

        $this->submitForm(__('hotel_question.update'), $this->getEditFields());

        $this->seeRouteIs('hotel_questions.show', $hotelQuestion);

        $this->seeInDatabase('hotel_questions', $this->getEditFields([
            'id' => $hotelQuestion->id,
        ]));
    }

    /** @test */
    public function validate_hotel_question_name_update_is_required()
    {
        $this->loginAsUser();
        $hotel_question = factory(HotelQuestion::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('hotel_questions.update', $hotel_question), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_question_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $hotel_question = factory(HotelQuestion::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('hotel_questions.update', $hotel_question), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_question_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $hotel_question = factory(HotelQuestion::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('hotel_questions.update', $hotel_question), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_hotel_question()
    {
        $this->loginAsUser();
        $hotelQuestion = factory(HotelQuestion::class)->create();
        factory(HotelQuestion::class)->create();

        $this->visitRoute('hotel_questions.edit', $hotelQuestion);
        $this->click('del-hotel_question-'.$hotelQuestion->id);
        $this->seeRouteIs('hotel_questions.edit', [$hotelQuestion, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('hotel_questions', [
            'id' => $hotelQuestion->id,
        ]);
    }
}
