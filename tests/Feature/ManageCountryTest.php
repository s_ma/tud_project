<?php

namespace Tests\Feature;

use App\Country;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageCountryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_country_list_in_country_index_page()
    {
        $country = factory(Country::class)->create();

        $this->loginAsUser();
        $this->visitRoute('countries.index');
        $this->see($country->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Country 1 name',
            'description' => 'Country 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_country()
    {
        $this->loginAsUser();
        $this->visitRoute('countries.index');

        $this->click(__('country.create'));
        $this->seeRouteIs('countries.create');

        $this->submitForm(__('country.create'), $this->getCreateFields());

        $this->seeRouteIs('countries.show', Country::first());

        $this->seeInDatabase('countries', $this->getCreateFields());
    }

    /** @test */
    public function validate_country_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('countries.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_country_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('countries.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_country_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('countries.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Country 1 name',
            'description' => 'Country 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_country()
    {
        $this->loginAsUser();
        $country = factory(Country::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('countries.show', $country);
        $this->click('edit-country-'.$country->id);
        $this->seeRouteIs('countries.edit', $country);

        $this->submitForm(__('country.update'), $this->getEditFields());

        $this->seeRouteIs('countries.show', $country);

        $this->seeInDatabase('countries', $this->getEditFields([
            'id' => $country->id,
        ]));
    }

    /** @test */
    public function validate_country_name_update_is_required()
    {
        $this->loginAsUser();
        $country = factory(Country::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('countries.update', $country), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_country_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $country = factory(Country::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('countries.update', $country), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_country_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $country = factory(Country::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('countries.update', $country), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_country()
    {
        $this->loginAsUser();
        $country = factory(Country::class)->create();
        factory(Country::class)->create();

        $this->visitRoute('countries.edit', $country);
        $this->click('del-country-'.$country->id);
        $this->seeRouteIs('countries.edit', [$country, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('countries', [
            'id' => $country->id,
        ]);
    }
}
