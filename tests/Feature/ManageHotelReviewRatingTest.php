<?php

namespace Tests\Feature;

use App\HotelReviewRating;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHotelReviewRatingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_hotel_review_rating_list_in_hotel_review_rating_index_page()
    {
        $hotelReviewRating = factory(HotelReviewRating::class)->create();

        $this->loginAsUser();
        $this->visitRoute('hotel_review_ratings.index');
        $this->see($hotelReviewRating->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelReviewRating 1 name',
            'description' => 'HotelReviewRating 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_hotel_review_rating()
    {
        $this->loginAsUser();
        $this->visitRoute('hotel_review_ratings.index');

        $this->click(__('hotel_review_rating.create'));
        $this->seeRouteIs('hotel_review_ratings.create');

        $this->submitForm(__('hotel_review_rating.create'), $this->getCreateFields());

        $this->seeRouteIs('hotel_review_ratings.show', HotelReviewRating::first());

        $this->seeInDatabase('hotel_review_ratings', $this->getCreateFields());
    }

    /** @test */
    public function validate_hotel_review_rating_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('hotel_review_ratings.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_rating_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('hotel_review_ratings.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_rating_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('hotel_review_ratings.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'HotelReviewRating 1 name',
            'description' => 'HotelReviewRating 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_hotel_review_rating()
    {
        $this->loginAsUser();
        $hotelReviewRating = factory(HotelReviewRating::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('hotel_review_ratings.show', $hotelReviewRating);
        $this->click('edit-hotel_review_rating-'.$hotelReviewRating->id);
        $this->seeRouteIs('hotel_review_ratings.edit', $hotelReviewRating);

        $this->submitForm(__('hotel_review_rating.update'), $this->getEditFields());

        $this->seeRouteIs('hotel_review_ratings.show', $hotelReviewRating);

        $this->seeInDatabase('hotel_review_ratings', $this->getEditFields([
            'id' => $hotelReviewRating->id,
        ]));
    }

    /** @test */
    public function validate_hotel_review_rating_name_update_is_required()
    {
        $this->loginAsUser();
        $hotel_review_rating = factory(HotelReviewRating::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('hotel_review_ratings.update', $hotel_review_rating), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_rating_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $hotel_review_rating = factory(HotelReviewRating::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('hotel_review_ratings.update', $hotel_review_rating), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_review_rating_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $hotel_review_rating = factory(HotelReviewRating::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('hotel_review_ratings.update', $hotel_review_rating), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_hotel_review_rating()
    {
        $this->loginAsUser();
        $hotelReviewRating = factory(HotelReviewRating::class)->create();
        factory(HotelReviewRating::class)->create();

        $this->visitRoute('hotel_review_ratings.edit', $hotelReviewRating);
        $this->click('del-hotel_review_rating-'.$hotelReviewRating->id);
        $this->seeRouteIs('hotel_review_ratings.edit', [$hotelReviewRating, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('hotel_review_ratings', [
            'id' => $hotelReviewRating->id,
        ]);
    }
}
