<?php

namespace Tests\Feature;

use App\Hotel;
use Tests\BrowserKitTest as TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManageHotelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_see_hotel_list_in_hotel_index_page()
    {
        $hotel = factory(Hotel::class)->create();

        $this->loginAsUser();
        $this->visitRoute('hotels.index');
        $this->see($hotel->name);
    }

    private function getCreateFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Hotel 1 name',
            'description' => 'Hotel 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_create_a_hotel()
    {
        $this->loginAsUser();
        $this->visitRoute('hotels.index');

        $this->click(__('hotel.create'));
        $this->seeRouteIs('hotels.create');

        $this->submitForm(__('hotel.create'), $this->getCreateFields());

        $this->seeRouteIs('hotels.show', Hotel::first());

        $this->seeInDatabase('hotels', $this->getCreateFields());
    }

    /** @test */
    public function validate_hotel_name_is_required()
    {
        $this->loginAsUser();

        // name empty
        $this->post(route('hotels.store'), $this->getCreateFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_name_is_not_more_than_60_characters()
    {
        $this->loginAsUser();

        // name 70 characters
        $this->post(route('hotels.store'), $this->getCreateFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_description_is_not_more_than_255_characters()
    {
        $this->loginAsUser();

        // description 256 characters
        $this->post(route('hotels.store'), $this->getCreateFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    private function getEditFields(array $overrides = [])
    {
        return array_merge([
            'name'        => 'Hotel 1 name',
            'description' => 'Hotel 1 description',
        ], $overrides);
    }

    /** @test */
    public function user_can_edit_a_hotel()
    {
        $this->loginAsUser();
        $hotel = factory(Hotel::class)->create(['name' => 'Testing 123']);

        $this->visitRoute('hotels.show', $hotel);
        $this->click('edit-hotel-'.$hotel->id);
        $this->seeRouteIs('hotels.edit', $hotel);

        $this->submitForm(__('hotel.update'), $this->getEditFields());

        $this->seeRouteIs('hotels.show', $hotel);

        $this->seeInDatabase('hotels', $this->getEditFields([
            'id' => $hotel->id,
        ]));
    }

    /** @test */
    public function validate_hotel_name_update_is_required()
    {
        $this->loginAsUser();
        $hotel = factory(Hotel::class)->create(['name' => 'Testing 123']);

        // name empty
        $this->patch(route('hotels.update', $hotel), $this->getEditFields(['name' => '']));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_name_update_is_not_more_than_60_characters()
    {
        $this->loginAsUser();
        $hotel = factory(Hotel::class)->create(['name' => 'Testing 123']);

        // name 70 characters
        $this->patch(route('hotels.update', $hotel), $this->getEditFields([
            'name' => str_repeat('Test Title', 7),
        ]));
        $this->assertSessionHasErrors('name');
    }

    /** @test */
    public function validate_hotel_description_update_is_not_more_than_255_characters()
    {
        $this->loginAsUser();
        $hotel = factory(Hotel::class)->create(['name' => 'Testing 123']);

        // description 256 characters
        $this->patch(route('hotels.update', $hotel), $this->getEditFields([
            'description' => str_repeat('Long description', 16),
        ]));
        $this->assertSessionHasErrors('description');
    }

    /** @test */
    public function user_can_delete_a_hotel()
    {
        $this->loginAsUser();
        $hotel = factory(Hotel::class)->create();
        factory(Hotel::class)->create();

        $this->visitRoute('hotels.edit', $hotel);
        $this->click('del-hotel-'.$hotel->id);
        $this->seeRouteIs('hotels.edit', [$hotel, 'action' => 'delete']);

        $this->press(__('app.delete_confirm_button'));

        $this->dontSeeInDatabase('hotels', [
            'id' => $hotel->id,
        ]);
    }
}
