<?php

namespace Tests\Unit\Models;

use App\User;
use App\SimilarHotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class SimilarHotelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_similar_hotel_has_name_link_attribute()
    {
        $similarHotel = factory(SimilarHotel::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $similarHotel->name, 'type' => __('similar_hotel.similar_hotel'),
        ]);
        $link = '<a href="'.route('similar_hotels.show', $similarHotel).'"';
        $link .= ' title="'.$title.'">';
        $link .= $similarHotel->name;
        $link .= '</a>';

        $this->assertEquals($link, $similarHotel->name_link);
    }

    /** @test */
    public function a_similar_hotel_has_belongs_to_creator_relation()
    {
        $similarHotel = factory(SimilarHotel::class)->make();

        $this->assertInstanceOf(User::class, $similarHotel->creator);
        $this->assertEquals($similarHotel->creator_id, $similarHotel->creator->id);
    }
}
