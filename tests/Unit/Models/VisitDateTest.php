<?php

namespace Tests\Unit\Models;

use App\User;
use App\VisitDate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class VisitDateTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_visit_date_has_name_link_attribute()
    {
        $visitDate = factory(VisitDate::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $visitDate->name, 'type' => __('visit_date.visit_date'),
        ]);
        $link = '<a href="'.route('visit_dates.show', $visitDate).'"';
        $link .= ' title="'.$title.'">';
        $link .= $visitDate->name;
        $link .= '</a>';

        $this->assertEquals($link, $visitDate->name_link);
    }

    /** @test */
    public function a_visit_date_has_belongs_to_creator_relation()
    {
        $visitDate = factory(VisitDate::class)->make();

        $this->assertInstanceOf(User::class, $visitDate->creator);
        $this->assertEquals($visitDate->creator_id, $visitDate->creator->id);
    }
}
