<?php

namespace Tests\Unit\Models;

use App\User;
use App\Hotelier;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelierTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotelier_has_name_link_attribute()
    {
        $hotelier = factory(Hotelier::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotelier->name, 'type' => __('hotelier.hotelier'),
        ]);
        $link = '<a href="'.route('hoteliers.show', $hotelier).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotelier->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotelier->name_link);
    }

    /** @test */
    public function a_hotelier_has_belongs_to_creator_relation()
    {
        $hotelier = factory(Hotelier::class)->make();

        $this->assertInstanceOf(User::class, $hotelier->creator);
        $this->assertEquals($hotelier->creator_id, $hotelier->creator->id);
    }
}
