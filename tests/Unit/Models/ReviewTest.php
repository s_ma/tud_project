<?php

namespace Tests\Unit\Models;

use App\User;
use App\Review;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class ReviewTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_review_has_name_link_attribute()
    {
        $review = factory(Review::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $review->name, 'type' => __('review.review'),
        ]);
        $link = '<a href="'.route('reviews.show', $review).'"';
        $link .= ' title="'.$title.'">';
        $link .= $review->name;
        $link .= '</a>';

        $this->assertEquals($link, $review->name_link);
    }

    /** @test */
    public function a_review_has_belongs_to_creator_relation()
    {
        $review = factory(Review::class)->make();

        $this->assertInstanceOf(User::class, $review->creator);
        $this->assertEquals($review->creator_id, $review->creator->id);
    }
}
