<?php

namespace Tests\Unit\Models;

use App\User;
use App\HotelQuestion;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelQuestionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotel_question_has_name_link_attribute()
    {
        $hotelQuestion = factory(HotelQuestion::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotelQuestion->name, 'type' => __('hotel_question.hotel_question'),
        ]);
        $link = '<a href="'.route('hotel_questions.show', $hotelQuestion).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotelQuestion->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotelQuestion->name_link);
    }

    /** @test */
    public function a_hotel_question_has_belongs_to_creator_relation()
    {
        $hotelQuestion = factory(HotelQuestion::class)->make();

        $this->assertInstanceOf(User::class, $hotelQuestion->creator);
        $this->assertEquals($hotelQuestion->creator_id, $hotelQuestion->creator->id);
    }
}
