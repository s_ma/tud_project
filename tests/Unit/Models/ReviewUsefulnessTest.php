<?php

namespace Tests\Unit\Models;

use App\User;
use App\ReviewUsefulness;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class ReviewUsefulnessTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_review_usefulness_has_name_link_attribute()
    {
        $reviewUsefulness = factory(ReviewUsefulness::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $reviewUsefulness->name, 'type' => __('review_usefulness.review_usefulness'),
        ]);
        $link = '<a href="'.route('review_usefulnesses.show', $reviewUsefulness).'"';
        $link .= ' title="'.$title.'">';
        $link .= $reviewUsefulness->name;
        $link .= '</a>';

        $this->assertEquals($link, $reviewUsefulness->name_link);
    }

    /** @test */
    public function a_review_usefulness_has_belongs_to_creator_relation()
    {
        $reviewUsefulness = factory(ReviewUsefulness::class)->make();

        $this->assertInstanceOf(User::class, $reviewUsefulness->creator);
        $this->assertEquals($reviewUsefulness->creator_id, $reviewUsefulness->creator->id);
    }
}
