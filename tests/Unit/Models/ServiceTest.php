<?php

namespace Tests\Unit\Models;

use App\User;
use App\Service;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class ServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_service_has_name_link_attribute()
    {
        $service = factory(Service::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $service->name, 'type' => __('service.service'),
        ]);
        $link = '<a href="'.route('services.show', $service).'"';
        $link .= ' title="'.$title.'">';
        $link .= $service->name;
        $link .= '</a>';

        $this->assertEquals($link, $service->name_link);
    }

    /** @test */
    public function a_service_has_belongs_to_creator_relation()
    {
        $service = factory(Service::class)->make();

        $this->assertInstanceOf(User::class, $service->creator);
        $this->assertEquals($service->creator_id, $service->creator->id);
    }
}
