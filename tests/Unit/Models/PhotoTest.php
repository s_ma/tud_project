<?php

namespace Tests\Unit\Models;

use App\User;
use App\Photo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class PhotoTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_photo_has_name_link_attribute()
    {
        $photo = factory(Photo::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $photo->name, 'type' => __('photo.photo'),
        ]);
        $link = '<a href="'.route('photos.show', $photo).'"';
        $link .= ' title="'.$title.'">';
        $link .= $photo->name;
        $link .= '</a>';

        $this->assertEquals($link, $photo->name_link);
    }

    /** @test */
    public function a_photo_has_belongs_to_creator_relation()
    {
        $photo = factory(Photo::class)->make();

        $this->assertInstanceOf(User::class, $photo->creator);
        $this->assertEquals($photo->creator_id, $photo->creator->id);
    }
}
