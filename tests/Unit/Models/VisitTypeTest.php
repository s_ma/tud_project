<?php

namespace Tests\Unit\Models;

use App\User;
use App\VisitType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class VisitTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_visit_type_has_name_link_attribute()
    {
        $visitType = factory(VisitType::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $visitType->name, 'type' => __('visit_type.visit_type'),
        ]);
        $link = '<a href="'.route('visit_types.show', $visitType).'"';
        $link .= ' title="'.$title.'">';
        $link .= $visitType->name;
        $link .= '</a>';

        $this->assertEquals($link, $visitType->name_link);
    }

    /** @test */
    public function a_visit_type_has_belongs_to_creator_relation()
    {
        $visitType = factory(VisitType::class)->make();

        $this->assertInstanceOf(User::class, $visitType->creator);
        $this->assertEquals($visitType->creator_id, $visitType->creator->id);
    }
}
