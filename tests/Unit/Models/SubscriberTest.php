<?php

namespace Tests\Unit\Models;

use App\User;
use App\Subscriber;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class SubscriberTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_subscriber_has_name_link_attribute()
    {
        $subscriber = factory(Subscriber::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $subscriber->name, 'type' => __('subscriber.subscriber'),
        ]);
        $link = '<a href="'.route('subscribers.show', $subscriber).'"';
        $link .= ' title="'.$title.'">';
        $link .= $subscriber->name;
        $link .= '</a>';

        $this->assertEquals($link, $subscriber->name_link);
    }

    /** @test */
    public function a_subscriber_has_belongs_to_creator_relation()
    {
        $subscriber = factory(Subscriber::class)->make();

        $this->assertInstanceOf(User::class, $subscriber->creator);
        $this->assertEquals($subscriber->creator_id, $subscriber->creator->id);
    }
}
