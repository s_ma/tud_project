<?php

namespace Tests\Unit\Models;

use App\User;
use App\SummaryStayType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class SummaryStayTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_summary_stay_type_has_name_link_attribute()
    {
        $summaryStayType = factory(SummaryStayType::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $summaryStayType->name, 'type' => __('summary_stay_type.summary_stay_type'),
        ]);
        $link = '<a href="'.route('summary_stay_types.show', $summaryStayType).'"';
        $link .= ' title="'.$title.'">';
        $link .= $summaryStayType->name;
        $link .= '</a>';

        $this->assertEquals($link, $summaryStayType->name_link);
    }

    /** @test */
    public function a_summary_stay_type_has_belongs_to_creator_relation()
    {
        $summaryStayType = factory(SummaryStayType::class)->make();

        $this->assertInstanceOf(User::class, $summaryStayType->creator);
        $this->assertEquals($summaryStayType->creator_id, $summaryStayType->creator->id);
    }
}
