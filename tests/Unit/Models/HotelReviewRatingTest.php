<?php

namespace Tests\Unit\Models;

use App\User;
use App\HotelReviewRating;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelReviewRatingTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotel_review_rating_has_name_link_attribute()
    {
        $hotelReviewRating = factory(HotelReviewRating::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotelReviewRating->name, 'type' => __('hotel_review_rating.hotel_review_rating'),
        ]);
        $link = '<a href="'.route('hotel_review_ratings.show', $hotelReviewRating).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotelReviewRating->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotelReviewRating->name_link);
    }

    /** @test */
    public function a_hotel_review_rating_has_belongs_to_creator_relation()
    {
        $hotelReviewRating = factory(HotelReviewRating::class)->make();

        $this->assertInstanceOf(User::class, $hotelReviewRating->creator);
        $this->assertEquals($hotelReviewRating->creator_id, $hotelReviewRating->creator->id);
    }
}
