<?php

namespace Tests\Unit\Models;

use App\User;
use App\Favourite;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class FavouriteTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_favourite_has_name_link_attribute()
    {
        $favourite = factory(Favourite::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $favourite->name, 'type' => __('favourite.favourite'),
        ]);
        $link = '<a href="'.route('favourites.show', $favourite).'"';
        $link .= ' title="'.$title.'">';
        $link .= $favourite->name;
        $link .= '</a>';

        $this->assertEquals($link, $favourite->name_link);
    }

    /** @test */
    public function a_favourite_has_belongs_to_creator_relation()
    {
        $favourite = factory(Favourite::class)->make();

        $this->assertInstanceOf(User::class, $favourite->creator);
        $this->assertEquals($favourite->creator_id, $favourite->creator->id);
    }
}
