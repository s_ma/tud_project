<?php

namespace Tests\Unit\Models;

use App\User;
use App\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class CountryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_country_has_name_link_attribute()
    {
        $country = factory(Country::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $country->name, 'type' => __('country.country'),
        ]);
        $link = '<a href="'.route('countries.show', $country).'"';
        $link .= ' title="'.$title.'">';
        $link .= $country->name;
        $link .= '</a>';

        $this->assertEquals($link, $country->name_link);
    }

    /** @test */
    public function a_country_has_belongs_to_creator_relation()
    {
        $country = factory(Country::class)->make();

        $this->assertInstanceOf(User::class, $country->creator);
        $this->assertEquals($country->creator_id, $country->creator->id);
    }
}
