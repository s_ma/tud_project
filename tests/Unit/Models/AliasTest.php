<?php

namespace Tests\Unit\Models;

use App\User;
use App\Alias;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class AliasTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_alias_has_name_link_attribute()
    {
        $alias = factory(Alias::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $alias->name, 'type' => __('alias.alias'),
        ]);
        $link = '<a href="'.route('aliases.show', $alias).'"';
        $link .= ' title="'.$title.'">';
        $link .= $alias->name;
        $link .= '</a>';

        $this->assertEquals($link, $alias->name_link);
    }

    /** @test */
    public function a_alias_has_belongs_to_creator_relation()
    {
        $alias = factory(Alias::class)->make();

        $this->assertInstanceOf(User::class, $alias->creator);
        $this->assertEquals($alias->creator_id, $alias->creator->id);
    }
}
