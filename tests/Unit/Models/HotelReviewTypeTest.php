<?php

namespace Tests\Unit\Models;

use App\User;
use App\HotelReviewType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelReviewTypeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotel_review_type_has_name_link_attribute()
    {
        $hotelReviewType = factory(HotelReviewType::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotelReviewType->name, 'type' => __('hotel_review_type.hotel_review_type'),
        ]);
        $link = '<a href="'.route('hotel_review_types.show', $hotelReviewType).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotelReviewType->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotelReviewType->name_link);
    }

    /** @test */
    public function a_hotel_review_type_has_belongs_to_creator_relation()
    {
        $hotelReviewType = factory(HotelReviewType::class)->make();

        $this->assertInstanceOf(User::class, $hotelReviewType->creator);
        $this->assertEquals($hotelReviewType->creator_id, $hotelReviewType->creator->id);
    }
}
