<?php

namespace Tests\Unit\Models;

use App\User;
use App\HotelCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelCategoryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotel_category_has_name_link_attribute()
    {
        $hotelCategory = factory(HotelCategory::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotelCategory->name, 'type' => __('hotel_category.hotel_category'),
        ]);
        $link = '<a href="'.route('hotel_categories.show', $hotelCategory).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotelCategory->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotelCategory->name_link);
    }

    /** @test */
    public function a_hotel_category_has_belongs_to_creator_relation()
    {
        $hotelCategory = factory(HotelCategory::class)->make();

        $this->assertInstanceOf(User::class, $hotelCategory->creator);
        $this->assertEquals($hotelCategory->creator_id, $hotelCategory->creator->id);
    }
}
