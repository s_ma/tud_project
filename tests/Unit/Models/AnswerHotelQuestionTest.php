<?php

namespace Tests\Unit\Models;

use App\User;
use App\AnswerHotelQuestion;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class AnswerHotelQuestionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_answer_hotel_question_has_name_link_attribute()
    {
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $answerHotelQuestion->name, 'type' => __('answer_hotel_question.answer_hotel_question'),
        ]);
        $link = '<a href="'.route('answer_hotel_questions.show', $answerHotelQuestion).'"';
        $link .= ' title="'.$title.'">';
        $link .= $answerHotelQuestion->name;
        $link .= '</a>';

        $this->assertEquals($link, $answerHotelQuestion->name_link);
    }

    /** @test */
    public function a_answer_hotel_question_has_belongs_to_creator_relation()
    {
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->make();

        $this->assertInstanceOf(User::class, $answerHotelQuestion->creator);
        $this->assertEquals($answerHotelQuestion->creator_id, $answerHotelQuestion->creator->id);
    }
}
