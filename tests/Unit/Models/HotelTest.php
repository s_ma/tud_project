<?php

namespace Tests\Unit\Models;

use App\User;
use App\Hotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotel_has_name_link_attribute()
    {
        $hotel = factory(Hotel::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotel->name, 'type' => __('hotel.hotel'),
        ]);
        $link = '<a href="'.route('hotels.show', $hotel).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotel->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotel->name_link);
    }

    /** @test */
    public function a_hotel_has_belongs_to_creator_relation()
    {
        $hotel = factory(Hotel::class)->make();

        $this->assertInstanceOf(User::class, $hotel->creator);
        $this->assertEquals($hotel->creator_id, $hotel->creator->id);
    }
}
