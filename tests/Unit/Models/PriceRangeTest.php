<?php

namespace Tests\Unit\Models;

use App\User;
use App\PriceRange;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class PriceRangeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_price_range_has_name_link_attribute()
    {
        $priceRange = factory(PriceRange::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $priceRange->name, 'type' => __('price_range.price_range'),
        ]);
        $link = '<a href="'.route('price_ranges.show', $priceRange).'"';
        $link .= ' title="'.$title.'">';
        $link .= $priceRange->name;
        $link .= '</a>';

        $this->assertEquals($link, $priceRange->name_link);
    }

    /** @test */
    public function a_price_range_has_belongs_to_creator_relation()
    {
        $priceRange = factory(PriceRange::class)->make();

        $this->assertInstanceOf(User::class, $priceRange->creator);
        $this->assertEquals($priceRange->creator_id, $priceRange->creator->id);
    }
}
