<?php

namespace Tests\Unit\Models;

use App\User;
use App\HotelService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelServiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_hotel_service_has_name_link_attribute()
    {
        $hotelService = factory(HotelService::class)->create();

        $title = __('app.show_detail_title', [
            'name' => $hotelService->name, 'type' => __('hotel_service.hotel_service'),
        ]);
        $link = '<a href="'.route('hotel_services.show', $hotelService).'"';
        $link .= ' title="'.$title.'">';
        $link .= $hotelService->name;
        $link .= '</a>';

        $this->assertEquals($link, $hotelService->name_link);
    }

    /** @test */
    public function a_hotel_service_has_belongs_to_creator_relation()
    {
        $hotelService = factory(HotelService::class)->make();

        $this->assertInstanceOf(User::class, $hotelService->creator);
        $this->assertEquals($hotelService->creator_id, $hotelService->creator->id);
    }
}
