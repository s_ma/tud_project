<?php

namespace Tests\Unit\Policies;

use App\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class CountryPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_country()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Country));
    }

    /** @test */
    public function user_can_view_country()
    {
        $user = $this->createUser();
        $country = factory(Country::class)->create();
        $this->assertTrue($user->can('view', $country));
    }

    /** @test */
    public function user_can_update_country()
    {
        $user = $this->createUser();
        $country = factory(Country::class)->create();
        $this->assertTrue($user->can('update', $country));
    }

    /** @test */
    public function user_can_delete_country()
    {
        $user = $this->createUser();
        $country = factory(Country::class)->create();
        $this->assertTrue($user->can('delete', $country));
    }
}
