<?php

namespace Tests\Unit\Policies;

use App\AnswerHotelQuestion;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class AnswerHotelQuestionPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_answer_hotel_question()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new AnswerHotelQuestion));
    }

    /** @test */
    public function user_can_view_answer_hotel_question()
    {
        $user = $this->createUser();
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create();
        $this->assertTrue($user->can('view', $answerHotelQuestion));
    }

    /** @test */
    public function user_can_update_answer_hotel_question()
    {
        $user = $this->createUser();
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create();
        $this->assertTrue($user->can('update', $answerHotelQuestion));
    }

    /** @test */
    public function user_can_delete_answer_hotel_question()
    {
        $user = $this->createUser();
        $answerHotelQuestion = factory(AnswerHotelQuestion::class)->create();
        $this->assertTrue($user->can('delete', $answerHotelQuestion));
    }
}
