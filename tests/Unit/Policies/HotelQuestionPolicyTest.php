<?php

namespace Tests\Unit\Policies;

use App\HotelQuestion;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelQuestionPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotel_question()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new HotelQuestion));
    }

    /** @test */
    public function user_can_view_hotel_question()
    {
        $user = $this->createUser();
        $hotelQuestion = factory(HotelQuestion::class)->create();
        $this->assertTrue($user->can('view', $hotelQuestion));
    }

    /** @test */
    public function user_can_update_hotel_question()
    {
        $user = $this->createUser();
        $hotelQuestion = factory(HotelQuestion::class)->create();
        $this->assertTrue($user->can('update', $hotelQuestion));
    }

    /** @test */
    public function user_can_delete_hotel_question()
    {
        $user = $this->createUser();
        $hotelQuestion = factory(HotelQuestion::class)->create();
        $this->assertTrue($user->can('delete', $hotelQuestion));
    }
}
