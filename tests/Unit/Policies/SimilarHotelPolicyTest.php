<?php

namespace Tests\Unit\Policies;

use App\SimilarHotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class SimilarHotelPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_similar_hotel()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new SimilarHotel));
    }

    /** @test */
    public function user_can_view_similar_hotel()
    {
        $user = $this->createUser();
        $similarHotel = factory(SimilarHotel::class)->create();
        $this->assertTrue($user->can('view', $similarHotel));
    }

    /** @test */
    public function user_can_update_similar_hotel()
    {
        $user = $this->createUser();
        $similarHotel = factory(SimilarHotel::class)->create();
        $this->assertTrue($user->can('update', $similarHotel));
    }

    /** @test */
    public function user_can_delete_similar_hotel()
    {
        $user = $this->createUser();
        $similarHotel = factory(SimilarHotel::class)->create();
        $this->assertTrue($user->can('delete', $similarHotel));
    }
}
