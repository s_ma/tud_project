<?php

namespace Tests\Unit\Policies;

use App\HotelReviewRating;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelReviewRatingPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotel_review_rating()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new HotelReviewRating));
    }

    /** @test */
    public function user_can_view_hotel_review_rating()
    {
        $user = $this->createUser();
        $hotelReviewRating = factory(HotelReviewRating::class)->create();
        $this->assertTrue($user->can('view', $hotelReviewRating));
    }

    /** @test */
    public function user_can_update_hotel_review_rating()
    {
        $user = $this->createUser();
        $hotelReviewRating = factory(HotelReviewRating::class)->create();
        $this->assertTrue($user->can('update', $hotelReviewRating));
    }

    /** @test */
    public function user_can_delete_hotel_review_rating()
    {
        $user = $this->createUser();
        $hotelReviewRating = factory(HotelReviewRating::class)->create();
        $this->assertTrue($user->can('delete', $hotelReviewRating));
    }
}
