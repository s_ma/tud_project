<?php

namespace Tests\Unit\Policies;

use App\HotelReviewType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelReviewTypePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotel_review_type()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new HotelReviewType));
    }

    /** @test */
    public function user_can_view_hotel_review_type()
    {
        $user = $this->createUser();
        $hotelReviewType = factory(HotelReviewType::class)->create();
        $this->assertTrue($user->can('view', $hotelReviewType));
    }

    /** @test */
    public function user_can_update_hotel_review_type()
    {
        $user = $this->createUser();
        $hotelReviewType = factory(HotelReviewType::class)->create();
        $this->assertTrue($user->can('update', $hotelReviewType));
    }

    /** @test */
    public function user_can_delete_hotel_review_type()
    {
        $user = $this->createUser();
        $hotelReviewType = factory(HotelReviewType::class)->create();
        $this->assertTrue($user->can('delete', $hotelReviewType));
    }
}
