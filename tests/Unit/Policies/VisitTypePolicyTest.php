<?php

namespace Tests\Unit\Policies;

use App\VisitType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class VisitTypePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_visit_type()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new VisitType));
    }

    /** @test */
    public function user_can_view_visit_type()
    {
        $user = $this->createUser();
        $visitType = factory(VisitType::class)->create();
        $this->assertTrue($user->can('view', $visitType));
    }

    /** @test */
    public function user_can_update_visit_type()
    {
        $user = $this->createUser();
        $visitType = factory(VisitType::class)->create();
        $this->assertTrue($user->can('update', $visitType));
    }

    /** @test */
    public function user_can_delete_visit_type()
    {
        $user = $this->createUser();
        $visitType = factory(VisitType::class)->create();
        $this->assertTrue($user->can('delete', $visitType));
    }
}
