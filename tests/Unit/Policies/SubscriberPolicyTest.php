<?php

namespace Tests\Unit\Policies;

use App\Subscriber;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class SubscriberPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_subscriber()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Subscriber));
    }

    /** @test */
    public function user_can_view_subscriber()
    {
        $user = $this->createUser();
        $subscriber = factory(Subscriber::class)->create();
        $this->assertTrue($user->can('view', $subscriber));
    }

    /** @test */
    public function user_can_update_subscriber()
    {
        $user = $this->createUser();
        $subscriber = factory(Subscriber::class)->create();
        $this->assertTrue($user->can('update', $subscriber));
    }

    /** @test */
    public function user_can_delete_subscriber()
    {
        $user = $this->createUser();
        $subscriber = factory(Subscriber::class)->create();
        $this->assertTrue($user->can('delete', $subscriber));
    }
}
