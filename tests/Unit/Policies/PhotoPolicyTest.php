<?php

namespace Tests\Unit\Policies;

use App\Photo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class PhotoPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_photo()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Photo));
    }

    /** @test */
    public function user_can_view_photo()
    {
        $user = $this->createUser();
        $photo = factory(Photo::class)->create();
        $this->assertTrue($user->can('view', $photo));
    }

    /** @test */
    public function user_can_update_photo()
    {
        $user = $this->createUser();
        $photo = factory(Photo::class)->create();
        $this->assertTrue($user->can('update', $photo));
    }

    /** @test */
    public function user_can_delete_photo()
    {
        $user = $this->createUser();
        $photo = factory(Photo::class)->create();
        $this->assertTrue($user->can('delete', $photo));
    }
}
