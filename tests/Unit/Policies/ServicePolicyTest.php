<?php

namespace Tests\Unit\Policies;

use App\Service;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class ServicePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_service()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Service));
    }

    /** @test */
    public function user_can_view_service()
    {
        $user = $this->createUser();
        $service = factory(Service::class)->create();
        $this->assertTrue($user->can('view', $service));
    }

    /** @test */
    public function user_can_update_service()
    {
        $user = $this->createUser();
        $service = factory(Service::class)->create();
        $this->assertTrue($user->can('update', $service));
    }

    /** @test */
    public function user_can_delete_service()
    {
        $user = $this->createUser();
        $service = factory(Service::class)->create();
        $this->assertTrue($user->can('delete', $service));
    }
}
