<?php

namespace Tests\Unit\Policies;

use App\Hotel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotel()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Hotel));
    }

    /** @test */
    public function user_can_view_hotel()
    {
        $user = $this->createUser();
        $hotel = factory(Hotel::class)->create();
        $this->assertTrue($user->can('view', $hotel));
    }

    /** @test */
    public function user_can_update_hotel()
    {
        $user = $this->createUser();
        $hotel = factory(Hotel::class)->create();
        $this->assertTrue($user->can('update', $hotel));
    }

    /** @test */
    public function user_can_delete_hotel()
    {
        $user = $this->createUser();
        $hotel = factory(Hotel::class)->create();
        $this->assertTrue($user->can('delete', $hotel));
    }
}
