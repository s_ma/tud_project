<?php

namespace Tests\Unit\Policies;

use App\PriceRange;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class PriceRangePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_price_range()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new PriceRange));
    }

    /** @test */
    public function user_can_view_price_range()
    {
        $user = $this->createUser();
        $priceRange = factory(PriceRange::class)->create();
        $this->assertTrue($user->can('view', $priceRange));
    }

    /** @test */
    public function user_can_update_price_range()
    {
        $user = $this->createUser();
        $priceRange = factory(PriceRange::class)->create();
        $this->assertTrue($user->can('update', $priceRange));
    }

    /** @test */
    public function user_can_delete_price_range()
    {
        $user = $this->createUser();
        $priceRange = factory(PriceRange::class)->create();
        $this->assertTrue($user->can('delete', $priceRange));
    }
}
