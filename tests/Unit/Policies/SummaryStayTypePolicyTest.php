<?php

namespace Tests\Unit\Policies;

use App\SummaryStayType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class SummaryStayTypePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_summary_stay_type()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new SummaryStayType));
    }

    /** @test */
    public function user_can_view_summary_stay_type()
    {
        $user = $this->createUser();
        $summaryStayType = factory(SummaryStayType::class)->create();
        $this->assertTrue($user->can('view', $summaryStayType));
    }

    /** @test */
    public function user_can_update_summary_stay_type()
    {
        $user = $this->createUser();
        $summaryStayType = factory(SummaryStayType::class)->create();
        $this->assertTrue($user->can('update', $summaryStayType));
    }

    /** @test */
    public function user_can_delete_summary_stay_type()
    {
        $user = $this->createUser();
        $summaryStayType = factory(SummaryStayType::class)->create();
        $this->assertTrue($user->can('delete', $summaryStayType));
    }
}
