<?php

namespace Tests\Unit\Policies;

use App\Review;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class ReviewPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_review()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Review));
    }

    /** @test */
    public function user_can_view_review()
    {
        $user = $this->createUser();
        $review = factory(Review::class)->create();
        $this->assertTrue($user->can('view', $review));
    }

    /** @test */
    public function user_can_update_review()
    {
        $user = $this->createUser();
        $review = factory(Review::class)->create();
        $this->assertTrue($user->can('update', $review));
    }

    /** @test */
    public function user_can_delete_review()
    {
        $user = $this->createUser();
        $review = factory(Review::class)->create();
        $this->assertTrue($user->can('delete', $review));
    }
}
