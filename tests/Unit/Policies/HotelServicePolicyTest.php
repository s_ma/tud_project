<?php

namespace Tests\Unit\Policies;

use App\HotelService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelServicePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotel_service()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new HotelService));
    }

    /** @test */
    public function user_can_view_hotel_service()
    {
        $user = $this->createUser();
        $hotelService = factory(HotelService::class)->create();
        $this->assertTrue($user->can('view', $hotelService));
    }

    /** @test */
    public function user_can_update_hotel_service()
    {
        $user = $this->createUser();
        $hotelService = factory(HotelService::class)->create();
        $this->assertTrue($user->can('update', $hotelService));
    }

    /** @test */
    public function user_can_delete_hotel_service()
    {
        $user = $this->createUser();
        $hotelService = factory(HotelService::class)->create();
        $this->assertTrue($user->can('delete', $hotelService));
    }
}
