<?php

namespace Tests\Unit\Policies;

use App\VisitDate;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class VisitDatePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_visit_date()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new VisitDate));
    }

    /** @test */
    public function user_can_view_visit_date()
    {
        $user = $this->createUser();
        $visitDate = factory(VisitDate::class)->create();
        $this->assertTrue($user->can('view', $visitDate));
    }

    /** @test */
    public function user_can_update_visit_date()
    {
        $user = $this->createUser();
        $visitDate = factory(VisitDate::class)->create();
        $this->assertTrue($user->can('update', $visitDate));
    }

    /** @test */
    public function user_can_delete_visit_date()
    {
        $user = $this->createUser();
        $visitDate = factory(VisitDate::class)->create();
        $this->assertTrue($user->can('delete', $visitDate));
    }
}
