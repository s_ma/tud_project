<?php

namespace Tests\Unit\Policies;

use App\Hotelier;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelierPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotelier()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Hotelier));
    }

    /** @test */
    public function user_can_view_hotelier()
    {
        $user = $this->createUser();
        $hotelier = factory(Hotelier::class)->create();
        $this->assertTrue($user->can('view', $hotelier));
    }

    /** @test */
    public function user_can_update_hotelier()
    {
        $user = $this->createUser();
        $hotelier = factory(Hotelier::class)->create();
        $this->assertTrue($user->can('update', $hotelier));
    }

    /** @test */
    public function user_can_delete_hotelier()
    {
        $user = $this->createUser();
        $hotelier = factory(Hotelier::class)->create();
        $this->assertTrue($user->can('delete', $hotelier));
    }
}
