<?php

namespace Tests\Unit\Policies;

use App\Favourite;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class FavouritePolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_favourite()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Favourite));
    }

    /** @test */
    public function user_can_view_favourite()
    {
        $user = $this->createUser();
        $favourite = factory(Favourite::class)->create();
        $this->assertTrue($user->can('view', $favourite));
    }

    /** @test */
    public function user_can_update_favourite()
    {
        $user = $this->createUser();
        $favourite = factory(Favourite::class)->create();
        $this->assertTrue($user->can('update', $favourite));
    }

    /** @test */
    public function user_can_delete_favourite()
    {
        $user = $this->createUser();
        $favourite = factory(Favourite::class)->create();
        $this->assertTrue($user->can('delete', $favourite));
    }
}
