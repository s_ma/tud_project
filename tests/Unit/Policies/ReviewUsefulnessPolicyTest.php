<?php

namespace Tests\Unit\Policies;

use App\ReviewUsefulness;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class ReviewUsefulnessPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_review_usefulness()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new ReviewUsefulness));
    }

    /** @test */
    public function user_can_view_review_usefulness()
    {
        $user = $this->createUser();
        $reviewUsefulness = factory(ReviewUsefulness::class)->create();
        $this->assertTrue($user->can('view', $reviewUsefulness));
    }

    /** @test */
    public function user_can_update_review_usefulness()
    {
        $user = $this->createUser();
        $reviewUsefulness = factory(ReviewUsefulness::class)->create();
        $this->assertTrue($user->can('update', $reviewUsefulness));
    }

    /** @test */
    public function user_can_delete_review_usefulness()
    {
        $user = $this->createUser();
        $reviewUsefulness = factory(ReviewUsefulness::class)->create();
        $this->assertTrue($user->can('delete', $reviewUsefulness));
    }
}
