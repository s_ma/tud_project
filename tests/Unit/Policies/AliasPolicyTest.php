<?php

namespace Tests\Unit\Policies;

use App\Alias;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class AliasPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_alias()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new Alias));
    }

    /** @test */
    public function user_can_view_alias()
    {
        $user = $this->createUser();
        $alias = factory(Alias::class)->create();
        $this->assertTrue($user->can('view', $alias));
    }

    /** @test */
    public function user_can_update_alias()
    {
        $user = $this->createUser();
        $alias = factory(Alias::class)->create();
        $this->assertTrue($user->can('update', $alias));
    }

    /** @test */
    public function user_can_delete_alias()
    {
        $user = $this->createUser();
        $alias = factory(Alias::class)->create();
        $this->assertTrue($user->can('delete', $alias));
    }
}
