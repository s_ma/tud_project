<?php

namespace Tests\Unit\Policies;

use App\HotelCategory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\BrowserKitTest as TestCase;

class HotelCategoryPolicyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_create_hotel_category()
    {
        $user = $this->createUser();
        $this->assertTrue($user->can('create', new HotelCategory));
    }

    /** @test */
    public function user_can_view_hotel_category()
    {
        $user = $this->createUser();
        $hotelCategory = factory(HotelCategory::class)->create();
        $this->assertTrue($user->can('view', $hotelCategory));
    }

    /** @test */
    public function user_can_update_hotel_category()
    {
        $user = $this->createUser();
        $hotelCategory = factory(HotelCategory::class)->create();
        $this->assertTrue($user->can('update', $hotelCategory));
    }

    /** @test */
    public function user_can_delete_hotel_category()
    {
        $user = $this->createUser();
        $hotelCategory = factory(HotelCategory::class)->create();
        $this->assertTrue($user->can('delete', $hotelCategory));
    }
}
